<?php

use App\Maincategory;
use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

Route::prefix('user')->name('secureApi')->middleware('api')->group(function() {
    Route::post('login', 'Api\UserController@login');
    Route::get('tokens', 'Api\UserController@tokens')->middleware('auth:api');
});

Route::prefix('user-actions')->name('userApi.')->group(function() {
    Route::get('/{target}/{targetId}/comments', 'CommentController@show')->name('comments');
    Route::post('/{target}/{targetId}/comment', 'CommentController@store')->name('commenting');
    Route::post('/{target}/{targetId}/like', 'LikeController@store')->name('liking');

});

Route::prefix('platform')->name('platform.')->group(function() {
    Route::get('/', function () {

    });
    Route::get('posts', function(){
        return Post::with(['categories.maincategory', 'photo.file'])->withCount(['likes', 'bookmarks'])->latest()->paginate(12);
    })->name('posts.all');
    Route::get('search/{keyword}', 'Api\SearchController@index')->name('search');
    Route::get('search/{keyword}/pro', 'Api\SearchController@show')->name('search.pro');
    Route::get('post/{post}', function ($post) {
        return Post::find($post);
    })->name('posts.show');
    Route::post('post/{post}', function (Request $request, $post) {
        $post = Post::find($post);
        $post->body = $request->body;
        $post->save();
        return ['okay' => true];
    })->name('posts.update');
    Route::get('/users/{user_id}/followers', 'FollowController@followers')->name('followers');
    Route::get('/users/{user_id}/followings', 'FollowController@followings')->name('followings');
    Route::post('/follow/{user}', 'FollowController@follow')->name('follow');
    Route::post('/unfollow/{user}', 'FollowController@unfollow')->name('unfollow');
});

Route::prefix('quiz')->name('Quiz.')->middleware('throttle:20,1')->group(function(){
   Route::get('/{quiz}/data', 'Api\QuizController@show');
   Route::post('/{quiz}/update', 'Api\QuizController@update')->name('update');
});

Route::post('writer/final', function(Request $request){
    if ($request->user) {
        $user = \App\User::with('writer')->whereEmail($request->user)->firstOrFail();
        $writer = $user->writer;
        if ($writer) {
            $writer->active = 2;
            $writer->save();
            $ticket = new \App\Ticket;
            $ticket->title = 'درخواست داستان نویسی کد ' . $writer->id;
            $ticket->question = 'درخواست بررسی فرم داستان نویسی اینجانب';
            $ticket->force_rate = 1;
            $ticket->status = 'OPEN';
            $ticket->code = time();
            $ticket->backup_section_id = 1;
            $res = $user->tickets()->create($ticket->toArray());
            $message = new \App\TicketMessage;
            $message->text = "با سلام و عرض ادب خدمتتان
\n
فرم درخواست نامه شما مبنی بر عضویت به عنوان داستان نویس در ال ناول به شناسه {$ticket->code} در تاریخ ... با موفقیت برای حلقه منتقدین ما ارسال شد . هم اکنون حلقه منتقدین در حال بررسی درخواست نامه شما هستند و به زودی پاسخ نهایی ما را از طریق همین سیستم پشتیبان برای شما ارسال خواهند کرد . در صورتی که پس از گذشت یک هفته از سیستم پشتیبان پاسخی دریافت نکردید می توانید در همین صفحه پشتیبانی , برای ما نامه ایی بنویسید و روند بررسی درخواست نامه خود را پیگیری کنید . امیدواریم به زودی زود شما را در جمع داستان نویسان ال ناول همراهی کنیم .
\n
قلمتان رقصان , شمع دلتان افروز
\n
مدیریت تیم ELNOVEL";
            $ticket = $res;
            $message->user_id = 1;
            $message->type = 'answer';
            $message = $ticket->messages()->create($message->toArray());
            $ticket->updated_at = now();
            $ticket->save();
            return ['okay' => true, 'redirect_uri' => route('userarea.tickets.show', ['ticket' => $res->id])];
        }
    }
    return response('UnAuthorized', 403);
})->middleware(['throttle:20,1'])->name('final-writer');

Route::prefix('cms')->name('Cms.')->middleware([
    'auth:api2', 'throttle:30,1'
])->group(function() {
    Route::get('/', function() { return true; });
    Route::get('/categories', function () {
        return Maincategory::all();
    })->name('categories');
    Route::get('files', function () {
        return App\File::paginate(20);
    })->name('files');
    Route::get('files/{type}', function ($type) {
        return App\File::whereType($type)->paginate(20);
    })->name('filetype');
    Route::post('/comments/{comment}/toggle', function ($comment) {
        $comment = \App\Comment::find($comment);
        $comment->update(['active' => ! $comment->active]);
        return ['mode' => $comment->active];
    })->name('comments.toggle');
	Route::post('changeFile/{method}/{id}/{type}', function(Request $request,$method, $id, $type) {
        $targets = ['podcast' => App\Podcast::with($type), 'post' => App\Post::with($type), 'writer' => App\Writer::with($type)];
        switch ($method) {
            case 'post':
            case 'podcast':
            case 'writer':
                $target = $targets[$method]->findOrFail($id);
                $file = App\File::find($request->file_id);
                if ($type == 'file' && $file) {
                    $target->file()->associate($file)->save();
                    return $target;
                } else if($type == 'photo' && $file) {
                    if ($target->photo) {
                        $target->photo()->update([
                            'file_id' => $file->id,
                            'title' => 'kos khar',
                            'alt' => ''
                        ]);
                        return $file;
                    } else {
                        $target->photo()->create([
                            'file_id' => $file->id,
                            'title' => 'kos khar',
                            'alt' => ''
                        ]);
                        return $file;
                    }
                }
                return $target;
            break;
            default:
                return abort(403);
            break;
        }
    })->name('changeFile');
});
