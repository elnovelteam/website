<?php

use App\Events\TicketCreated;
use App\Notifications\PostCreatedNotification;
use App\Notifications\RulesChangedNotification;
use App\Podcast;
use App\Ticket;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use NotificationChannels\Telegram\TelegramChannel;

app()->setLocale('fa');
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('theme.index');
})->name('home');

Route::namespace('\ElAdmin\UA\Http\Controllers')->prefix('userarea')->middleware([
    'verified'
])->name('userarea.')->group(function(){
   Route::get('/', 'DashboardController@index');
   Route::get('settings', 'DashboardController@settings')->name('settings.index');
   Route::post('settings', 'DashboardController@updateProfile')->name('settings.update');
   Route::get('writers/{writer}', 'WriterController@edit')->name('writers.edit');
   Route::get('writer/request', 'WriterController@create')->name('writers.request');
   Route::resource('writers', 'WriterController')->only(['update', 'store']);
   Route::resource('tickets', 'TicketController');
   Route::resource('books', 'BookController');
});

Route::resource('books', 'BookController');
Route::resource('podcasts', 'PodcastController')->only(['index','show']);
Route::get('contact', 'HomeController@contact');

Route::get('users/{name}', function ($name) {
    return $user = App\User::whereName($name)->firstOrFail();
});

Route::get('sms', 'SmsController@index');

Route::get('notifs', function () {
    $user = User::find(1);
    $notif = new RulesChangedNotification(null, 'database');
    $user->notify($notif->toSms(null));
    // return Notification::send($users, $notif);
});

Route::get('queue', function () {
    // Log::info('hello there !');
    // $podcast = Podcast::find(2);
    // event(new TestEvent($podcast));
    // return $podcast;
    // // // return $podcast;
    // ProcessPodcast::dispatch($podcast)
    //     ->delay(now()->addSeconds(10));
    // Queue::push(function($job){
    //     // Log::info('Hello my queue !');
    //     // $job->delete();
    //     // return 'done !';
    // });
    $user = User::find(23);
    $ticket = Ticket::find(1);
    event(new TicketCreated($user, $ticket));
    return 'yeah !';
    // event(new UserVerified($user));
});

Route::get('telegram-test', function () {
    $podcast = Podcast::find(2);
    $podcast->notify((new PostCreatedNotification($podcast, [TelegramChannel::class]))->delay(now()->addMinute()));
    return 'sent !';
});

Route::get('test', function() {
    $json = '{"pages" : [
        {
            "id": 1,
            "content": [
                {
                    "id": 1
                },
                {
                    "id": 2
                }
            ]
        }
    ]}';
    $json = json_decode($json, true);
    return $json;
    // return response(json_encode($json));
    // return json_encode($json);
    // Auth::loginUsingId(1);
    // $user = auth()->user();
    // $user->unreadNotifications()->update(['read_at' => now()]);
    // // return $user;
    // $book = [];
    // $book['id'] = 678;
    // $notification = new InvoicePaidNotification($book);
    // $user->notify($notification);
});

Auth::routes(['verify' => true]);

Route::prefix('login')->group(function(){
    Route::get('{driver}', 'SocialController@provider')->name('login.driver');
    Route::get('{driver}/validate', 'SocialController@providerRedirect');
});

Route::get('/phpinfo', function () {
    return phpinfo();
});

Route::get('search', function (Request $request) {
    $keyword = $request->keyword ?: '';
    return view('theme.search', compact('keyword'));
})->name('search');

Route::prefix('sitemaps')->name('sitemaps.')->group(function(){
    Route::get('/sitemaps.xml', 'SitemapsController@index');
    Route::get('/sitemap-{sitemap}.xml', 'SitemapsController@show')->name('show');
});

Route::resource('blog', 'BlogController');

Route::get('editor', 'HomeController@editor');

Route::get('/follow/{user}', 'FollowController@follow');

Route::get('/unfollow/{user}', 'FollowController@unfollow');

Route::get('/me', function() {
   $user = User::whereId(auth()->user()->id)->withCount('followings')->firstOrFail();
   $user->load('followings.followable', 'followers.user');
//    return $user;
   return view('theme.sample-profile', compact('user'))->with(['self' => true]);
});

Route::get('/users/{user}', function(User $user) {
//    $user = User::whereId(auth()->user()->id)->withCount('followings')->firstOrFail();
   $user->load('followings.followable', 'followers.user');
   return view('theme.sample-profile', compact('user'))->with(['self' => false]);
});
Route::resource('writers', 'WriterController');
