<?php

namespace Ghasedak\Laravel;

use Ghasedak\GhasedakApi;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    public function boot()
    {
        $this->publishes([
            __DIR__.'/config/config.php' => config_path('ghasedak.php'),
        ]);
    }
    public function register()
    {
        $this->app->singleton('ghasedak', function($app) {
            dd(config('ghasedak'));
            // return new GhasedakApi(config('ghasedak.apikey'));
        });
    }
}
