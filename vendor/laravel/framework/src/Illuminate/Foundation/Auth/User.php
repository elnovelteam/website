<?php

namespace Illuminate\Foundation\Auth;

use App\Role;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\MustVerifyEmail;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\Access\Authorizable;

class User extends Model implements
    AuthenticatableContract,
    AuthorizableContract,
    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword, MustVerifyEmail;
    public function hasRole($role)
    {
        if (is_string($role)) {
            return $this->roles->contains('name',$role);
        }
        if (isset($this->roles[0])) {
            return !! $role->intersect($this->roles)->count();
        }
        return false;
    }
    public function hasPermission($role, $permission)
    {
        $perms = $this->roles()->where("name",$role)->first()->permissions;

        if (is_string($permission)) {
            return $perms->contains('name',$permission);
        }

        $perms = $perms->mapToGroups(function ($item, $key) {
            return [$item['name']];
        })->first();

        $permission = collect($permission);
        return !! ($permission->intersect($perms)->count() == $permission->count());
    }
    public function assignRole($role)
    {
        return $this->roles()->sync(Role::whereId($role)->firstOrFail());
    }
    public function owns($related)
    {
        return ($this->id == $related->user_id);
    }
    public function active() {
		return $this->update(['active' => true]);
	}
}
