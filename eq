[33mcommit 677d3257bcb328c4e68a55908c71fb3882bc0d8d[m[33m ([m[1;36mHEAD -> [m[1;32mform[m[33m, [m[1;31morigin/form[m[33m)[m
Author: Amir <amirreza.mansoorian2003@gmail.com>
Date:   Sun Apr 26 19:38:08 2020 +0430

    max words count for all components is applied

[33mcommit 9cc6ec6683af3c9788767414e3564aacdb4d85db[m
Author: Amir <amirreza.mansoorian2003@gmail.com>
Date:   Sun Apr 26 16:12:02 2020 +0430

    id problem and bind callback fixed

[33mcommit 3b88289cb7c9c5480b44e5508284c7d72eb5fa4d[m
Author: Amir <amirreza.mansoorian2003@gmail.com>
Date:   Sun Apr 26 00:21:45 2020 +0430

    all redux state updates and reducer actions are fixed

[33mcommit 74a3e663d569cef2e4e0ea2e8e294397230f211c[m
Author: Amir <amirreza.mansoorian2003@gmail.com>
Date:   Sat Apr 25 20:45:59 2020 +0430

    radio buttons fixed

[33mcommit 05a7259b7a76ea3fc067546761d596ee570adbe6[m
Author: Amir <amirreza.mansoorian2003@gmail.com>
Date:   Sat Apr 25 20:22:28 2020 +0430

    just a little bog fixing

[33mcommit 2da11e37837583b71c8c1ea2bf2f34b4f6fdab07[m
Author: Amir <amirreza.mansoorian2003@gmail.com>
Date:   Sat Apr 25 19:36:24 2020 +0430

    last error for quiz item solved

[33mcommit 9cef2938dafb99988da79193b8e7d87da0d86460[m
Author: yoonus <yoonustehrani28@gmail.com>
Date:   Sat Apr 25 19:25:27 2020 +0430

    bug fixed QuizItem.js

[33mcommit f8611cbaa49488a3efbf2a28ffdb37551fc7de32[m
Author: Amir <amirreza.mansoorian2003@gmail.com>
Date:   Sat Apr 25 19:12:07 2020 +0430

    a few changes in quizitem

[33mcommit 7e355563e4ff97fbef1e84367aff6b306c48eb7f[m
Author: yoonus <yoonustehrani28@gmail.com>
Date:   Sat Apr 25 17:59:23 2020 +0430

    Preparing Quiz Api

[33mcommit 97710a3b2199a2d92da0bcdbd60fa2e5accb6ee1[m
Author: Amir <amirreza.mansoorian2003@gmail.com>
Date:   Sat Apr 25 17:50:12 2020 +0430

    fix initial state value error

[33mcommit 1ba208de41c4b867a287223da45a2b8ef9d7facb[m
Author: Amir <amirreza.mansoorian2003@gmail.com>
Date:   Sat Apr 25 17:22:22 2020 +0430

    fixing error in request-form.js

[33mcommit 7807de0728558b38402677cfa9b4d4e792a30ac4[m
Author: Amir <amirreza.mansoorian2003@gmail.com>
Date:   Sat Apr 25 17:20:21 2020 +0430

    fixing webpack mix and other errors for request form

[33mcommit f12e67c7d889ee78ad691e046daff02681ed420f[m
Author: yoonus <yoonustehrani28@gmail.com>
Date:   Sat Apr 25 16:56:20 2020 +0430

    loading request-form.js in writer request form create.blade.php

[33mcommit 7a730a75ebfd2d7e8c4c01fd4205eaa54bd0ce0b[m
Author: yoonus <yoonustehrani28@gmail.com>
Date:   Sat Apr 25 16:54:03 2020 +0430

    bug fixed

[33mcommit ec90387700eea8b6548ddbbd8e4630849570de79[m
Merge: 6a6afc8 9a4cdca
Author: yoonus <yoonustehrani28@gmail.com>
Date:   Sat Apr 25 16:48:07 2020 +0430

    preparing view and controller for the redux quiz form

[33mcommit 6a6afc83a07ecdd900b30573d4576f58cf025fbf[m
Author: yoonus <yoonustehrani28@gmail.com>
Date:   Sat Apr 25 16:40:26 2020 +0430

    my god

[33mcommit 9a4cdca6d0a7b2e2492e78d0dd5b142506d833a6[m
Author: Amir <amirreza.mansoorian2003@gmail.com>
Date:   Sat Apr 25 16:32:47 2020 +0430

    fixing questions ids

[33mcommit 0a9a5080a7b67a4d9750b9b806cd202b5cfa5e37[m
Author: Amir <amirreza.mansoorian2003@gmail.com>
Date:   Sat Apr 25 16:00:16 2020 +0430

    request form designed

[33mcommit 6112662f7bde7dc81e7ab714c74c30f6346b8972[m
Author: yoonus <yoonustehrani28@gmail.com>
Date:   Sat Apr 25 03:24:59 2020 +0430

    writer store bug fixed

[33mcommit d43923fb2530b9486fc6ea782b6b3f6a5569d3a6[m
Author: yoonus <yoonustehrani28@gmail.com>
Date:   Sat Apr 25 01:41:05 2020 +0430

    1. prepared and modified Quiz models
    2. changhedWriter controller
    3. updated validation translation loop,
    4. modified error layouts with swal2 and customized css
    5. updated userarea routings in web.php router

[33mcommit c5e4f7156acacf0dcdcbfc67685dda912d0b63a6[m
Author: yoonus <yoonustehrani28@gmail.com>
Date:   Fri Apr 24 04:50:29 2020 +0430

    RequestFormReducer default states

[33mcommit 20123536df73b4acdbf7cb68b9ffa747a605a873[m
Author: yoonus <yoonustehrani28@gmail.com>
Date:   Fri Apr 24 04:14:32 2020 +0430

    edited request-from.js bug fixed

[33mcommit 76162cc227dd9bf4d43aac0417c9211fa9afa22b[m
Author: yoonus <yoonustehrani28@gmail.com>
Date:   Fri Apr 24 03:41:26 2020 +0430

    quiz_questions migration fixed bug

[33mcommit 88f5d4fc44fa9511423a2d19ed6e88ce7e58528d[m
Author: yoonus <yoonustehrani28@gmail.com>
Date:   Fri Apr 24 03:30:11 2020 +0430

    quiz migrations bug fixed

[33mcommit f8d90d78043ef2acf4c866f93bfe322dc40d6e44[m
Author: yoonus <yoonustehrani28@gmail.com>
Date:   Fri Apr 24 03:09:31 2020 +0430

    feature added => create_quiz_questions added required type boolean column

[33mcommit 2307f9a9b541a26bd60562c41d893a9dbf4eb3fe[m
Author: yoonus <yoonustehrani28@gmail.com>
Date:   Fri Apr 24 02:58:30 2020 +0430

    last commit migration fixed bug

[33mcommit a719998fd47177755463c2fcae4cf3d5d308c0c9[m
Author: yoonus <yoonustehrani28@gmail.com>
Date:   Fri Apr 24 02:54:32 2020 +0430

    Creating models and migrations for
    quizzes.

[33mcommit 82f188ac553d55691c765ad75c4a761ba2263ea3[m
Author: Amir <amirreza.mansoorian2003@gmail.com>
Date:   Fri Apr 24 02:32:34 2020 +0430

    request form redux initial commit

[33mcommit cf372c6b6cc30b0c1394ec9237fb5799dfb32f36[m
Author: yoonus <yoonustehrani28@gmail.com>
Date:   Fri Apr 24 02:19:50 2020 +0430

    fix create.blade.php

[33mcommit a79e86ac0ae4338efa27d1dbaaadf5fdc65677e4[m
Author: yoonus <yoonustehrani28@gmail.com>
Date:   Fri Apr 24 02:06:33 2020 +0430

    writer requests create.blade.php

[33mcommit 57b710f71bb5cd632d836bc8cca53be13113f17f[m
Author: yoonus <yoonustehrani28@gmail.com>
Date:   Fri Apr 24 01:33:09 2020 +0430

    Preparing view, controller and route
    for the writers request form

[33mcommit fa15c067d0a93eb2daf008b75e7cd4b14162c5f1[m[33m ([m[1;32mdesign[m[33m)[m
Author: Amir <amirreza.mansoorian2003@gmail.com>
Date:   Fri Apr 24 01:13:02 2020 +0430

    last changes to statistics userarea

[33mcommit 51936175c01e11086dbc1887420f9a1d894b6af8[m
Author: Amir <amirreza.mansoorian2003@gmail.com>
Date:   Fri Apr 24 00:54:33 2020 +0430

    new changes for user area panel

[33mcommit 2fbb36fb4980c01d19fe08e997aeccb80ae6a694[m
Author: Amir <amirreza.mansoorian2003@gmail.com>
Date:   Thu Apr 23 14:53:48 2020 +0430

    new changes in user area panel for top statistics has done

[33mcommit d5e63b9010a5c18eedfa8446152e8148a4b06e2a[m
Author: Amir <amirreza.mansoorian2003@gmail.com>
Date:   Wed Apr 22 22:36:12 2020 +0430

    statistics first desing has done in user area

[33mcommit e503dfeba60c1dcb2beda4fe1b5bd81cdcc18db7[m
Merge: 18401d1 11ec605
Author: Amir <amirreza.mansoorian2003@gmail.com>
Date:   Wed Apr 22 20:28:00 2020 +0430

    Merge branch 'master' of gitlab.com:elnovelteam/website

[33mcommit 18401d1479ec46e7401294ccb4b5c6c3b756d4dd[m
Author: Amir <amirreza.mansoorian2003@gmail.com>
Date:   Wed Apr 22 20:27:31 2020 +0430

    start editing views

[33mcommit 11ec605ebb5bfed44fe79282f12200607cd2485b[m
Author: yoonus <yoonustehrani28@gmail.com>
Date:   Wed Apr 22 20:12:27 2020 +0430

    the webpack mix

[33mcommit e98cf5f63e86cb5032877d0a82a5eeb83992fae8[m
Author: yoonus <yoonustehrani28@gmail.com>
Date:   Wed Apr 22 06:22:17 2020 +0430

    deleted my dick

[33mcommit 26915ae99d61f40fefdc94f2decb3365115730f5[m
Author: yoonus <yoonustehrani28@gmail.com>
Date:   Wed Apr 22 06:20:55 2020 +0430

    added vendor/ghasedak

[33mcommit 02fc6adf32327fd6b9813b2e520ccbb7df4642cf[m
Author: yoonus <yoonustehrani28@gmail.com>
Date:   Wed Apr 22 05:59:52 2020 +0430

    adding the public directory to the git project . editing gitignore . editing coutries table migration

[33mcommit dd297798280a5a5aa8e427c4ef09f5c859eb943a[m
Author: yoonus <yoonustehrani28@gmail.com>
Date:   Wed Apr 22 04:45:54 2020 +0430

    some needy changes on userarea

[33mcommit c8aa407063b047ab4f1f9e56ba53ad584cfc956a[m
Author: yoonus <yoonustehrani28@gmail.com>
Date:   Tue Apr 21 12:41:04 2020 +0430

    mydick

[33mcommit 2084e61cddf7b8d2461bc71adc8ce0cd8e352629[m
Author: yoonus <yoonustehrani28@gmail.com>
Date:   Sun Apr 19 16:11:56 2020 +0430

    deleting shit.txt

[33mcommit a6ff21796a283d3781e29ff9070e7cdcd4ab1631[m
Author: yoonus <yoonustehrani28@gmail.com>
Date:   Sun Apr 19 15:52:03 2020 +0430

    test deleting hello.php

[33mcommit b3648ad1221c8eedc7e6fa5fc1eec907dc9af943[m
Author: yoonus <yoonustehrani28@gmail.com>
Date:   Sun Apr 19 14:46:39 2020 +0430

    Initial commit

[33mcommit 38f38be0817325f74006a1cd09203e05bd40f38e[m
Author: yoonus <yoonustehrani28@gmail.com>
Date:   Sun Apr 19 14:13:01 2020 +0430

    branch master pushing

[33mcommit 2cb9e3c24c70ec3c8bcece7b649591790bce29c7[m
Author: yoonus <yoonustehrani28@gmail.com>
Date:   Sun Apr 19 12:47:30 2020 +0430

    ready for first push

[33mcommit 2ce3341cd26bc69c0192dd907df4a7f3279a65c4[m
Author: Yoonus <yoonustehrani28@gmail.com>
Date:   Sun Apr 5 04:02:34 2020 +0430

    database migrations overall

[33mcommit 383fe592900f706103f2c932042f2bd2dc7df976[m
Author: Yoonus <yoonustehrani28@gmail.com>
Date:   Sat Mar 28 05:16:38 2020 +0430

    up to pick.js

[33mcommit df78b5dca261b095c78dd9d679381b2c5246c630[m
Author: Yoonus <yoonustehrani28@gmail.com>
Date:   Sat Mar 7 18:04:27 2020 +0330

    Kavenegar config

[33mcommit 5f5f46b5c772cd3501bd6c79bbfa70ec4ed1de64[m
Author: Yoonus <yoonustehrani28@gmail.com>
Date:   Mon Feb 24 22:59:03 2020 +0330

    unit 5 done!

[33mcommit 7cff193bcb5811cf70bfa0706f331c5f43159527[m
Author: Yoonus <yoonustehrani28@gmail.com>
Date:   Tue Feb 11 19:31:30 2020 +0330

    git ignore

[33mcommit 2fcf67e289e1062a88796b35bd246d96b8faf0ed[m
Author: Yoonus <yoonustehrani28@gmail.com>
Date:   Tue Feb 11 19:30:42 2020 +0330

    configs

[33mcommit fc4a39442243b726ba5f9acd11b35f82c419d7fe[m
Author: Yoonus <yoonustehrani28@gmail.com>
Date:   Thu Jan 23 11:04:36 2020 +0330

    nothing special

[33mcommit 8e6dbc93cbc2ace9f70b0c256ac6ec08063f5905[m
Author: Yoonus <yoonustehrani28@gmail.com>
Date:   Fri Dec 6 15:36:07 2019 +0330

    setting over editor configs

[33mcommit d910c4e1592283fdf849b4c8e6959ce6ae437fcf[m
Author: Yoonus <yoonustehrani28@gmail.com>
Date:   Fri Dec 6 15:34:29 2019 +0330

    some changes to broadcastings

[33mcommit caa771e9e19bd145b6e54215782d12c3a527f506[m
Author: Yoonus <yoonustehrani28@gmail.com>
Date:   Fri Nov 22 19:46:32 2019 +0330

    training how to use policies

[33mcommit 4ce3f9c9ce3e1b6708835aece30fcaf4b74e8d06[m
Author: Yoonus <yoonustehrani28@gmail.com>
Date:   Fri Nov 22 18:39:50 2019 +0330

    overview on policies

[33mcommit bbe73c0645116499de44c8b708072104235e7db6[m
Author: Yoonus <yoonustehrani28@gmail.com>
Date:   Fri Nov 22 13:59:10 2019 +0330

    some inline javascript changes

[33mcommit dffadf42183a972a0f4850b628f86b014e09d53d[m
Author: Yoonus <yoonustehrani28@gmail.com>
Date:   Fri Nov 22 13:58:09 2019 +0330

    clearing app.scss file

[33mcommit 74f7a393ba34ebe92b4c509922eef98325dad470[m
Author: Yoonus <yoonustehrani28@gmail.com>
Date:   Tue Nov 19 21:21:08 2019 +0330

    some footer css

[33mcommit b7e142e776cb8de84c66a13ff95d3cc060c372f5[m
Author: Yoonus <yoonustehrani28@gmail.com>
Date:   Fri Nov 22 16:50:43 2019 +0330

    some changes to migrations and seeders as well

[33mcommit 5da9b98183fc340fec0bbe081abd692788e37d95[m
Author: Yoonus <yoonustehrani28@gmail.com>
Date:   Fri Nov 22 14:11:50 2019 +0330

    configuring factories and seeders by migrations default

[33mcommit a37a078c5f529a5e22f3f867e71cbc13d2bc3584[m
Author: Yoonus <yoonustehrani28@gmail.com>
Date:   Wed Nov 20 00:37:23 2019 +0330

    models and migration bug fixed

[33mcommit b09191208c01ded2cc13d26aedbf6f50efddb53f[m
Author: Yoonus <yoonustehrani28@gmail.com>
Date:   Tue Nov 19 19:51:47 2019 +0330

    some css changing and making blade view files better

[33mcommit 5882c4b6ba1e93a228e4d871acff07f10c33995c[m
Author: Yoonus <yoonustehrani28@gmail.com>
Date:   Tue Nov 19 17:45:31 2019 +0330

    s

[33mcommit 737e45b9a618b778c5712314f3661765d9efa060[m
Author: Yoonus <yoonustehrani28@gmail.com>
Date:   Tue Nov 19 17:44:16 2019 +0330

    some changes to robots.txt

[33mcommit d4fe820bce46528e4493e498159bdc4961a432a4[m
Author: Yoonus <yoonustehrani28@gmail.com>
Date:   Tue Nov 19 17:35:34 2019 +0330

    views folder von framework

[33mcommit 720d19d212d0b6e9aba52f9c20e343d142d871a3[m
Author: Yoonus <yoonustehrani28@gmail.com>
Date:   Tue Nov 19 17:32:29 2019 +0330

    sliders problems

[33mcommit 68eba4089beda872cc4b80655f702c0f25272933[m
Author: Yoonus <yoonustehrani28@gmail.com>
Date:   Tue Nov 19 15:33:49 2019 +0330

    first configs

[33mcommit e0160ed327b1f8bedbcaeb95d9e4541f3a3b500a[m
Author: Yoonus <yoonustehrani28@gmail.com>
Date:   Tue Nov 19 15:32:36 2019 +0330

    first configs

[33mcommit ebde493ffbac0dbd8c48083b2486569b3dc20a66[m
Author: Yoonus <yoonustehrani28@gmail.com>
Date:   Fri Oct 25 12:55:30 2019 +0330

    what ????

[33mcommit 6c4e7dee42f7defa4a37ab83fe78219cafb33669[m
Author: Yoonus <yoonustehrani28@gmail.com>
Date:   Fri Oct 25 12:55:07 2019 +0330

    first time git

[33mcommit 993fc4b02f78e413fea6e3cdefd3d8e4aac954d3[m[33m ([m[1;31mold-origin/master[m[33m)[m
Author: Elnovel <amirreza.mansoorian81@gmail.com>
Date:   Sun Apr 19 06:30:49 2020 +0000

    Initial commit
