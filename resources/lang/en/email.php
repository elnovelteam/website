<?php

return [
    'subcopy' => "If you’re having trouble clicking the \":actionText\" button, copy and paste the URL below\n".'into your web browser: [:actionURL](:actionURL)'
];