<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'register' => 'Register',
    'remember' => 'Remember Me',
    'login' => 'Login',
    'passwordforget' => 'Forgot Your Password?',
    'name' => 'Username',
    'email' => 'E-mail',
    'birthdate' => 'Birth date',
    'password' => 'Password',
    'passwordconfirm' => 'Confirm password',
    'sex' => 'Sex',
    'phonenumber' => 'Phone number',
    'country' => 'Country',
    'state' => 'State',


    'verifyemail' => 'Verify Your Email Address',
    'freshLinkMessage' => 'A fresh verification link has been sent to your email address.',
    'checkYourEmailMessage' => 'Before proceeding, please check your email for a verification link.',
    'ifDidnotRecieveEmail' => 'If you did not receive the email',
    'requestAnother' => 'click here to request another'
];
