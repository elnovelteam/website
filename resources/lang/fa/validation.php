<?php
return [
    'required' => 'فیلد :attribute اجباری است.',
    'email' => 'فیلد ایمیل صحیح نمی باشد.',
    'confirmed' => 'فیلد تایید :attribute همخوانی ندارد.',
    'unique' => 'این :attribute قبلا انتخاب شده است.',
    'min' => [
        'numeric' => 'مقدار فیلد :attribute باید حداقل :min باشد.',
        'file' => ':attribute باید حداقل :min کیلوبایت باشد.',
        'string' => 'فیلد :attribute باید حداقل :min کاراکتر باشد.',
        'array' => 'فیلد :attribute باید حداقل :min آیتم داشته باشد.',
    ],
    'max' => [
        'numeric' => 'مقدار فیلد :attribute می تواند حداکثر :min باشد.',
        'file' => ':attribute می تواند حداکثر :min کیلوبایت باشد.',
        'string' => 'فیلد :attribute می تواند حداکثر :min کاراکتر باشد.',
        'array' => 'فیلد :attribute می تواند حداکثر :min آیتم داشته باشد.',
    ],
    'fileds' => [
        'firstname' => 'نام',
        'lastname'  => 'نام خانوادگی'
    ]
];