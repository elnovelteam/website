import React from 'react';
import  ReactDOM from 'react-dom';
import CommentsContainer from './components/comments' ;

let target = document.getElementById('react-comments-section');

if (target) {
    let CommentsURL   = target.getAttribute('data-comments');
    let commentingURL = target.getAttribute('data-commenting');
    let apiKey        = target.getAttribute('data-apiKey');
    let likeURL       = function (commentId) {
        let like_uri = target.getAttribute('data-likes');
        return like_uri.replace('commentId', commentId);
    }

    ReactDOM.render(<CommentsContainer apiKey={apiKey} getURL={CommentsURL} postURL={commentingURL} likeURL={likeURL} />, target);
}
