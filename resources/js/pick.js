import React from 'react';
import ReactDOM from "react-dom";
import PickFiles from './components/PickFiles';

function renderFilePicker(id) {
    let filePicker = document.getElementById(id);
    if (filePicker) {
        let fp = $(`#${id}`),
        filetype = fp.attr("data-filetype"),
        target = fp.attr("data-target"),
        csrf_token = fp.attr("data-csrf"),
        mainpage = fp.attr("data-mainpage"),
        main = fp.attr("data-main"),
        usage = fp.attr("data-usage"),
        api_token = fp.attr("data-api-token"),
        callback = fp.attr("data-callback"),
        targetHTML = fp.attr('data-targetHTML');
        ReactDOM.render(<PickFiles Target={target} Usage={usage} Main={main} TargetHTML={targetHTML}
        MainPage={mainpage} ApiToken={api_token} CallBack={callback} CSRF={csrf_token} FileType={filetype} />, filePicker);
    }
}

const filePickers = $(".react-file-picker");

if (filePickers) {
    filePickers.each(function() {
        renderFilePicker($(this).attr('id'));
    });
}