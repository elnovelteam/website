import React from 'react';
import ReactDOM from 'react-dom';
import AudioPlayer from './components/AudioPlayer';

const RAP = document.getElementById('react-audio-player');

if (RAP) {
    let target = RAP.getAttribute('target-audio');
	let avatar = RAP.getAttribute('data-avatar');
	let title = RAP.getAttribute('data-title');
	let subtitle = RAP.getAttribute('data-subtitle');
    ReactDOM.render(<AudioPlayer Avatar={avatar} AudioID={target} title={title} subtitle={subtitle}/>, RAP);
}