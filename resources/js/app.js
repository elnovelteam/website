/**
 * First we will load all of this project's JavaScript dependencies which
 * includes React and other helpers. It's a great starting point while
 * building robust, powerful web applications using React + Laravel.
 */

require('./bootstrap');
require('select2');
require('./search');
window.Swiper = require('swiper');

$('#search-icon,.search-section').click((e) => {
    if(e.target.id !== "react-search-input" && e.target.id !== "react-search-icon" && e.target.id !== "react-search-button" && !e.target.classList.contains("checkboxes") && e.target.id !== "link-li" && e.target.id !== "link") {
        $('.search-section').toggleClass('animated active fadeIn');
        $('#react-search-input').focus();
    }
})

$('.fa-heart,.fa-bookmark').hover(function() {
    $(this).toggleClass('far fas')
});

$('#menu-opener').click(() => $('nav.menu > ul').fadeToggle(500))
/**
 * Next, we will create a fresh React component instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// require('./components/Example');
