import React, { Component } from 'react'
import ReactDom from 'react-dom';
import SearchContainer from './components/search/search';

let target = document.getElementById('react-search');
let search = (keyword) => {
    let search_uri = target.getAttribute("search");
    return search_uri.replace('keyword', keyword)
}
let searchPro = (keyword) => {
    let searchPro_uri = target.getAttribute("searchPro");
    return searchPro_uri.replace('keyword', keyword)
}

if(target) {
    ReactDom.render(<SearchContainer search={search} searchPro={searchPro} />, target)
}