import React from 'react';
import ReactDOM from 'react-dom';
import ContentEditor from './components/ContentEditor';

var CKEditorElement = document.getElementById('react-ck-editor');

if (CKEditorElement) {
    let getTargetURL = $('#react-ck-editor').attr('data-target-url');
    let id = 'react-ck-editor';
    let fp = $(`#${id}`),
        filetype = fp.attr("data-filetype"),
        target = fp.attr("data-target"),
        csrf_token = fp.attr("data-csrf"),
        mainpage = fp.attr("data-mainpage"),
        main = fp.attr("data-main"),
        usage = fp.attr("data-usage"),
        api_token = fp.attr("data-api-token"),
        callback = fp.attr("data-callback"),
        targetHTML = fp.attr('data-targetHTML');
    ReactDOM.render(<ContentEditor filetype={filetype} target={target} api_token={api_token} 
    TargetURL={getTargetURL} csrf_token={csrf_token} main={main} mainpage={mainpage}/>, CKEditorElement);
}