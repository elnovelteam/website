import React from 'react';
import ReactDOM from 'react-dom';
import Gallery from './components/Gallery';
var reactgallery = document.getElementById('react-gallery');
var mainpage = reactgallery.getAttribute('data-mainpage');
var deleter = reactgallery.getAttribute('data-delete');
var csrf = reactgallery.getAttribute('data-csrf');
ReactDOM.render(
	<Gallery Deleter={deleter} CSRF={csrf} MainPage={mainpage} />, reactgallery
)

