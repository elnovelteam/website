import React, { Component } from 'react'

export default class AttrForm extends Component {
    constructor(props){
        super(props);
        this.state = {
            attributes: [],
            selectedAttr: [],
            attrValue: ''
        }
    }
    handleSelectAttr() {
        
    }
    handleAddAttr() {
        console.log(`hello !`);
    }
    handleChangeAttr(e) {
        let vals = e.target.value;
        this.setState(prevState => ({
            attrValue: vals
        }));
    }
    handleLoadAttr(e) {
        console.log('value =>', this.state.attrValue);
    }
    render() {
        return (
            <div className="col-12 p-0">
                <div className="input-group col-md-6 col-12 float-left mb-4 p-0">
                    <div className="input-group-prepend">
                        <span className="input-group-text"><i className="fas fa-euro-sign"></i></span>
                    </div>
                    <input value={this.state.attrValue} onKeyUp={this.handleLoadAttr.bind(this)} onChange={this.handleChangeAttr.bind(this)} type="text" placeholder="attribute" className="form-control col-12"/>
                    <input type="text" placeholder="value" className="form-control col-12"/>
                    <button onClick={this.handleAddAttr.bind(this)} className="btn btn-secondary"><i className="fa fa-plus"></i></button>
                </div>
            </div>
        )
    }
}
