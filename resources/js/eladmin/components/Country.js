import React, { Component } from 'react'
import Axios from 'axios';
import State from './State';

export default class Country extends Component {
  constructor(props){
    super(props);
    this.Countries = React.createRef();
    this.StatesAll = React.createRef();
    this.state = {
        countries: [],
		states: [],
    }
    this.findStates = this.findStates.bind(this);
  }
  formatCountry (country) {
    if (!country.id) {
        return country.text;
    }
    var baseUrl =  `${this.props.MainPage}/images/world-icons`;
    var countryfile = country.element.innerText.replace(" ","-").toLowerCase();
    var $country = $(
        '<span><img width="25" height="25" src="' + baseUrl + '/' + countryfile + '.svg" class="img-flag img-flag-small" /> ' + country.text + '</span>'
    );
    return $country;
  }
  componentDidMount()
  {
    var target = `${this.props.MainPage}/api/v1/countries/?api_token=thisistestapitoken`;
    Axios.defaults.headers.post['Content-Type'] ='application/x-www-form-urlencoded';
    Axios.get(target)
    .then(response => {
        this.setState( (prevState) => {
            return {
                countries: response.data.countries,
                states: response.data.states,
            }
        });
    })
    .catch(error => {
        console.log(error);
    });
    let s = $(this.Countries.current);
    let states = $(this.StatesAll.current);
    s.select2({
        templateResult: this.formatCountry.bind(this)
    });
    states.select2();
    s.on("change", this.findStates);
  }
  findStates(event) {
    var target = `${this.props.MainPage}/api/v1/country/${event.target.value}/states/?api_token=thisistestapitoken`;
    Axios.defaults.headers.post['Content-Type'] ='application/x-www-form-urlencoded';
    Axios.get(target)
    .then(response => {
        this.setState( (prevState) => {
            return {
                states: response.data
            }
        });
        let s = $(this.Countries.current);
        let states = $(this.StatesAll.current);
        s.select2({
            templateResult: this.formatCountry.bind(this)
        });
        states.select2();
        s.on("change", this.findStates);
    })
    .catch(error => {
        console.log(error);
    });
  }
  render() {
    return (
        <div className="col-12 float-left mb-4 p-0">
            <div className="input-group col-lg-6 col-12 float-left">
            <div className="input-group-prepend">
                <span className="input-group-text"><i className="fas fa-flag"> Country</i></span>
            </div>
            <select primary={this.props.MainPage} name="country" onChange={this.findStates} ref={this.Countries} id="country"
            title="state" className="form-control col-12 countries-list float-left">
                {
                this.state.countries.map((data, i) => {
                    return (
                        <State key={i} value={data.id} countryName={data.name}/>
                    )
                })
                }
            </select>
            </div>
            <div className="input-group col-lg-6 col-12 float-left">
            <div className="input-group-prepend">
                <span className="input-group-text"><i className="fas fa-chess-rook"> State</i></span>
            </div>
            <select name="state_id" id="state" ref={this.StatesAll}
            title="state" className="form-control col-10 countries-list float-left">
                {
                this.state.states.map((data, i) => {
                    return (
                        <State key={i} value={data.id} countryName={data.name}/>
                    )
                })
                }
            </select>
            </div>
        </div>
    )
  }
}
