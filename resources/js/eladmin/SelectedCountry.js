import React from 'react';
import ReactDOM from "react-dom";
import Country2 from './components/Country2';
var countries = document.getElementById('react-countries');
var mainpage = countries.getAttribute('data-firstpage')
var selectedstate = countries.getAttribute('data-state')
ReactDOM.render(
    <Country2 MainPage={mainpage} SelectedState={selectedstate}/>, countries
)
