import React from 'react';
import ReactDOM from "react-dom";
import Country from './components/Country';
var countries = document.getElementById('react-countries');
var mainpage = countries.getAttribute('data-firstpage')
ReactDOM.render(
    <Country MainPage={mainpage}/>, countries
)
