import React from 'react';
import ReactDOM from 'react-dom';
import Editor from './components/Editor';

const editorjs = document.getElementById('editor');

ReactDOM.render(<Editor/>, editorjs);
