import React, { component } from 'react';
import reactDom from 'react-dom';
import WriterProfile from './components/writer-profile';

let target = document.getElementById("react-writer-info")

if(target) {
    reactDom.render(<WriterProfile/>, target)
}