import React, { Component } from 'react'
import Axios from 'axios';
import { Bounce } from 'react-activity';
import 'react-activity/dist/react-activity.css';

export default class WriteComment extends Component {
    constructor(props) {
        super(props);
        this.replyRef = React.createRef();
        this.state = {
            isWriting: this.props.replyTo === null ? false : true,
            value: '',
            replyTo: this.props.replyTo,
            commentUser: this.props.commentUser,
            commentBody: this.props.commentBody,
            statusMessage: null,
        }
    }

    UNSAFE_componentWillReceiveProps = (newProps) => {
        const oldProps = this.props;
            this.setState({
                replyTo: newProps.replyTo,
                commentUser: newProps.commentUser,
                commentBody: newProps.commentBody,
                isWriting: newProps.replyTo === null ? false : true,
            })
            if(oldProps.replyTo !== newProps.replyTo) {
                this.scrollToReply();
            }
    }

    toggleState = () => {
        if(this.state.isWriting) {
            this.setState({
                value: '',
                statusMessage: null,
                reachedMax: false
            })
        }
        this.setState(prevState => ({
            isWriting: !prevState.isWriting,
            replyTo: null,
            commentUser: null,
            commentBody: null
        }))
    }

    send = () => {
        let { value, replyTo } = this.state;
        if(this.state.value === '') {
            this.setState({
                statusMessage: <p className="text-danger">لطفا محتوای نظر خود را داخل کادر پایین وارد نمایید</p>
            })
        }
        else if(this.state.reachedMax) {
            this.setState({
                statusMessage: <p className="text-danger">حداکثر تعداد کاراکتر برای هر دیدگاه 220 عدد می باشد</p>
            })
        } else {
            this.setState({
                statusMessage: <div className="text-info mb-3">دیدگاه در حال ارسال <Bounce /></div>,
            })
            Axios.post(`${this.props.URL}?api_token=${this.props.apiKey}`, {
                body: value,
                reply_id: replyTo
            }).then((res) => {
                let comments = this.props.comments;
                comments.unshift({
                    body: value,
                    created_at: Date.now(),
                    likes_count: 0,
                    user: this.props.user,
                    active: 0,
                    status_message: (
                        <div>
                            <p className="text-primary float-right ml-1 p-0">دیدگاه شما با موفقیت ارسال و در انتظار تایید است</p>
                            <i className="fas fa-check-circle fa-1x text-info"></i>
                        </div>
                    )
                })
                this.props.sendComments(comments);
                this.setState({
                    statusMessage: null,
                    isWriting: false,
                    reachedMax: false,
                    value: '',
                    replyTo: null,
                    commentUser: null,
                    commentBody: null
                })
            })
        }
    }

    onchangeText = (e) => {
        if(e.target.value.length >= 220 && e.keyCode !== 8 && e.keyCode !== 37 && e.keyCode !== 38 && e.keyCode !== 39 && e.keyCode !== 40) {
            e.preventDefault()
            this.setState({
                reachedMax: true
            })
            e.target.classList.add("maxed-txtarea")
        } else {
            this.setState({
                reachedMax: false,
                value: e.target.value
            })
            e.target.classList.remove("maxed-txtarea")
        }
    }

    scrollToReply = () => {
        if(this.replyRef.current) {
            window.scrollTo(0, this.replyRef.current.offsetTop)
        }
    }

    render() {
        let { commentBody, commentUser } = this.state;
        let { user, loggedIn } = this.props;
        if(user) {
            if(this.state.isWriting) {
                return (
                    <div ref={this.replyRef} className="write-comment">
                        {this.state.statusMessage}
                        {this.state.replyTo !== null &&
                            <div className="reply-container animated fadeInLeft">
                                <p className="text-primary">در پاسخ به کاربر {commentUser}</p>
                                <p className="text-secondary">{commentBody}</p>
                            </div>
                        }
                        <textarea
                            className="form-control animated fadeIn"
                            value={this.state.value}
                            onChange={this.onchangeText}
                            placeholder="لطفا محتوای دیدگاه خود را وارد نمایید"
                        >
                        </textarea>
                        <div className="float-right">
                            <img src={`http://localhost/images/inc/${user.sex}-avatar.svg`}></img>
                        </div>
                        <div className="float-right mt-1">
                            <button className="btn btn-dark" onClick={this.send}>ارسال</button>
                            <button className="btn btn-secondary" onClick={this.toggleState}>لغو</button>
                        </div>
                    </div>
                )
            } else {
                return (
                    <div className="write-comment" ref={this.replyRef}>
                        <div className="float-right">
                            <img src={`http://localhost/images/inc/${user.sex}-avatar.svg`}></img>
                        </div>
                        <div className="float-right">
                            <button className="btn btn-dark" onClick={this.toggleState}>افزودن دیدگاه</button>
                        </div>
                    </div>
                )
            }
        } else {
            return (
                <div className="login-Status alert alert-info alert-comment">برای ثبت دیدگاه ابتدا لازم است وارد حساب کاربری خود شوید</div>
            )
        }
    }
}
