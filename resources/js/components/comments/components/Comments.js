import React, { Component } from 'react'
import InfiniteScroll from 'react-infinite-scroller';
import Comment from './Comment';

export default class Comments extends Component {
    state = {
        comments: this.props.comments
    }

    componentDidMount() {
       document.getElementsByTagName("body")[0].classList.add("set-window")
       document.getElementsByTagName("html")[0].classList.add("set-window")
    }

    UNSAFE_componentWillReceiveProps = (newProps) => {
        const { comments } = this.props.comments;
        if(comments !== newProps.comments) {
            this.setState({ comments: newProps.comments })
        }
    }

    render() {
        let { comments } = this.state;
        let { likeURL, setReplyId, apiKey } = this.props;
        return (
            <div className="comments">
                <InfiniteScroll pageStart={1} hasMore={this.props.activity === null && this.props.hasMore} loadMore={this.props.handleLoadMore} useWindow={true}>
                    {
                        comments.map((comment, i) => {
                            return ( <Comment key={i} setReplyId={setReplyId} likeURL={likeURL} apiKey={apiKey} {...comment} />)
                        })
                    }
                </InfiniteScroll>
            </div>
        )
    }
}
