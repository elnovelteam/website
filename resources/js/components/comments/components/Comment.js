import React, { Component } from 'react'
import Axios from 'axios'
import moment from 'moment'

moment.locale('fa');

export default class Comment extends Component {
    constructor(props) {
        super(props);
        this.state = {
            likesCount: this.props.likes_count,
            likedByUser: this.props.liked_by_user,
            replyLikes: []
        }
        this.replyLikes = []
        this.props.replies !== undefined ? this.props.replies.map((reply, i) => {
            this.replyLikes.push({likesCount: reply.likes_count, likedByUser: reply.liked_by_user})
        }) : null
    }

    componentDidMount = () => {
        this.setState({
            replyLikes: this.replyLikes
        })
    }

    UNSAFE_componentWillReceiveProps = (newProps) => {
        this.setState(prevState => ({
            likesCount: newProps.likes_count,
            likedByUser: newProps.liked_by_user,
        }))
    }

    like = (changeState, replyId, replyIndex, e) => {
        let { likeURL, apiKey, id } = this.props;
        e.target.classList.toggle("liked")
        if(changeState) {
            this.setState(prevState => ({
                likesCount: prevState.likedByUser ? prevState.likesCount - 1 : prevState.likesCount + 1,
                likedByUser: !prevState.likedByUser
            }))
            Axios.post(`${likeURL(id)}?api_token=${apiKey}`, null).then(res => { });
        } else {
            let replyLikes = this.state.replyLikes;
            if(replyLikes[replyIndex].likedByUser) {
                replyLikes[replyIndex].likesCount -= 1
            } else {
                replyLikes[replyIndex].likesCount += 1
            }
            replyLikes[replyIndex].likedByUser = !replyLikes[replyIndex].likedByUser
            this.setState(prevState => ({
                replyLikes: replyLikes
            }))
            Axios.post(`${likeURL(replyId)}?api_token=${apiKey}`, null).then(res => { });
        }
    }

    reply = (id, userName, body, e) => {
        let { setReplyId } = this.props;
        setReplyId(id, userName, body);
    }

    render() {
        let { created_at, user, body, liked_by_user, replies, status_message, id, active } = this.props;
        return (
            <div>
                <div className="animated fadeIn">
                    <hr></hr>
                    <div className="comment-container">
                        <div className="user-info-container">
                            <span>{moment(created_at).fromNow()}</span>
                            <p>{user.name}</p>
                            <img src={`http://localhost/images/inc/${user.sex}-avatar.svg`}></img>
                        </div>
                        <div className="comment-content">
                            <p>{body}</p>
                            {liked_by_user !== null && active !== 0 &&
                                <div className="last-comment-section">
                                    <div>
                                        <span className="float-right">{this.state.likesCount}</span>
                                        <i className={this.state.likedByUser ? "fas fa-heart liked" : "far fa-heart"} onClick={this.like.bind(this, true, null, null)}></i>
                                    </div>
                                    <div>
                                        <i className="fas fa-reply" onClick={this.reply.bind(this, id, user.name, body)}></i>
                                    </div>
                                </div>
                            }
                            {status_message !== 'undefined' &&
                                    status_message
                            }
                        </div>
                    </div>
                </div>
                {replies !== null && replies !== undefined ? replies.map((reply, index) => {
                    if(this.state.replyLikes[index] !== undefined) {
                        return (
                            <div key={index} className="col-11 reply-comment animated fadeIn">
                                <div className="comment-container">
                                    <div className="user-info-container">
                                        <p className="reply-info text-info d-sm-inline-flex d-none">در پاسخ به دیدگاه کاربر {user.name}</p>
                                        <span>{moment(reply.created_at).fromNow()}</span>
                                        <p>{reply.user.name}</p>
                                        <img src={`http://localhost/images/inc/${reply.user.sex}-avatar.svg`}></img>
                                    </div>
                                    <div className="comment-content">
                                        <p>{reply.body}</p>
                                        {reply.liked_by_user !== null && active !== 0 &&
                                            <div className="last-comment-section">
                                                <div>
                                                    <span className="float-right">{this.state.replyLikes[index].likesCount}</span>
                                                    <i className={this.state.replyLikes[index].likedByUser ? "fas fa-heart liked" : "far fa-heart"} onClick={this.like.bind(this, false, reply.id, index)}></i>
                                                </div>
                                            </div>
                                        }
                                        {status_message !== 'undefined' &&
                                                status_message
                                        }
                                    </div>
                                </div>
                            </div>
                        )
                    }
                }) : null}
            </div>


        )
    }
}
