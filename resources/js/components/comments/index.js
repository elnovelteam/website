import React, { Component } from 'react'
import Container from './Container';

export default class CommentsContainer extends Component {
    render() {
        let {apiKey, getURL, postURL, likeURL} = this.props;
        return (
            <Container apiKey={apiKey} getURL={getURL} postURL={postURL} likeURL={likeURL}/>
        )
    }
}
