import React, { Component } from 'react'
import Comments from './components/Comments';
import WriteComment from './components/WriteComment';
import Axios from 'axios';
import { Squares } from 'react-activity';
import 'react-activity/dist/react-activity.css';

export default class Container extends Component {
    constructor(props) {
        super(props);
        this.state = {
            comments: [],
            user: null,
            nextPage: 1,
            hasMore: true,
            activity: null,
            loggedIn: this.props.apiKey ? true : false,
            replyTo: null,
            commentUser: null,
            commentBody: null
        }
    }

    setComments = (comments) => {
        this.setState({
            comments: comments
        });
    }

    handleLoadMore = () => {
        let targetURL = this.state.loggedIn ? `${this.props.getURL}?page=${this.state.nextPage}&api_token=${this.props.apiKey}` : `${this.props.getURL}?page=${this.state.nextPage}`;
        if(this.state.activity === null) {
            this.setState({
                activity: <Squares />
            });
        }
        Axios.get(targetURL).then(res => {
            let { data, current_page, last_page } = res.data.comments;
            let { user } = res.data;
            this.setState(prevState => ({
                comments: [...prevState.comments, ...data],
                hasMore: (current_page === last_page) ? false : true,
                nextPage: current_page + 1,
                user: (prevState.user) ? prevState.user : user,
                activity: null
            }));
        });
    }

    setReplyId = (replyId, commentUser, commentBody) => {
        this.setState({
            replyTo: replyId,
            commentUser: commentUser,
            commentBody: commentBody
        });
    }

    render() {
        let { comments, user, hasMore, activity, replyTo, commentUser, commentBody, loggedIn } = this.state;
            return (
                <div className="comment-section-container">
                    { activity }
                    <Comments comments={comments} hasMore={hasMore} handleLoadMore={this.handleLoadMore} activity={activity} apiKey={this.props.apiKey} setReplyId={this.setReplyId} likeURL={this.props.likeURL} />
                    <WriteComment URL={this.props.postURL} comments={comments} loggedIn={loggedIn} user={user} sendComments={this.setComments} apiKey={this.props.apiKey} replyTo={replyTo} commentUser={commentUser} commentBody={commentBody} />
                </div>
            )
    }
}
