import React, { Component } from 'react'
import { Provider } from 'react-redux';
import { createStore } from "redux";
import reducers from './reducers';
import RequestForm from './RequestForm';

export default class RequestFormContainer extends Component {

    store = createStore(reducers, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
    );

    render() {
        return (
            <Provider store={this.store}>
                <RequestForm quizUrl={this.props.Quizzes} Email={this.props.Email} FinalURL={this.props.FinalURL}/>
            </Provider>
        )
    }
}

