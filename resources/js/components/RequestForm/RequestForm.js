import React, { Component } from 'react';
import { connect } from 'react-redux';
import QuizForm from './compnents/QuizForm';
import StoryForm from './compnents/StoryForm';
import LetterForm from './compnents/LetterForm';
import RulesForm from './compnents/RulesForm';
import { getQuiz, setAnswerId } from './actions';
import axios from 'axios';

class RequestForm extends Component {
    constructor(props) {
        super(props)
        if (this.props.quizUrl) {
            this.props.quizUrl.map((url, i) => {
                axios.get(`${url[0]}?user=${this.props.Email}`).then(res => {
                    this.props.getQuiz(res.data, i)
                    this.props.setAnswerId(res.data, i)
                });
            });
        }
    }
    render() {
        let { chapters, quizUrl, Email, FinalURL } = this.props;
        return (
            <div className="col-12 float-right mb-4">
            {chapters.map((chapter, i) => {
                if(i === 0) {
                    return <QuizForm URL={quizUrl[i]} Email={Email} key={i} id={i} {...chapter} />
                }
                else if(i === 1) {
                    return <StoryForm URL={quizUrl[i]} Email={Email} key={i} id={i} {...chapter} />
                }
                else if(i === 2) {
                    return <LetterForm URL={quizUrl[i]} Email={Email} key={i} id={i} {...chapter} />
                }
                else if(i === 3) {
                    return <RulesForm URL={FinalURL} Email={Email} key={i} id={i} {...chapter} />
                }
            })}
            </div>
        )
    }
}

const mapStateToProps = state => ({
    chapters: state.RequestFormReducer
});

const mapDispatchToProps = dispatch => ({
    getQuiz: (data, chapterId) => dispatch(getQuiz(data, chapterId)),
    setAnswerId: (data, chapterId) => dispatch(setAnswerId(data, chapterId))
});

export default connect(mapStateToProps, mapDispatchToProps)(RequestForm);