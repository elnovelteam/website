import { combineReducers } from "redux";
import RequestFormReducer from './RequestFormReducer';

export default combineReducers({
    RequestFormReducer
});