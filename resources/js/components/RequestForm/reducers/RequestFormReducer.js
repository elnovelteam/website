import {
    EDIT_QUIZ_INPUT,
    EDIT_QUIZ_SELECT,
    ACCEPT_RULES,
    GET_QUIZ,
    CHANGE_CHAPTER,
    SET_VALIDATION,
    SET_ANSWER_ID
} from '../actions/actiontypes';

const chapters = [
    {
        current: true,
        quiz: null,
        answers: []
    },
    {
        current: false,
        validated: false,
        quiz: null,
        answers: []
    },
    {
        current: false,
        validated: false,
        quiz: null,
        answers: []
    },
    {
        current: false,
        title: 'بررسی و تایید شرایط و ضوابط ',
        rules: {
            short_description: '',
            url: '',
            accepted: false
        }
    },
]

const RequestFormReducer = (state=chapters, action) => {
    switch(action.type) {
        case EDIT_QUIZ_INPUT:
            let newStateInput = null
            state[action.chapterId].answers.map((answer, i) => {
                if(answer.questionId === action.questionId) {
                    newStateInput = Object.assign({}, state, {
                        [action.chapterId]: Object.assign({}, state[action.chapterId], {
                            answers: Object.values(Object.assign({}, state[action.chapterId].answers, {
                                [i]: Object.assign({}, state[action.chapterId].answers[i], {
                                    answerText: action.value,
                                    quizId: action.quizId,
                                    questionId: action.questionId
                                })
                            }))
                        })
                    })
                }
            })
            return Object.values(newStateInput);

        case EDIT_QUIZ_SELECT:
            let newStateSelect = null
            state[action.chapterId].answers.map((answer, i) => {
                if(answer.questionId === action.questionId) {
                    newStateSelect = Object.assign({}, state, {
                        [action.chapterId]: Object.assign({}, state[action.chapterId], {
                            answers: Object.values(Object.assign({}, state[action.chapterId].answers, {
                                [i]: Object.assign({}, state[action.chapterId].answers[i], {
                                    answerId: action.value,
                                    quizId: action.quizId,
                                    questionId: action.questionId
                                })
                            }))
                        })
                    })
                }
            })
            return Object.values(newStateSelect);

        case ACCEPT_RULES:
            return Object.values(Object.assign({}, state, {
                [action.chapterId]: Object.assign({}, state[action.chapterId], {
                    rules: Object.assign({}, state[action.chapterId].rules, {
                        accepted: action.value
                    })
                })
            }))

        case GET_QUIZ:
            return Object.values(Object.assign({}, state, {
                [action.chapterId]: Object.assign({}, state[action.chapterId], {
                    quiz: action.data
                })
            }))

        case CHANGE_CHAPTER:
            return Object.values(Object.assign({}, state, {
                [action.currentChapter]: Object.assign({}, state[action.currentChapter], {
                    current: false
                }),
                [action.selectedChapter]: Object.assign({}, state[action.selectedChapter], {
                    current: true
                })
            }))

        case SET_VALIDATION:
            return Object.values(Object.assign({}, state, {
                [action.chapterId]: Object.assign({}, state[action.chapterId], {
                    validated: action.value
                })
            }))

        case SET_ANSWER_ID:
            let answersArray = [];
            let {questions, answers, id} = action.data;
            for (let i = 0; i < questions.length; i++) {
                let defs = {
                    quizId: id,
                    questionId: questions[i].id,
                    answerId: null,
                    answerText: null
                };
                if (typeof(answers) !== 'undefined') {
                    answers.map((answer) => {
                        if (answer.quiz_question_id === questions[i].id) {
                            defs.answerId = answer.quiz_option_id;
                            defs.answerText = answer.text_answer;
                        }
                    });
                }
                answersArray.push(defs)
            }
            return Object.values(Object.assign({}, state, {
                [action.chapterId]: Object.assign({}, state[action.chapterId], {
                    answers: answersArray
                })
            }))

        default:
            return state
    }
}

export default RequestFormReducer;