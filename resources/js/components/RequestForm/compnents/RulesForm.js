import React, { Component } from 'react'
import { connect } from 'react-redux';
import { acceptRules, changeChapter } from '../actions';
import Buttons from './Buttons';
import Chapters from './Chapters';
import Axios from 'axios';

class RulesForm extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isChecked: false,
            isAccepted: null
        }
    }
    preChapter = () => {
        let { chapterId, changeChapter } = this.props;
        changeChapter(chapterId, chapterId - 1);
    }

    onToggle = () => {
        let { chapterId, acceptRules } = this.props;
        this.setState({
            isChecked: !this.state.isChecked
        })
        acceptRules(chapterId, !this.state.isChecked)
    }

    submitForm = () => {
        let { rules, Email, URL } = this.props;
        if(rules.accepted) {
            this.setState({
                isAccepted: null
            })
            axios.post(`${URL}?user=${Email}`).then(res => {
                window.location.replace(res.data.redirect_uri)
            });
        } else {
            this.setState({
                isAccepted: (
                    <h5 className="validation-message">برای نهایی کردن درخواست باید با قوانین و شرایط داستان نویسان موافقت نمایید</h5>
                )
            })
        }
    }

    changeChapterBack = (selectedChapterId) => {
        let { changeChapter, chapterId } = this.props;
        changeChapter(chapterId, selectedChapterId);
    }

    render() {
        let { title, short_description, url, accepted, rulesChapter, chapterId } = this.props;
        return (
            <div className={`${rulesChapter.current ? "col-12 current-chapter animated fadeIn" : "col-12 disable-chapter animated fadeIn"}`}>
                <Chapters chapterId={chapterId} changeChapterBack={this.changeChapterBack} />
                <div className="quiz-container col-12 float-left">
                    {this.state.isAccepted}
                    <h4>{title}</h4>
                    <p>{short_description}</p>
                    <input className="mr-2" type="checkbox" id="accept" checked={this.state.isChecked} onChange={this.onToggle}/>
                    <label htmlFor="accept">با <a href={url}>قوانین داستان نویسان</a> در الناول موافقم</label>
                    <Buttons chapterId={chapterId} preChapter={this.preChapter} nextChapter={this.submitForm}/>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state, ownProps) => ({
    title: state.RequestFormReducer[3].title,
    short_description: state.RequestFormReducer[3].rules.short_description,
    url: state.RequestFormReducer[3].rules.url,
    accepted: state.RequestFormReducer[3].rules.accepted,
    rules: state.RequestFormReducer[3].rules,
    rulesChapter: state.RequestFormReducer[3],
    chapterId: ownProps.id
});

const mapDispatchToProps = dispatch => ({
    acceptRules: (chapterId, value) => dispatch(acceptRules(chapterId, value)),
    changeChapter: (currentChapter, selectedChapter) => dispatch(changeChapter(currentChapter, selectedChapter))
});

export default connect(mapStateToProps, mapDispatchToProps)(RulesForm);
