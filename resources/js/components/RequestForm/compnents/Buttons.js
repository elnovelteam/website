import React, { Component } from 'react'

export default class Buttons extends Component {
    render() {
        if(this.props.chapterId === 0) {
            return (
                <div className="col-12 float-left request-form-buttons-container mb-4 mt-4">
                    <button onClick={this.props.nextChapter} className=" btn p-1 float-right"> <i className="far fa-hand-point-right"></i><span className="mx-2">مرحله بعد</span></button>
                </div>
            )
        } else if(this.props.chapterId === 3) {
            return (
                <div className="col-12 float-left request-form-buttons-container mb-4 mt-4">
                    <button onClick={this.props.preChapter} className="btn p-1 float-left"><span className="mx-2">مرحله قبل</span><i className="far fa-hand-point-left"></i> </button>
                    <button onClick={this.props.nextChapter} className=" btn p-1 float-right"> <i className="far fa-hand-point-right"></i><span className="mx-2">ارسال فرم</span></button>
                </div>
            )
        } else {
            return (
                <div className="col-12 float-left request-form-buttons-container mb-4 mt-4">
                    <button onClick={this.props.preChapter} className="btn p-1 float-left"><span className="mx-2">مرحله قبل</span><i className="far fa-hand-point-left"></i> </button>
                    <button onClick={this.props.nextChapter} className=" btn p-1 float-right"> <i className="far fa-hand-point-right"></i><span className="mx-2">مرحله بعد</span></button>
                </div>
            )
        }
    }
}
