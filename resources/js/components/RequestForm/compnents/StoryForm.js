import React, { Component } from 'react'
import { connect } from 'react-redux';
import QuizItem from './QuizItem';
import { editQuizInput, changeChapter, setValidation } from '../actions';
import { getWordsCount } from '../helpers';
import Buttons from './Buttons';
import Chapters from './Chapters';

class StoryForm extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isEmptyMessage: null,
        }
    }

    nextChapter = () => {
        let { chapterId, changeChapter, setValidation, answers, questions, URL, Email } = this.props;
        for (let index = 0; index < answers.length; index++) {
            if(answers[index].answerId === null && answers[index].answerText === null && questions[index].required) {
                this.setState({
                    isEmptyMessage: (<h5 className="validation-message">لطفاً فیلد مربوطه را پر نمایید</h5>)
                })
                setValidation(chapterId, false)
                break;
            }
            else if( answers[index].answerText !== null && getWordsCount(answers[index].answerText) < questions[index].min) {
                this.setState({
                    isEmptyMessage: (<h5 className="validation-message">حداقل تعداد کلمات برای این قسمت {questions[index].min} کلمه میباشد</h5>)
                })
                setValidation(chapterId, false)
                break;
            }
            else if( answers[index].answerText !== null && getWordsCount(answers[index].answerText) > questions[index].max) {
                this.setState({
                    isEmptyMessage: (<h5 className="validation-message">حداکثر تعداد کلمات برای این قسمت {questions[index].max} کلمه میباشد</h5>)
                })
                setValidation(chapterId, false)
                break;
            }
            else {
                this.setState({
                    isEmptyMessage: null
                })
                setValidation(chapterId, true)
                changeChapter(chapterId, chapterId + 1)
                axios.post(`${URL[1]}?user=${Email}`, {answers: answers}).then(res => {
                });
            }   
        }
    }

    changeChapterBack = (selectedChapterId) => {
        let { chapterId, changeChapter, setValidation, answers, questions } = this.props;
        for (let index = 0; index < answers.length; index++) {
            if(answers[index].answerId === null && answers[index].answerText === null && questions[index].required) {
                setValidation(chapterId, false)
                changeChapter(chapterId, chapterId - 1)
                break;
            }
            else if( answers[index].answerText !== null && getWordsCount(answers[index].answerText) < questions[index].min) {
                setValidation(chapterId, false)
                changeChapter(chapterId, chapterId - 1)
                break;
            }
            else if( answers[index].answerText !== null && getWordsCount(answers[index].answerText) > questions[index].max) {
                setValidation(chapterId, false)
                changeChapter(chapterId, chapterId - 1)
                break;
            }
            else {
                this.setState({
                    isEmptyMessage: null
                })
                setValidation(chapterId, true)
                changeChapter(chapterId, chapterId - 1)
            }
        }
    }

    changeChapterForward = (selectedChapterId) => {
        let { chapterId, changeChapter, setValidation, answers, questions, chapters, URL, Email } = this.props;
        for (let index = 0; index < answers.length; index++) {
            if(answers[index].answerId === null && answers[index].answerText === null && questions[index].required) {
                this.setState({
                    isEmptyMessage: (<h5 className="validation-message">لطفاً فیلد مربوطه را پر نمایید</h5>)
                })
                setValidation(chapterId, false)
                break;
            }
            else if( answers[index].answerText !== null && getWordsCount(answers[index].answerText) < questions[index].min) {
                this.setState({
                    isEmptyMessage: (<h5 className="validation-message">حداقل تعداد کلمات برای این قسمت {questions[index].min} کلمه میباشد</h5>)
                })
                setValidation(chapterId, false)
                break;
            }
            else if( answers[index].answerText !== null && getWordsCount(answers[index].answerText) > questions[index].max) {
                this.setState({
                    isEmptyMessage: (<h5 className="validation-message">حداکثر تعداد کلمات برای این قسمت {questions[index].max} کلمه میباشد</h5>)
                })
                setValidation(chapterId, false)
                break;
            }
            else {
                this.setState({
                    isEmptyMessage: null
                })
                setValidation(chapterId, true)
                if(chapterId === selectedChapterId -1 ) {
                    changeChapter(chapterId, selectedChapterId)
                    axios.post(`${URL[1]}?user=${Email}`, {answers: answers}).then(res => {
                    });
                } else {
                    if(chapters[selectedChapterId - 1].validated) {
                        changeChapter(chapterId, selectedChapterId)
                        axios.post(`${URL[1]}?user=${Email}`, {answers: answers}).then(res => {
                        });
                    } else {
                        this.setState({
                            isEmptyMessage: (<h5 className="validation-message">لطفا ابتدا مرحله سوم درخواست را تکمیل فرمایید</h5>)
                        })
                    }
                }
            }
        }
    }

    render() {
        let { storyChapter, chapterId, quiz } = this.props;
        return (
            <div className={`${storyChapter.current ? "col-12 current-chapter animated fadeIn" : "col-12 disable-chapter animated fadeIn"}`}>
                <Chapters chapterId={chapterId} changeChapterBack={this.changeChapterBack} changeChapterForward={this.changeChapterForward}/>
                <div className="quiz-container col-12 float-left">
                    {this.state.isEmptyMessage}
                    <h4>{quiz.title}</h4>
                    <QuizItem chapterId={chapterId} />
                    <Buttons chapterId={chapterId} preChapter={this.changeChapterBack} nextChapter={this.nextChapter}/>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state, ownProps) => ({
    storyChapter: state.RequestFormReducer[1],
    chapters: state.RequestFormReducer,
    chapterId: ownProps.id,
    quiz: (state.RequestFormReducer[1].quiz) ? state.RequestFormReducer[1].quiz : {},
    questions: (state.RequestFormReducer[1].quiz) ? state.RequestFormReducer[1].quiz.questions : [],
    answers: state.RequestFormReducer[1].answers,
});

const mapDispatchToProps = dispatch => ({
    editQuizInput: (questionId, quizId, chapterId, value) => dispatch(editQuizInput(questionId, quizId, chapterId, value)),
    changeChapter: (currentChapter, selectedChapter) => dispatch(changeChapter(currentChapter, selectedChapter)),
    setValidation: (chapterId, value) => dispatch(setValidation(chapterId, value))
});

export default connect(mapStateToProps, mapDispatchToProps)(StoryForm);
