import React, { Component } from 'react'
import { connect } from 'react-redux';
import { editQuizInput, editQuizSelect } from '../actions';
import { getWordsCount } from '../helpers';

class QuizItem extends Component {
    constructor(props) {
        super(props)
    }

    onEditInput = (id, quizId, e) => {
        let { editQuizInput, chapterId } = this.props;
        editQuizInput(id, quizId, chapterId, e.target.value);
    }

    onEditSelect = (id, quizId, e) => {
        let { editQuizSelect, chapterId } = this.props;
        editQuizSelect(id, quizId, chapterId, Number(e.target.value));
    }

    onKeyDown = (id, min, max, e) => {
        let { answers } = this.props;
        answers.map((answer, i) => {
            if(id === answer.questionId) {
                if(answer.answerText !== null) {
                    let wordsCount = getWordsCount(answer.answerText);
                    if(wordsCount > max　&& e.keyCode !== 8 && e.keyCode !== 9 && e.keyCode !== 37 && e.keyCode !== 38 && e.keyCode !== 39 && e.keyCode !== 40 ) {
                        e.preventDefault();
                        e.target.classList.replace('unmaxed-txtarea', 'maxed-txtarea')
                    } else {
                        e.target.classList.replace('maxed-txtarea', 'unmaxed-txtarea')
                    }
                }
            }
        })
    }

    render() {
        let { questions, chapterId, answers } = this.props;
        return (
            <div className="col-12 float-right questions-container">
                {questions.map((question, index) => {
                    let radioName = `radioName${question.id}`;
                    if(question.type === "YESNO" || question.type === "TEST") {
                        return (
                            <div className="col-lg-6 col-md-6 col-12 float-right" key={index}>
                                <p>{question.required ? `* ${question.index}- ${question.text}` : `${question.index}- ${question.text}`}</p>
                                <div>
                                    {question.options.map((option, i) => {
                                        let radioId = `radioId${option.id}`;
                                        return (
                                            <div className="col-lg-4 col-md-5 col-6 float-right" key={i}>
                                                <input
                                                    type="radio" 
                                                    id={radioId} 
                                                    name={radioName} 
                                                    value={option.id} 
                                                    onChange={this.onEditSelect.bind(this, question.id, question.quiz_id)} 
                                                    checked={answers[index] !== undefined ? answers[index].answerId === option.id : false}
                                                />
                                                <label htmlFor={radioId}>{option.text}</label>
                                            </div>
                                        )
                                    })}
                                </div>
                            </div>
                        )
                    }
                    else if(question.type === "DESCRIPTIVE") {
                        return (
                            <div className={chapterId < 1 ? "col-lg-8 col-md-8 col-12 float-right" : "col-12 float-right"} key={index}>
                                <p>{question.required ? `* ${question.index}- ${question.text}` : `${question.index}- ${question.text}`}</p>
                                <textarea
                                    className="form-control unmaxed-txtarea"
                                    onChange={this.onEditInput.bind(this, question.id, question.quiz_id)}
                                    onKeyDown={this.onKeyDown.bind(this, question.id, question.min, question.max)}
                                    placeholder={`حداکثر ${question.max} کلمه`}
                                    value={answers[index] !== undefined ? answers[index].answerText === null ? '' : answers[index].answerText : ''}
                                ></textarea>
                            </div>
                        )
                    }
                })}
            </div>
        )
    }
}

const mapStateToProps = (state, ownProps) => ({
    questions: (state.RequestFormReducer[ownProps.chapterId].quiz) ? state.RequestFormReducer[ownProps.chapterId].quiz.questions : [],
    answers: state.RequestFormReducer[ownProps.chapterId].answers,
    chapterId: ownProps.chapterId
});

const mapDispatchToProps = dispatch => ({
    editQuizInput: (questionId, quizId, chapterId, value) => dispatch(editQuizInput(questionId, quizId, chapterId, value)),
    editQuizSelect: (questionId, quizId, chapterId, value) => dispatch(editQuizSelect(questionId, quizId, chapterId, value)),
});

export default connect(mapStateToProps, mapDispatchToProps)(QuizItem);
