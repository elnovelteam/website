import React, { Component } from 'react'
import { connect } from 'react-redux';
import QuizItem from './QuizItem';
import { changeChapter } from '../actions';
import { getWordsCount } from '../helpers';
import Buttons from './Buttons';
import Chapters from './Chapters';
import axios from 'axios';

class QuizForm extends Component {
    constructor(props) {
        super(props)
        this.state = {
            validationMessage: null,
        }
    }

    nextChapter = () => {
        let { chapterId, changeChapter, answers, questions, URL, Email } = this.props;
        for (let index = 0; index < answers.length; index++) {
            if(answers[index].answerId === null && (answers[index].answerText === null || answers[index].answerText.length === 0) && questions[index].required) {
                this.setState({
                    validationMessage: (<h5 className="validation-message">لطفاً تمامی سوالات مورد نیاز را تکمیل فرمایید</h5>)
                })
                break;
            }
            else if(questions[index].type === "DESCRIPTIVE" && answers[index].answerText !== null && getWordsCount(answers[index].answerText) < questions[index].min) {
                this.setState({
                validationMessage: (<h5 className="validation-message"> حداقل تعداد کلمات برای سوال شماره {questions[index].index} ، {questions[index].min} کلمه میباشد</h5>)
                })
                break;
            }
            else if(questions[index].type === "DESCRIPTIVE" && answers[index].answerText !== null && getWordsCount(answers[index].answerText) > questions[index].max) {
                this.setState({
                    validationMessage: (<h5 className="validation-message"> حداکثر تعداد کلمات برای سوال شماره {questions[index].index} ، {questions[index].max} کلمه میباشد</h5>)
                    })
                break;
            }
            else if(index === answers.length - 1) {
                this.setState({
                    validationMessage: null
                })
                changeChapter(chapterId, chapterId + 1)
                axios.post(`${URL[1]}?user=${Email}`, {answers: answers}).then(res => {
                });
            }
        }
    }

    changeChapterBack = (selectedChapterId) => {
        let { changeChapter, chapterId } = this.props;
        changeChapter(chapterId, selectedChapterId);
    }

    changeChapterForward = (selectedChapterId) => {
        let { changeChapter, chapterId, chapters, answers, questions, URL, Email } = this.props;
        for (let index = 0; index < answers.length; index++) {
            if(answers[index].answerId === null && (answers[index].answerText === null || answers[index].answerText.length === 0) && questions[index].required) {
                this.setState({
                    validationMessage: (<h5 className="validation-message">لطفاً تمامی سوالات مورد نیاز را تکمیل فرمایید</h5>)
                })
                break;
            }
            else if(questions[index].type === "DESCRIPTIVE" && answers[index].answerText !== null && getWordsCount(answers[index].answerText) < questions[index].min) {
                this.setState({
                validationMessage: (<h5 className="validation-message"> حداقل تعداد کلمات برای سوال شماره {questions[index].index} ، {questions[index].min} کلمه میباشد</h5>)
                })
                break;
            }
            else if(questions[index].type === "DESCRIPTIVE" && answers[index].answerText !== null && getWordsCount(answers[index].answerText) > questions[index].max) {
                this.setState({
                    validationMessage: (<h5 className="validation-message"> حداکثر تعداد کلمات برای سوال شماره {questions[index].index} ، {questions[index].max} کلمه میباشد</h5>)
                    })
                    break;
            }
            else if(index === answers.length - 1) {
                this.setState({
                    validationMessage: null
                })
                if(chapterId === selectedChapterId -1 ) {
                    changeChapter(chapterId, selectedChapterId)
                    axios.post(`${URL[1]}?user=${Email}`, {answers: answers}).then(res => {
                    });
                } else {
                    for (let index = 1; index < selectedChapterId - chapterId; index++) {
                        if(chapters[index].validated === false) {
                            this.setState({
                                validationMessage: (<h5 className="validation-message"> لطفا ابتدا مرحله دوم آزمون را به اتمام برسانید</h5>)
                            })
                            break;
                        }
                        else if(selectedChapterId === 2) {
                            changeChapter(chapterId, selectedChapterId)
                            axios.post(`${URL[1]}?user=${Email}`, {answers: answers}).then(res => {
                            });
                        }
                        else if(index === 2) {
                            changeChapter(chapterId, selectedChapterId)
                            axios.post(`${URL[1]}?user=${Email}`, {answers: answers}).then(res => {
                            });
                        }
                    }
                }
            }
        }
    }

    render() {
        let { questions, chapterId, current, quizChapter, quiz } = this.props;
        return (
            <div className={`${quizChapter.current ? "col-12 current-chapter animated fadeIn" : "col-12 disable-chapter animated fadeIn"}`}>
                <Chapters chapterId={chapterId} changeChapterBack={this.changeChapterBack} changeChapterForward={this.changeChapterForward}/>
                <div className="quiz-container col-12 float-left">
                    {this.state.validationMessage}
                    <h4>{quiz.title}</h4>
                    <QuizItem chapterId={chapterId} />
                    <Buttons chapterId={chapterId} nextChapter={this.nextChapter}/>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        answers: state.RequestFormReducer[0].answers,
        questions: (state.RequestFormReducer[0].quiz) ? state.RequestFormReducer[0].quiz.questions : [],
        quiz: (state.RequestFormReducer[0].quiz) ? state.RequestFormReducer[0].quiz : {},
        current: state.RequestFormReducer[0].current,
        quizChapter: state.RequestFormReducer[0],
        chapters: state.RequestFormReducer,
        chapterId: ownProps.id
    }
};

const mapDispatchToProps = dispatch => ({
    changeChapter: (currentChapter, selectedChapter) => dispatch(changeChapter(currentChapter, selectedChapter))
});

export default connect(mapStateToProps, mapDispatchToProps)(QuizForm);