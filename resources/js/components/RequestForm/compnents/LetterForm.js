import React, { Component } from 'react'
import { connect } from 'react-redux';
import QuizItem from './QuizItem';
import { editQuizInput, changeChapter, setValidation } from '../actions';
import { getWordsCount } from '../helpers';
import Buttons from './Buttons';
import Chapters from './Chapters';

class LetterForm extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isEmptyMessage: null,
        }
    }

    preChapter = () => {
        let { chapterId, changeChapter, setValidation, answers, questions } = this.props;
        for (let index = 0; index < answers.length; index++) {
            if( answers[index].answerText !== null && getWordsCount(answers[index].answerText) > questions[index].max) {
                setValidation(chapterId, false)
                changeChapter(chapterId, chapterId - 1)
                break;
            }
            else {
                this.setState({
                    isEmptyMessage: null
                })
                setValidation(chapterId, true)
                changeChapter(chapterId, chapterId - 1)
            }
        }
    }

    changeChapterBack = (selectedChapterId) => {
        let { chapterId, changeChapter, setValidation, answers, questions } = this.props;
        for (let index = 0; index < answers.length; index++) {
            if( answers[index].answerText !== null && getWordsCount(answers[index].answerText) > questions[index].max) {
                setValidation(chapterId, false)
                changeChapter(chapterId, selectedChapterId)
                break;
            }
            else {
                this.setState({
                    isEmptyMessage: null
                })
                setValidation(chapterId, true)
                changeChapter(chapterId, selectedChapterId)
            }
        }
    }

    changeChapterForward = (selectedChapterId) => {
        let { chapterId, changeChapter, setValidation, answers, questions, URL, Email } = this.props;
        for (let index = 0; index < answers.length; index++) {
            if( answers[index].answerText !== null && getWordsCount(answers[index].answerText) > questions[index].max) {
                this.setState({
                    isEmptyMessage: (<h5 className="validation-message">حداکثر تعداد کلمات برای این قسمت {questions[index].max} کلمه میباشد</h5>)
                })
                setValidation(chapterId, false)
                break;
            }
            else {
                this.setState({
                    isEmptyMessage: null
                })
                setValidation(chapterId, true)
                changeChapter(chapterId, chapterId + 1)
                axios.post(`${URL[1]}?user=${Email}`, {answers: answers}).then(res => {
                });
            }
        }
    }

    render() {
        let { letterChapter, chapterId, quiz } = this.props;
        return (
            <div className={`${letterChapter.current ? "col-12 current-chapter animated fadeIn" : "col-12 disable-chapter animated fadeIn"}`}>
                <Chapters chapterId={chapterId} changeChapterBack={this.changeChapterBack} changeChapterForward={this.changeChapterForward}/>
                <div className="quiz-container col-12 float-left">
                    {this.state.isEmptyMessage}
                    <h4>{quiz.title}</h4>
                    <QuizItem chapterId={chapterId} />
                    <Buttons chapterId={chapterId} preChapter={this.preChapter} nextChapter={this.changeChapterForward}/>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state, ownProps) => ({
    letterChapter: state.RequestFormReducer[2],
    chapterId: ownProps.id,
    quiz: (state.RequestFormReducer[2].quiz) ? state.RequestFormReducer[2].quiz : {},
    questions: (state.RequestFormReducer[2].quiz) ? state.RequestFormReducer[2].quiz.questions : [],
    answers: state.RequestFormReducer[2].answers,
});

const mapDispatchToProps = dispatch => ({
    editQuizInput: (questionId, quizId, chapterId, value) => dispatch(editQuizInput(questionId, quizId, chapterId, value)),
    changeChapter: (currentChapter, selectedChapter) => dispatch(changeChapter(currentChapter, selectedChapter)),
    setValidation: (chapterId, value) => dispatch(setValidation(chapterId, value))
});

export default connect(mapStateToProps, mapDispatchToProps)(LetterForm);
