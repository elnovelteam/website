import React, { Component } from 'react'

export default class Chapters extends Component {
    render() {
        let chapters = [        
            <button><i className="far fa-question-circle"></i><span>فرم سوالات</span></button>,
            <button><i className="fas fa-scroll"></i><span>ارسال یک داستان</span></button>,
            <button><i className="fas fa-feather"></i><span>یک نامه به الناول</span></button>,
            <button><i className="fas fa-handshake"></i><span>بررسی و تأیید شرایط و ضوابط</span></button>
        ]
        return (
            <div className="chapters-container">
                {
                    chapters.map((chapter, i) => {
                        if(i === this.props.chapterId) {
                            return React.createElement("button",
                                {key: i, className: "current-chapter-button"},
                                React.createElement("i", {className: `${chapter.props.children[0].props.className} current-chapter-icon`}),
                                React.createElement("span", null, `${chapter.props.children[1].props.children}`))
                        }
                        if(i < this.props.chapterId) {
                            return React.createElement("button",
                                {key: i, onClick: () => {this.props.changeChapterBack(i)}},
                                React.createElement("i", {className: `${chapter.props.children[0].props.className} finished-chapter`}),
                                React.createElement("span", null, `${chapter.props.children[1].props.children}`))
                        } else {
                            return React.createElement("button",
                                {key: i, onClick: () => {this.props.changeChapterForward(i)}},
                                React.createElement("i", {className: `${chapter.props.children[0].props.className}`}),
                                React.createElement("span", null, `${chapter.props.children[1].props.children}`))
                        }
                    })
                }
            </div>
        )
    }
}