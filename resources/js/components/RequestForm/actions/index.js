import {
    EDIT_QUIZ_INPUT,
    EDIT_QUIZ_SELECT, 
    ACCEPT_RULES, 
    GET_QUIZ, 
    CHANGE_CHAPTER,
    SET_VALIDATION,
    SET_ANSWER_ID
} from './actiontypes';

export const editQuizInput = (questionId, quizId, chapterId, value) => ({
    type: EDIT_QUIZ_INPUT,
    questionId: questionId,
    quizId: quizId,
    chapterId: chapterId,
    value: value
})

export const editQuizSelect = (questionId, quizId, chapterId, value) => ({
    type: EDIT_QUIZ_SELECT,
    questionId: questionId,
    quizId: quizId,
    chapterId: chapterId,
    value: value
})

export const acceptRules = (id, value) => ({
    type: ACCEPT_RULES,
    chapterId: id,
    value: value
})

export const getQuiz = (data, chapterId) => ({
    type: GET_QUIZ,
    data: data,
    chapterId: chapterId
})

export const changeChapter = (currentChapter, selectedChapter) => ({
    type: CHANGE_CHAPTER,
    currentChapter: currentChapter,
    selectedChapter: selectedChapter
})

export const setValidation = (chapterId, value) => ({
    type: SET_VALIDATION,
    chapterId: chapterId,
    value: value
})

export const setAnswerId = (data, chapterId) => ({
    type: SET_ANSWER_ID,
    chapterId: chapterId,
    data: data
})