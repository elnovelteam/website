import React, { Component } from 'react'

export default class AudioPlayer extends Component {
    constructor(props) {
        super(props);
        let audio = document.getElementById(this.props.AudioID)
        audio.preload = "metadata";
        audio.ontimeupdate = this.audioTimeUpdate;
        audio.onloadedmetadata = this.handleAudioMetadata;
        audio.onpause = this.handleToggle;
        audio.onplay = this.handleToggle;
        audio.onprogress = this.handleProgress;
        this.state = {
            active: false,
            range: 0,
            max: 0,
            percentage: 0 + 'px',
            hoverwidth: 0,
            percent: 0,
            volumeIcon: true,
            speed: 1.0,
            volume: 1.0,
            animation: null
        };
    }
    toggleAudioPlay = () => {
        this.setState(prevState => ({
            active: ! prevState.active
        }));
        let audio = document.getElementById(this.props.AudioID);
        if (this.state.active) {
            audio.pause();
        } else {
            audio.play();
        }
    }
    audioTimeUpdate = (e) => {
        let {currentTime} = e.target;
        let percentage = (Math.round(currentTime) / Math.round(this.state.max)).toFixed(2) * 100;
        this.setState(prevState => ({
            range: currentTime,
            percentage: percentage + '%',
            percent: percentage.toFixed(0)
        }));
    }
    handleAudioMetadata = (e) => {
        let {duration} = e.target;
        this.setState({max: duration});
    }
    handleToggle = (e) => {
        this.setState({ active: e.type == 'play' ? true : false });
    }
    showHover = (e) => {
        let sArea = $(e.target);
        let seekBarPos = sArea.offset();
        let seekT = e.clientX - seekBarPos.left;
        this.setState({ hoverwidth: seekT + 'px'});
    }
    hideHover = () => {
        this.setState({ hoverwidth: 0 });
    }
    playAudioFromClickedPos = (e) => {
        let cX = e.clientX;
        let sArea = $(e.target);
        sArea = sArea.hasClass('bar') ? sArea.parent() : sArea;
        let audio = $(`#${this.props.AudioID}`)[0];
        let seekBarPos = sArea.offset();
        let seekT = (seekBarPos.left - cX) * -1;
        let percentage = (seekT.toFixed(0) / sArea.outerWidth()) * 100;
        audio.currentTime = (audio.duration / 100) *  percentage.toFixed(0);
        this.setState({ percentage: percentage.toFixed(0) + '%', hoverwidth: percentage.toFixed(0) + "%"});
    }
    stopAudioPlay = () => {
        let audio = $(`#${this.props.AudioID}`)[0];
        this.setState({ percentage: 0 + 'px', hoverwidth: 0});
        audio.currentTime = 0;
    }
    SoundCTRL = () => {
        let audio = $(`#${this.props.AudioID}`)[0];
        audio.volume = (this.state.volumeIcon) ? 0.0 : 1.0;
        this.setState(prevState => ({
            volumeIcon: ! prevState.volumeIcon,
            volume: (prevState.volumeIcon) ? 0.0 : 1.0
        }));
    }
    SoundSpeed = () => {
        let audio = $(`#${this.props.AudioID}`)[0];
        audio.playbackRate = (this.state.speed == 2) ? 1.0 : 2.0
        this.setState(prevState => ({
            speed: (prevState.speed == 2) ? 1.0 : 2.0,
            animation: (prevState.speed != 2) ? 'activer' : null
        }));
    }
    setVolume = (e) => {
        let val = e.target.value;
        let audio = $(`#${this.props.AudioID}`)[0];
        audio.volume = val;
        this.setState(prevState => ({
           volume: val,
           volumeIcon: (val == 0) ? false : ((val < 0.4) ? null : true)
        }));
    }
    render() {
        let { Avatar, title, subtitle } = this.props;
        let { active, percent, percentage, hoverwidth, animation, volumeIcon, volume, speed} = this.state;
        return (
            <div className="elnovel-audioplayer col-lg-8 offset-lg-2 col-md-6 offset-md-3 col-sm-8 offset-sm-2 col-12">
                <div className={`player-content ${(active) ? "active" : ""}`}>
                    <span className="title">{title} %{percent}</span>
                    <span className="subtitle">{subtitle}</span>
                    <div className="audio-slider" onClick={this.playAudioFromClickedPos} onMouseOut={this.hideHover} onMouseMove={this.showHover}>
                        <div className="bar" style={{ width: percentage}}></div>
                        <div className="s-hover" style={{ width: hoverwidth}}></div>
                    </div>
                </div>
                <div className="pleayer-over">
                    <div className={`album-art ${(active) ? "active" : ""} ${animation}`}><img src={Avatar} alt=""/></div>
                    <div className="player-controls">
                        <div className="control volume-box">
                            <i onClick={this.SoundCTRL} className={`fas ${(volumeIcon) ? 'fa-volume-up' : ((volumeIcon == null) ? 'fa-volume-down' : 'fa-volume-mute')}`}></i>
                            <div className="volume-container animated fadeIn">
                                <div className="volume-slide"><input type="range" min="0" max="1" step="0.1"
                                value={volume} className="volume-slider" onChange={this.setVolume}/></div>
                            </div>
                        </div>
                        <div className="control"><i onClick={this.toggleAudioPlay} className={`fas fa-${(active) ? "pause" : "play"}`}></i></div>
                        <div className="control"><i onClick={this.stopAudioPlay} className="fas fa-stop"></i></div>
                        <div className="control"><i onClick={this.SoundSpeed} className="fas fa-fast-forward"></i><span className="volume-speed">{speed}</span></div>
                    </div>
                </div>
            </div>
        )
    }
}
