import React, { Component } from 'react'

export default class writerProfile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            activeTab: 'books'
        }
    }

    changeActiveTab = (tab, e) => {
        this.setState({
            activeTab: tab
        })
    }

    render() {
        let { activeTab } = this.state;
        return (
            <div className="col-12 col-md-10 offset-md-1 writer-profile-tabs-container">
                <nav className="col-12 writer-tabs-top-navbar">
                    <div className={activeTab === 'books' ? "active animated heartBeat" : null} onClick={this.changeActiveTab.bind(this, "books")}>
                        <p>کتاب ها</p>
                        <i className="fas fa-book"></i>
                    </div>
                    <div className={activeTab === 'series' ? 'active animated heartBeat' : null} onClick={this.changeActiveTab.bind(this, "series")}>
                        <p>سری ها</p>
                        <i className="fas fa-box-open"></i>
                    </div>
                </nav>
                <ul className="col-12 writer-tabs-content">
                    <li className={activeTab === 'books' ? "active" : null}>
                        <div className="col-12 col-md-10">
                            <div className="col-10 offset-0 col-sm-4 col-lg-3 book-serie-podcast-result">
                                <div className="cover">
                                    <div className="toolbox">
                                        <i className="fas fa-share-alt sharethis"></i>
                                        <i className="fas fa-heart likethis"></i>
                                        <i className="far fa-bookmark bookmarkthis"></i>
                                    </div>
                                    <a><img src={"/images/book.jpg"} alt=""/></a>
                                </div>
                                <div className="info">
                                    <p className="book-title animated bounceInLeft">{"دول سگ"}</p>
                                    <p className="book-subtitle animated lightSpeedIn">{" سگ سگ سگ سگ"}</p>
                                    <div className="generes">
                                        <a href="/categories/fantasy" className="badge badge-secondary badge-pill animated rotateInUpLeft">فانتزی</a>
                                        <a href="/categories/fiction" className="badge badge-secondary badge-pill  animated rotateInUpLeft">تخیلی</a>
                                    </div>
                                </div>
                            </div>
                            <div className="col-10 offset-0 col-sm-4 col-lg-3 book-serie-podcast-result">
                                <div className="cover">
                                    <div className="toolbox">
                                        <i className="fas fa-share-alt sharethis"></i>
                                        <i className="fas fa-heart likethis"></i>
                                        <i className="far fa-bookmark bookmarkthis"></i>
                                    </div>
                                    <a><img src={"/images/book.jpg"} alt=""/></a>
                                </div>
                                <div className="info">
                                    <p className="book-title animated bounceInLeft">{"دول سگ"}</p>
                                    <p className="book-subtitle animated lightSpeedIn">{" سگ سگ سگ سگ"}</p>
                                    <div className="generes">
                                        <a href="/categories/fantasy" className="badge badge-secondary badge-pill animated rotateInUpLeft">فانتزی</a>
                                        <a href="/categories/fiction" className="badge badge-secondary badge-pill  animated rotateInUpLeft">تخیلی</a>
                                    </div>
                                </div>
                            </div>
                            <div className="col-10 offset-0 col-sm-4 col-lg-3 book-serie-podcast-result">
                                <div className="cover">
                                    <div className="toolbox">
                                        <i className="fas fa-share-alt sharethis"></i>
                                        <i className="fas fa-heart likethis"></i>
                                        <i className="far fa-bookmark bookmarkthis"></i>
                                    </div>
                                    <a><img src={"/images/book.jpg"} alt=""/></a>
                                </div>
                                <div className="info">
                                    <p className="book-title animated bounceInLeft">{"دول سگ"}</p>
                                    <p className="book-subtitle animated lightSpeedIn">{" سگ سگ سگ سگ"}</p>
                                    <div className="generes">
                                        <a href="/categories/fantasy" className="badge badge-secondary badge-pill animated rotateInUpLeft">فانتزی</a>
                                        <a href="/categories/fiction" className="badge badge-secondary badge-pill  animated rotateInUpLeft">تخیلی</a>
                                    </div>
                                </div>
                            </div>
                            <div className="col-10 offset-0 col-sm-4 col-lg-3 book-serie-podcast-result">
                                <div className="cover">
                                    <div className="toolbox">
                                        <i className="fas fa-share-alt sharethis"></i>
                                        <i className="fas fa-heart likethis"></i>
                                        <i className="far fa-bookmark bookmarkthis"></i>
                                    </div>
                                    <a><img src={"/images/book.jpg"} alt=""/></a>
                                </div>
                                <div className="info">
                                    <p className="book-title animated bounceInLeft">{"دول سگ"}</p>
                                    <p className="book-subtitle animated lightSpeedIn">{" سگ سگ سگ سگ"}</p>
                                    <div className="generes">
                                        <a href="/categories/fantasy" className="badge badge-secondary badge-pill animated rotateInUpLeft">فانتزی</a>
                                        <a href="/categories/fiction" className="badge badge-secondary badge-pill  animated rotateInUpLeft">تخیلی</a>
                                    </div>
                                </div>
                            </div>
                            <div className="col-10 offset-0 col-sm-4 col-lg-3 book-serie-podcast-result">
                                <div className="cover">
                                    <div className="toolbox">
                                        <i className="fas fa-share-alt sharethis"></i>
                                        <i className="fas fa-heart likethis"></i>
                                        <i className="far fa-bookmark bookmarkthis"></i>
                                    </div>
                                    <a><img src={"/images/book.jpg"} alt=""/></a>
                                </div>
                                <div className="info">
                                    <p className="book-title animated bounceInLeft">{"دول سگ"}</p>
                                    <p className="book-subtitle animated lightSpeedIn">{" سگ سگ سگ سگ"}</p>
                                    <div className="generes">
                                        <a href="/categories/fantasy" className="badge badge-secondary badge-pill animated rotateInUpLeft">فانتزی</a>
                                        <a href="/categories/fiction" className="badge badge-secondary badge-pill  animated rotateInUpLeft">تخیلی</a>
                                    </div>
                                </div>
                            </div>
                            <div className="col-10 offset-0 col-sm-4 col-lg-3 book-serie-podcast-result">
                                <div className="cover">
                                    <div className="toolbox">
                                        <i className="fas fa-share-alt sharethis"></i>
                                        <i className="fas fa-heart likethis"></i>
                                        <i className="far fa-bookmark bookmarkthis"></i>
                                    </div>
                                    <a><img src={"/images/book.jpg"} alt=""/></a>
                                </div>
                                <div className="info">
                                    <p className="book-title animated bounceInLeft">{"دول سگ"}</p>
                                    <p className="book-subtitle animated lightSpeedIn">{" سگ سگ سگ سگ"}</p>
                                    <div className="generes">
                                        <a href="/categories/fantasy" className="badge badge-secondary badge-pill animated rotateInUpLeft">فانتزی</a>
                                        <a href="/categories/fiction" className="badge badge-secondary badge-pill  animated rotateInUpLeft">تخیلی</a>
                                    </div>
                                </div>
                            </div>
                            <div className="col-10 offset-0 col-sm-4 col-lg-3 book-serie-podcast-result">
                                <div className="cover">
                                    <div className="toolbox">
                                        <i className="fas fa-share-alt sharethis"></i>
                                        <i className="fas fa-heart likethis"></i>
                                        <i className="far fa-bookmark bookmarkthis"></i>
                                    </div>
                                    <a><img src={"/images/book.jpg"} alt=""/></a>
                                </div>
                                <div className="info">
                                    <p className="book-title animated bounceInLeft">{"دول سگ"}</p>
                                    <p className="book-subtitle animated lightSpeedIn">{" سگ سگ سگ سگ"}</p>
                                    <div className="generes">
                                        <a href="/categories/fantasy" className="badge badge-secondary badge-pill animated rotateInUpLeft">فانتزی</a>
                                        <a href="/categories/fiction" className="badge badge-secondary badge-pill  animated rotateInUpLeft">تخیلی</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li className={activeTab === 'series' ? 'active' : null}>
                        <div className="col-12 col-md-10">
                            <div className="col-10 offset-0 col-sm-4 col-lg-3 book-serie-podcast-result">
                                <div className="cover">
                                    <div className="toolbox">
                                        <i className="fas fa-share-alt sharethis"></i>
                                        <i className="fas fa-heart likethis"></i>
                                        <i className="far fa-bookmark bookmarkthis"></i>
                                    </div>
                                    <a><img src={"/images/book-serie.jpg"} alt=""/></a>
                                </div>
                                <div className="info">
                                    <p className="book-title animated bounceInLeft">{"دول سگ"}</p>
                                    <p className="book-subtitle animated lightSpeedIn">{" سگ سگ سگ سگ"}</p>
                                    <div className="generes">
                                        <a href="/categories/fantasy" className="badge badge-secondary badge-pill animated rotateInUpLeft">فانتزی</a>
                                        <a href="/categories/fiction" className="badge badge-secondary badge-pill  animated rotateInUpLeft">تخیلی</a>
                                    </div>
                                </div>
                            </div>
                            <div className="col-10 offset-0 col-sm-4 col-lg-3 book-serie-podcast-result">
                                <div className="cover">
                                    <div className="toolbox">
                                        <i className="fas fa-share-alt sharethis"></i>
                                        <i className="fas fa-heart likethis"></i>
                                        <i className="far fa-bookmark bookmarkthis"></i>
                                    </div>
                                    <a><img src={"/images/book-serie.jpg"} alt=""/></a>
                                </div>
                                <div className="info">
                                    <p className="book-title animated bounceInLeft">{"دول سگ"}</p>
                                    <p className="book-subtitle animated lightSpeedIn">{" سگ سگ سگ سگ"}</p>
                                    <div className="generes">
                                        <a href="/categories/fantasy" className="badge badge-secondary badge-pill animated rotateInUpLeft">فانتزی</a>
                                        <a href="/categories/fiction" className="badge badge-secondary badge-pill  animated rotateInUpLeft">تخیلی</a>
                                    </div>
                                </div>
                            </div>
                            <div className="col-10 offset-0 col-sm-4 col-lg-3 book-serie-podcast-result">
                                <div className="cover">
                                    <div className="toolbox">
                                        <i className="fas fa-share-alt sharethis"></i>
                                        <i className="fas fa-heart likethis"></i>
                                        <i className="far fa-bookmark bookmarkthis"></i>
                                    </div>
                                    <a><img src={"/images/book-serie.jpg"} alt=""/></a>
                                </div>
                                <div className="info">
                                    <p className="book-title animated bounceInLeft">{"دول سگ"}</p>
                                    <p className="book-subtitle animated lightSpeedIn">{" سگ سگ سگ سگ"}</p>
                                    <div className="generes">
                                        <a href="/categories/fantasy" className="badge badge-secondary badge-pill animated rotateInUpLeft">فانتزی</a>
                                        <a href="/categories/fiction" className="badge badge-secondary badge-pill  animated rotateInUpLeft">تخیلی</a>
                                    </div>
                                </div>
                            </div>
                            <div className="col-10 offset-0 col-sm-4 col-lg-3 book-serie-podcast-result">
                                <div className="cover">
                                    <div className="toolbox">
                                        <i className="fas fa-share-alt sharethis"></i>
                                        <i className="fas fa-heart likethis"></i>
                                        <i className="far fa-bookmark bookmarkthis"></i>
                                    </div>
                                    <a><img src={"/images/book-serie.jpg"} alt=""/></a>
                                </div>
                                <div className="info">
                                    <p className="book-title animated bounceInLeft">{"دول سگ"}</p>
                                    <p className="book-subtitle animated lightSpeedIn">{" سگ سگ سگ سگ"}</p>
                                    <div className="generes">
                                        <a href="/categories/fantasy" className="badge badge-secondary badge-pill animated rotateInUpLeft">فانتزی</a>
                                        <a href="/categories/fiction" className="badge badge-secondary badge-pill  animated rotateInUpLeft">تخیلی</a>
                                    </div>
                                </div>
                            </div>
                            <div className="col-10 offset-0 col-sm-4 col-lg-3 book-serie-podcast-result">
                                <div className="cover">
                                    <div className="toolbox">
                                        <i className="fas fa-share-alt sharethis"></i>
                                        <i className="fas fa-heart likethis"></i>
                                        <i className="far fa-bookmark bookmarkthis"></i>
                                    </div>
                                    <a><img src={"/images/book-serie.jpg"} alt=""/></a>
                                </div>
                                <div className="info">
                                    <p className="book-title animated bounceInLeft">{"دول سگ"}</p>
                                    <p className="book-subtitle animated lightSpeedIn">{" سگ سگ سگ سگ"}</p>
                                    <div className="generes">
                                        <a href="/categories/fantasy" className="badge badge-secondary badge-pill animated rotateInUpLeft">فانتزی</a>
                                        <a href="/categories/fiction" className="badge badge-secondary badge-pill  animated rotateInUpLeft">تخیلی</a>
                                    </div>
                                </div>
                            </div>
                            <div className="col-10 offset-0 col-sm-4 col-lg-3 book-serie-podcast-result">
                                <div className="cover">
                                    <div className="toolbox">
                                        <i className="fas fa-share-alt sharethis"></i>
                                        <i className="fas fa-heart likethis"></i>
                                        <i className="far fa-bookmark bookmarkthis"></i>
                                    </div>
                                    <a><img src={"/images/book-serie.jpg"} alt=""/></a>
                                </div>
                                <div className="info">
                                    <p className="book-title animated bounceInLeft">{"دول سگ"}</p>
                                    <p className="book-subtitle animated lightSpeedIn">{" سگ سگ سگ سگ"}</p>
                                    <div className="generes">
                                        <a href="/categories/fantasy" className="badge badge-secondary badge-pill animated rotateInUpLeft">فانتزی</a>
                                        <a href="/categories/fiction" className="badge badge-secondary badge-pill  animated rotateInUpLeft">تخیلی</a>
                                    </div>
                                </div>
                            </div>
                            <div className="col-10 offset-0 col-sm-4 col-lg-3 book-serie-podcast-result">
                                <div className="cover">
                                    <div className="toolbox">
                                        <i className="fas fa-share-alt sharethis"></i>
                                        <i className="fas fa-heart likethis"></i>
                                        <i className="far fa-bookmark bookmarkthis"></i>
                                    </div>
                                    <a><img src={"/images/book-serie.jpg"} alt=""/></a>
                                </div>
                                <div className="info">
                                    <p className="book-title animated bounceInLeft">{"دول سگ"}</p>
                                    <p className="book-subtitle animated lightSpeedIn">{" سگ سگ سگ سگ"}</p>
                                    <div className="generes">
                                        <a href="/categories/fantasy" className="badge badge-secondary badge-pill animated rotateInUpLeft">فانتزی</a>
                                        <a href="/categories/fiction" className="badge badge-secondary badge-pill  animated rotateInUpLeft">تخیلی</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        )
    }
}
