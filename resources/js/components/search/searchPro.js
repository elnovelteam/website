import React, { Component } from 'react';
import Axios from 'axios';
import { Dots, Sentry, Spinner } from 'react-activity';
import 'react-activity/dist/react-activity.css';
import InfiniteScroll from 'react-infinite-scroller';
export default class searchProContainer extends Component {
    constructor(props) {
        super(props);
        this.filterRef = React.createRef();
        this.inputRef = React.createRef();
        this.catTitleRefs = [];
        this.catResultRefs = [];
        this.dafaultScrollOptions = {
            results: [],
            hasMore: true,
            nextPage: 1
        }
        for (let index = 0; index < 5; index++) {
            this.catTitleRefs.push(React.createRef())
            this.catResultRefs.push(React.createRef())
        }
        this.state = {
            searchValue: "",
            lastSearchedValue: "",
            checkboxes: {
                book: true,
                writer: true,
                serie: true,
                podcast: true,
                post: true
            },
            orderBy: "descending",
            resultsNav: null,
            infiniteScroller: null,
        }
    }

    clearResults = () => {
        let { checkboxes } = this.state;
        let defaultScrollerOptions = {};
        Object.keys(checkboxes).map((cat, i) => {
            defaultScrollerOptions = {
                ... defaultScrollerOptions,
                [cat]: {
                    results: [],
                    hasMore: true,
                    nextPage: 1
                }
            }
        })
        this.setState({
            infiniteScroller: defaultScrollerOptions
        })
    }

    componentDidMount() {
        let { dataKeyword } = this.props;
        let { checkboxes } = this.state;
        this.clearResults();
        if(dataKeyword !== "search=http://localhost/api/v1/platform/search/keyword") {
            this.inputRef.current.value = dataKeyword;
            if(dataKeyword.length >= 3) {
                this.setState({
                    lastSearchedValue: dataKeyword,
                    searchValue: dataKeyword,
                    resultsNav: (
                        <div className="col-12 results-nav">
                            <nav>
                                {
                                    Object.values(checkboxes).map((catValue, i) => {
                                        if(catValue) {
                                            return (
                                                React.createElement("div", { key: i, className: i === 0 ? "active animated fadeIn" : "animated fadeIn", ref: this.catTitleRefs[i], onClick: this.changeActiveCat.bind(this, i) },
                                                    React.createElement("a", {}, this.setTitle(Object.keys(checkboxes)[i]))
                                                )
                                            )
                                        }
                                    })
                                }
                            </nav>
                        </div>
                    )
                })
            } else {
                this.setState({
                    resultsNav: (
                        <div className="col-12 results-nav">
                            <p className="text-danger animated rubberBand">لطفاً عنوان جستجوی خود را وارد نمایید (حداقل 3 کاراکتر)</p>
                        </div>
                    )
                })
            }
        } else null
    }


    setTitle = (cat) => {
        switch (cat) {
            case "book":
                return "داستان ها"
                break;
            case "writer":
                return "نویسندگان"
                break;
            case "serie":
                return "سری ها"
                break;
            case "podcast":
                return "پادکست ها"
                break;
            case "post":
                return "مقالات"
                break;
            default:
                break;
        }
    }

    onChangeCheck = (e) => {
        let { checkboxes } = this.state;
        let catagory = e.target.id;
        let newCheckboxes = checkboxes;
        Object.assign(newCheckboxes, {
            [catagory]: !eval(`newCheckboxes.${catagory}`)
        })
        this.setState(prevState => ({
            checkboxes: newCheckboxes,
        }))
    }

    changeActiveCat = (key, e) => {
        this.clearResults();
        this.catTitleRefs.map((ref, index) => {
            ref.current !== null ? ref.current.classList.remove("active") : null
        })
        this.catResultRefs.map((ref, i) => {
            ref.current !== null ? ref.current.classList.remove("active") : null
        })
        e.target.parentNode.classList.add("active");
        this.catResultRefs[key].current.classList.add("active")
    }

    loadMore = (cat, e) => {
        let { searchPro } = this.props;
        let { lastSearchedValue, infiniteScroller, orderBy } = this.state;
        let model = `model[]=${cat}&`;
        let search_url = `${searchPro(lastSearchedValue)}?${model}orderby=created_at&${orderBy === "descending" ? `desc=true` : `order=asc`}&page=${eval(`infiniteScroller.${cat}.nextPage`)}`;
        Axios.get(search_url).then(res => {
            let { data, current_page, last_page } = eval(`res.data.${cat}`);
            this.setState(prevState => {
                let newInfiniteScrollerValues = Object.assign(prevState.infiniteScroller, {
                    [cat]: {
                        results: [...eval(`prevState.infiniteScroller.${cat}.results`), ...data],
                        hasMore: current_page === last_page ? false : true,
                        nextPage: current_page + 1
                    }
                })
                return ({
                    infiniteScroller: newInfiniteScrollerValues,
                })
            })
        })   
    }

    sendResults = (e) => {
        let { searchPro } = this.props;
        let { checkboxes, searchValue, orderBy, infiniteScroller } = this.state;
        if(e.keyCode === 13 || e.type === "click") {
            this.clearResults();
            this.setState({
                lastSearchedValue: searchValue
            })
            if (searchValue.length >= 3) {
                this.setState({
                    resultsNav: (
                        <div className="col-12 results-nav">
                            <nav>
                                {
                                    Object.values(checkboxes).map((catValue, i) => {
                                        if(catValue) {
                                            return (
                                                React.createElement("div", { key: i, className: i === 0 ? "active animated fadeIn" : "animated fadeIn", ref: this.catTitleRefs[i], onClick: this.changeActiveCat.bind(this, i) },
                                                    React.createElement("a", {}, this.setTitle(Object.keys(checkboxes)[i]))
                                                )
                                            )
                                        }
                                    })
                                }
                            </nav>
                        </div>
                    )
                })
            } else {
                this.setState({
                    resultsNav: (
                        <div className="col-12 results-nav">
                            <p className="text-danger animated rubberBand">لطفاً عنوان جستجوی خود را وارد نمایید (حداقل 3 کاراکتر)</p>
                        </div>
                    )
                })
            }
        }
    }

    onChangeSearchValue = (e) => {
        this.setState({
            searchValue: e.target.value,
        })
    }

    onChangeRadio = (e) => {
        this.setState({
            orderBy: e.target.id
        })
    }

    toggleFilter = (e) => {
        this.filterRef.current.classList.toggle("active")
    }

    render() {
        let { checkboxes, searchValue, orderBy, resultsNav, infiniteScroller, lastSearchedValue } = this.state;
        return (
            <div className="search-pro-container col-12">
                <div className="input-group input-section col-10 offset-1 col-md-8 offset-md-2">
                    <div className="input-group-prepend">
                        <button onClick={this.sendResults.bind(this)} className="search-button btn btn-dark" type="button"><i className="fas fa-search"></i></button>
                    </div>
                    <input
                        id="react-search-input"
                        autoFocus={true}
                        ref={this.inputRef}
                        value={searchValue}
                        className="form-control text-dark bg-light"
                        placeholder="دنبال چی میگردی؟"
                        autoComplete="off"
                        onKeyDown={this.sendResults.bind(this)}
                        onChange={this.onChangeSearchValue.bind(this)}
                    />
                    <div className="input-group-prepend filters-container bg-light">
                        <span className="text-dark" onClick={this.toggleFilter.bind(this)}>فیلتر ها</span>
                        <div ref={this.filterRef} className="animated fadeIn disable">
                            <div className="ordered-by">
                                <div className="filter-section">
                                    <p>مرتب سازی بر اساس :</p>
                                    <div className="radio-container">
                                        <input type="radio" name="order" id="ascending" onChange={this.onChangeRadio.bind(this)} checked={orderBy === "ascending"? true : false} />
                                        <label htmlFor="ascending">قدیمی ترین</label>
                                    </div>
                                    <div className="radio-container">
                                        <input type="radio" name="order" id="descending" onChange={this.onChangeRadio.bind(this)} checked={orderBy === "descending" ? true : false} />
                                        <label htmlFor="descending">جدید ترین</label>
                                    </div>
                                </div>
                                <div className="filter-section">
                                    <p>جستجو در :</p>
                                    <div className="catagory-section">
                                        <div className="check-container">
                                            <label className="checkboxes text-dark" htmlFor="book">داستان ها</label>
                                            <input className="checkboxes" type="checkbox" id="book" onChange={this.onChangeCheck.bind(this)} checked={checkboxes.book} />
                                        </div>
                                        <div className="check-container">
                                            <label className="checkboxes text-dark" htmlFor="writer">نویسندگان</label>
                                            <input className="checkboxes" type="checkbox" id="writer" onChange={this.onChangeCheck.bind(this)} checked={checkboxes.writer} />
                                        </div>
                                        <div className="check-container">
                                            <label className="checkboxes text-dark" htmlFor="serie">سری ها</label>
                                            <input className="checkboxes" type="checkbox" id="serie" onChange={this.onChangeCheck.bind(this)} checked={checkboxes.serie} />
                                        </div>
                                        <div className="check-container">
                                            <label className="checkboxes text-dark" htmlFor="podcast">پادکست ها</label>
                                            <input className="checkboxes" type="checkbox" id="podcast" onChange={this.onChangeCheck.bind(this)} checked={checkboxes.podcast} />
                                        </div>
                                        <div className="check-container">
                                            <label className="checkboxes text-dark" htmlFor="post">مقالات</label>
                                            <input className="checkboxes" type="checkbox" id="post" onChange={this.onChangeCheck.bind(this)} checked={checkboxes.post} />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                { resultsNav }
                { resultsNav !== null && lastSearchedValue.length > 2 &&
                    Object.values(checkboxes).map((catValue, i) => {
                        if (catValue) {
                            return (
                                <div key={i} className={ i === 0 ? "col-12 results-container animated zoomIn active" : "col-12 results-container animated zoomIn" } ref={this.catResultRefs[i]}>
                                    {
                                        <InfiniteScroll
                                            useWindow={false}
                                            hasMore={eval(`infiniteScroller.${Object.keys(checkboxes)[i]}.hasMore`)}
                                            loadMore={this.loadMore.bind(this, Object.keys(checkboxes)[i])}
                                            getScrollParent={() => { return document.body }}
                                            loader= {
                                                <div key={0} className="loader">
                                                    <Sentry color="#0275d8"/>
                                                    <p className="text-primary animated headShake">در حال جستجو</p>
                                                </div>
                                            }
                                        >
                                            {
                                                eval(`infiniteScroller.${Object.keys(checkboxes)[i]}.hasMore`) !== true && eval(`infiniteScroller.${Object.keys(checkboxes)[i]}.results`).length === 0 ?
                                                <p className="text-danger">نتیجه ای یافت نشد</p>
                                                : null
                                            }
                                            {
                                                Object.keys(checkboxes)[i] === "book" &&
                                                    infiniteScroller.book.results.map((item, i) => {
                                                        return (
                                                            <div key={i} className="col-9 col-sm-4 col-md-3 book-serie-podcast-result">
                                                                <div className="cover">
                                                                    <div className="toolbox">
                                                                        <i className="fas fa-share-alt sharethis"></i>
                                                                        <i className="fas fa-heart likethis"></i>
                                                                        <i className="far fa-bookmark bookmarkthis"></i>
                                                                    </div>
                                                                    <a href={`/book/${item.slug}`}><img src={item.photo !== null ? item.photo.file.path : "/images/book.jpg"} alt=""/></a>
                                                                </div>
                                                                <div className="info">
                                                                    <p className="book-title animated bounceInLeft">{item.title}</p>
                                                                    <p className="book-subtitle animated lightSpeedIn">{item.description}</p>
                                                                    <div className="generes">
                                                                        <a href="/categories/fantasy" className="badge badge-secondary badge-pill animated rotateInUpLeft">فانتزی</a>
                                                                        <a href="/categories/fiction" className="badge badge-secondary badge-pill  animated rotateInUpLeft">تخیلی</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        )
                                                    })

                                            }
                                            {
                                                Object.keys(checkboxes)[i] === 'writer' &&
                                                    infiniteScroller.writer.results.map((item, i) => {
                                                        return (
                                                            <div key={i} className="writer-result col-10 col-sm-6 col-md-4 col-lg-3 animated flash">
                                                                <div className="profile-photo-container">
                                                                    <div className="circle-image-container">
                                                                        <a href={`/writers/${item.user_id}`}><img src={item.photo !== null ? item.photo.file.path : "/images/writer.jpg"} alt=""/></a>
                                                                    </div>
                                                                </div>
                                                                <div className="writer-info animated fadeIn">
                                                                    <p className="text-danger p-0">{item.nickname}</p>
                                                                    <p className="writer-bio">{item.bio}</p>
                                                                </div>
                                                                <div className="see-button-container">
                                                                    <a href={`/writers/${item.user_id}`}><button className="btn btn-sm btn-outline-primary animated flipInX">مشاهده پروفایل</button></a>
                                                                </div>
                                                            </div>
                                                        )
                                                    })
                                            }
                                            {
                                                Object.keys(checkboxes)[i] === 'podcast' &&
                                                    infiniteScroller.podcast.results.map((item, i) => {
                                                        return (
                                                            <div key={i} className="col-9 col-sm-4 col-md-3 book-serie-podcast-result">
                                                                <div className="cover">
                                                                    <div className="toolbox">
                                                                        <i className="fas fa-share-alt sharethis"></i>
                                                                        <i className="fas fa-heart likethis"></i>
                                                                        <i className="far fa-bookmark bookmarkthis"></i>
                                                                    </div>
                                                                    <a href={`/podcasts/${item.slug}`}><img src={item.photo !== null ? item.photo.file.path : "/images/podcast.jpg"} alt="" className="podcast-image"/></a>
                                                                </div>
                                                                <div className="info">
                                                                    <p className="book-title animated bounceInLeft">{item.title}</p>
                                                                    <p className="book-subtitle animated lightSpeedIn">{item.description}</p>
                                                                    <div className="generes">
                                                                        <a href="/categories/fantasy" className="badge badge-secondary badge-pill animated rotateInUpLeft">فانتزی</a>
                                                                        <a href="/categories/fiction" className="badge badge-secondary badge-pill  animated rotateInUpLeft">تخیلی</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        )
                                                    })
                                            }
                                            {
                                                Object.keys(checkboxes)[i] === 'post' &&
                                                    infiniteScroller.post.results.map((item, i) => {
                                                        return (
                                                            <div key={i} className="col-12 col-sm-6 col-md-4 post-result">
                                                                <div className="animated flash">
                                                                    <img src={item.photo !== null ? item.photo.file.path : "/images/post.jpg"} alt=""/>
                                                                    <div className="overlay">
                                                                        <div className="float-right badge badge-sm badge-pill">
                                                                            <span>{ item.likes_count }</span>
                                                                            <i className="fas fa-heart text-danger"></i>
                                                                        </div>
                                                                        <div className="float-left badge badge-sm badge-pill">
                                                                            <span>{ item.last_views_count }</span>
                                                                            <i className="fas fa-eye text-dark"></i>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div className="post-info-container p-2 animated bounce">
                                                                    <div className="p-1">
                                                                        <p className="text-dark post-title"><a href={ `/blog/${item.slug}` }>{ item.title }</a></p>
                                                                        <div className="post-desc">
                                                                            <p className="text-secondary">{ item.summary }</p>
                                                                        </div>
                                                                    </div>
                                                                    <div className="see-section col-12 float-left mb-2">
                                                                        <a href={ `/blog/${item.slug}` } className="float-left"><button className="btn btn-outline-dark btn-sm pd-3">مشاهده<i className="fas fa-eye mr-1"></i></button></a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        )
                                                    })
                                            }
                                            {
                                                Object.keys(checkboxes)[i] === 'serie' &&
                                                infiniteScroller.serie.results.map((item, i) => {
                                                    return (
                                                        <div key={i} className="col-9 col-sm-4 col-md-3 book-serie-podcast-result">
                                                            <div className="cover">
                                                                <div className="toolbox">
                                                                    <i className="fas fa-share-alt sharethis"></i>
                                                                    <i className="fas fa-heart likethis"></i>
                                                                    <i className="far fa-bookmark bookmarkthis"></i>
                                                                </div>
                                                                <a href={`/serie/${item.slug}`}><img src={item.photo !== null ? item.photo.file.path : "/images/book-serie.jpg"} alt=""/></a>
                                                            </div>
                                                            <div className="info">
                                                                <p className="book-title animated bounceInLeft">{item.title}</p>
                                                                <p className="book-subtitle animated lightSpeedIn">{item.description}</p>
                                                                <div className="generes">
                                                                    <a href="/categories/fantasy" className="badge badge-secondary badge-pill animated rotateInUpLeft">فانتزی</a>
                                                                    <a href="/categories/fiction" className="badge badge-secondary badge-pill  animated rotateInUpLeft">تخیلی</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    )
                                                })
                                            }
                                        </InfiniteScroll>
                                    }
                                </div>
                            )
                        }
                    })
                }
            </div>
        )
    }
}
