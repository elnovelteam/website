import React, { Component } from 'react'
import Axios from 'axios';
import { Dots, Sentry, Spinner } from 'react-activity';
import 'react-activity/dist/react-activity.css';

export default class SearchContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            searchValue: "",
            checkboxes: {
                book: true,
                writer: true,
                serie: false,
                podcast: false,
                post: false
            },
            combobox: [],
            firstTrueIndex: 0,
            hoveredCat: null,
            hoveredIndex: null
        }
        this.linksRef = {};
        Object.keys(this.state.checkboxes).map((cat, index) => {
            Object.assign(this.linksRef, {
                [cat]: this.makeRefs()
            })
        })
    }

    makeRefs = () => {
        let arrayResult = [];
        for (let index = 0; index < 6; index++) {
            arrayResult.push(React.createRef());            
        }
        return arrayResult;
    }

    setTitle = (cat) => {
        switch (cat) {
            case "book":
                return "داستان ها"

            case "writer":
                return "نویسندگان"

            case "serie":
                return "سری ها"

            case "podcast":
                return "پادکست ها"

            case "post":
                return "مقالات"

            case "داستان ها":
                return "book"

            case "نویسندگان":
                return "writer"

            case "سری ها":
                return "serie"

            case "پادکست ها":
                return "podcast"

            case "مقالات":
                return "post"

            default:
                break;
        }
    }

    setSlug = (cat) => {
        switch (cat) {
            case "book":
                return "books"

            case "writer":
                return "writers"

            case "serie":
                return "series"

            case "podcast":
                return "podcasts"

            case "post":
                return "blog"

            default:
                break;

        }
    }

    componentDidMount() {
        let { checkboxes } = this.state;
        let comboCol = null;
        let combobox = [];
        Object.values(checkboxes).map((item, i) => {
            if (item === true) {
                comboCol = React.createElement("div", { className: "col-12 col-md-4 animated rotateInDownLeft", key: i }, React.createElement("p", {}, this.setTitle(Object.keys(checkboxes)[i])));
                combobox.push(comboCol)
            }
        })
        this.setState({
            combobox: combobox
        })
    }

    onChangeInput = (e) => {
        let thisCopy = this;
        let searchValue = e.target.value;
        let { search, searchPro } = this.props;
        let { checkboxes, combobox } = this.state;
        let models = '';
        let checksCount = 0;
        this.setState({
            searchValue: searchValue
        })
        if (searchValue.length <= 0) {
            let newCombobox = combobox.map((col, i) => {
                let newChildren = col.props.children;
                newChildren.splice(1, 1);
                return React.cloneElement(col, {
                    children: newChildren
                })
            })
            this.setState(prevState => ({
                combobox: newCombobox
            }))
        } else {
            Object.values(checkboxes).map((value, i) => {
                if(value === true) {
                    models += `model[]=${Object.keys(checkboxes)[i]}&`;
                    checksCount += 1;
                } else
                    models
            })
            let newCombobox = combobox.map((col, index) => {
                let waitingElement = <Spinner key={Date.now()} color="black" />;
                let TitleObj = col.props.children[0] ? col.props.children[0] : col.props.children;
                let newChildren = [TitleObj, waitingElement];
                let comboCol = React.cloneElement(col, {
                    children: newChildren
                })
                return comboCol
            })
            this.setState({
                combobox: newCombobox
            })
            let search_url = `${search(searchValue)}?${models}orderby=created_at&desc=true`;
            Axios.get(search_url, null).then(res => {
                let searchResults = res.data;
                let finalCombobox = newCombobox.map((col, i) => {
                    let links = []
                    if (Object.values(searchResults)[i].length !== 0 && typeof Object.values(searchResults)[0] !== "undefined") {
                        Object.values(searchResults)[i].map((link, j) => {
                            if (j < 3) {
                                links.push(React.createElement("li", { key: j, className:"animated bounceIn", id: "link-li", ref: eval(`thisCopy.linksRef.${Object.keys(searchResults)[i]}[${j}]`) }, React.createElement("a", { id: "link", ref: eval(`thisCopy.linksRef.${Object.keys(searchResults)[i]}[${ j + 3 }]`), href: `/${this.setSlug(Object.keys(searchResults)[i])}/${link.slug}` }, link.title ? link.title : link.nickname)));
                            } 
                        })   
                    } else {
                        links.push(React.createElement("li", { key: i, className: "not-found animated swing" }, React.createElement("p", { className: "text-dark" }, "نتیجه ای یافت نشد")))
                    }
                    let colLinks = React.createElement("ul", { key: i }, links.map((link, index) => (link)));
                    let newChildren = col.props.children;
                    newChildren.splice(1, 1, colLinks);
                    return React.cloneElement(col, {
                        children: newChildren
                    })
                })
                this.setState({
                    combobox: finalCombobox
                })
            })
        }
    }

    sendResults = (e) => {
        let thisCopy = this;
        let { searchValue, checkboxes, combobox, firstTrueIndex, hoveredCat, hoveredIndex  } = this.state;
        if(searchValue.length > 0) {
            if((e.keyCode === 13 || e.type === "click")) {
                window.location.href = `/search?keyword=${this.state.searchValue}`
            } else {
                let newSearchValue = null;
                switch (e.keyCode) {
                    case 40:
                        let firstTrueCat = null;
                        let firstHovered = firstTrueIndex;
                        if (firstTrueIndex === 0) {
                            combobox.map((comboCol, i) => {
                                if(firstHovered === 0 && typeof comboCol.props.children[1] !== "undefined" && comboCol.props.children[1].props.children[0].props.children.type !== "p") {
                                    firstHovered =+ 1;
                                    firstTrueCat = this.setTitle(comboCol.props.children[0].props.children);
                                    newSearchValue = eval(`thisCopy.linksRef.${firstTrueCat}[3].current.innerText`);
                                    eval(`thisCopy.linksRef.${firstTrueCat}[0].current`).classList.add("hovered")
                                    // eval(`thisCopy.linksRef.${firstTrueCat}[3].current`).focus();
                                    this.setState(prevState => ({
                                        firstTrueIndex: prevState.firstTrueIndex + 1,
                                        hoveredCat: firstTrueCat,
                                        hoveredIndex: 0,
                                        searchValue: newSearchValue
                                    }))
                                }
                            })
                        } else {
                            if(hoveredIndex < 2 && eval(`thisCopy.linksRef.${hoveredCat}[${hoveredIndex + 1}].current`) !== null) {
                                eval(`thisCopy.linksRef.${hoveredCat}[${hoveredIndex}].current`).classList.remove("hovered");
                                eval(`thisCopy.linksRef.${hoveredCat}[${hoveredIndex + 1}].current`).classList.add("hovered");
                                // eval(`thisCopy.linksRef.${hoveredCat}[${hoveredIndex + 4}].current`).focus();
                                newSearchValue = eval(`thisCopy.linksRef.${hoveredCat}[${hoveredIndex + 4}].current.innerText`)
                                this.setState(prevState => ({
                                    hoveredIndex: prevState.hoveredIndex + 1,
                                    searchValue: newSearchValue
                                }))
                            } else {
                                eval(`thisCopy.linksRef.${hoveredCat}[${hoveredIndex}].current`).classList.remove("hovered");
                                eval(`thisCopy.linksRef.${hoveredCat}[0].current`).classList.add("hovered");
                                // eval(`thisCopy.linksRef.${hoveredCat}[3].current`).focus();
                                newSearchValue = eval(`thisCopy.linksRef.${hoveredCat}[3].current.innerText`)
                                this.setState(prevState => ({
                                    hoveredIndex: 0,
                                    searchValue: newSearchValue
                                }))
                            }
                        }  
                        break;
    
                    case 38:
                        if (firstTrueIndex !== 0) {
                            if(hoveredIndex > 0) {
                                eval(`thisCopy.linksRef.${hoveredCat}[${hoveredIndex}].current`).classList.remove("hovered");
                                eval(`thisCopy.linksRef.${hoveredCat}[${hoveredIndex - 1}].current`).classList.add("hovered");
                                // eval(`thisCopy.linksRef.${hoveredCat}[${hoveredIndex + 2}].current`).focus();
                                newSearchValue = eval(`thisCopy.linksRef.${hoveredCat}[${hoveredIndex + 2}].current.innerText`)
                                this.setState(prevState => ({
                                    hoveredIndex: prevState.hoveredIndex - 1,
                                    searchValue: newSearchValue
                                }))
                            } else {
                                let linkHoverTarget = null;
                                let linkHoverIndex = null;
                                eval(`thisCopy.linksRef.${hoveredCat}[${hoveredIndex}].current`).classList.remove("hovered");
                                eval(`thisCopy.linksRef.${hoveredCat}`).map((ref, i) => {
                                    if (ref.current !== null) {
                                        linkHoverTarget = ref;
                                        linkHoverIndex = i;
                                    }
                                })
                                eval(`thisCopy.linksRef.${hoveredCat}[${linkHoverIndex - 3}].current`).classList.add("hovered");
                                // eval(`thisCopy.linksRef.${hoveredCat}[${hoveredIndex + 3}].current`).focus();
                                newSearchValue = linkHoverTarget.current.innerText
                                this.setState(prevState => ({
                                    hoveredIndex: linkHoverIndex - 3,
                                    searchValue: newSearchValue
                                }))
                            }   
                        }
                        break;
    
                    case 39:
                        if (firstTrueIndex !== 0) {
                            let preventRepeatMap = 0;
                            let prevHoveredCatIndex = combobox.length;
                            combobox.map((comboCol, index) => {
                                if (comboCol.props.children[0].props.children === this.setTitle(hoveredCat)) {
                                    prevHoveredCatIndex = index;
                                }
                                if(preventRepeatMap === 0 && typeof comboCol.props.children[1] !== "undefined" && comboCol.props.children[1].props.children[0].props.children.type !== "p" && index > prevHoveredCatIndex) {
                                    preventRepeatMap =+ 1;
                                    let newHoveredCat = this.setTitle(comboCol.props.children[0].props.children);
                                    this.setState({
                                        hoveredCat: newHoveredCat
                                    })
                                    eval(`thisCopy.linksRef.${hoveredCat}[${hoveredIndex}].current`).classList.remove("hovered");
                                    if (eval(`thisCopy.linksRef.${newHoveredCat}[${hoveredIndex}].current`) !== null) {
                                        eval(`thisCopy.linksRef.${newHoveredCat}[${hoveredIndex}].current`).classList.add("hovered");
                                        // eval(`thisCopy.linksRef.${newHoveredCat}[${hoveredIndex + 3}].current`).focus();
                                        newSearchValue = eval(`thisCopy.linksRef.${newHoveredCat}[${hoveredIndex + 3}].current.innerText`)
                                        this.setState({
                                            searchValue: newSearchValue
                                        })
                                    } else {
                                        eval(`thisCopy.linksRef.${newHoveredCat}[0].current`).classList.add("hovered");
                                        // eval(`thisCopy.linksRef.${newHoveredCat}[3].current`).focus();
                                        newSearchValue = eval(`thisCopy.linksRef.${newHoveredCat}[3].current.innerText`)
                                        this.setState({
                                            hoveredIndex: 0,
                                            searchValue: newSearchValue
                                        })
                                    }
                                }
                            })
                        }
                        break;

                    case 37:
                        if (firstTrueIndex !== 0) {
                            let preventRepeatMap = 0;
                            let prevHoveredCatIndex = 0;
                            combobox.slice(0).reverse().map((comboCol, index) => {
                                let i = index;
                                switch (index) {
                                    case 2:
                                        i = 0
                                        break
                                    case 0:
                                        i = 2
                                        break
                                    default:
                                        break;
                                }
                                if (comboCol.props.children[0].props.children === this.setTitle(hoveredCat)) {
                                    prevHoveredCatIndex = i;
                                }
                                if(preventRepeatMap === 0 && typeof comboCol.props.children[1] !== "undefined" && comboCol.props.children[1].props.children[0].props.children.type !== "p" && i < prevHoveredCatIndex) {
                                    preventRepeatMap =+ 1;
                                    let newHoveredCat = this.setTitle(comboCol.props.children[0].props.children);
                                    this.setState({
                                        hoveredCat: newHoveredCat
                                    })
                                    eval(`thisCopy.linksRef.${hoveredCat}[${hoveredIndex}].current`).classList.remove("hovered");
                                    if (eval(`thisCopy.linksRef.${newHoveredCat}[${hoveredIndex}].current`) !== null) {
                                        eval(`thisCopy.linksRef.${newHoveredCat}[${hoveredIndex}].current`).classList.add("hovered");
                                        // eval(`thisCopy.linksRef.${newHoveredCat}[${hoveredIndex + 3}].current`).focus();
                                        newSearchValue = eval(`thisCopy.linksRef.${newHoveredCat}[${hoveredIndex + 3}].current.innerText`)
                                        this.setState({
                                            searchValue: newSearchValue
                                        })
                                    } else {
                                        eval(`thisCopy.linksRef.${newHoveredCat}[0].current`).classList.add("hovered");
                                        // eval(`thisCopy.linksRef.${newHoveredCat}[3].current`).focus();
                                        newSearchValue = eval(`thisCopy.linksRef.${newHoveredCat}[3].current.innerText`)
                                        this.setState({
                                            hoveredIndex: 0,
                                            searchValue: eval(`thisCopy.linksRef.${newHoveredCat}[3].current.innerText`)
                                        })
                                    }
                                }
                            })
                        }
                        break;
    
                    default:
                        break;
                }
            }
        }
    }

    onChangeCheck = (e) => {
        let thisCopy = this;
        let { search, searchPro } = this.props;
        let { checkboxes, combobox, searchValue } = this.state;
        let catagory = e.target.id;
        let checksCount = 0;
        Object.values(checkboxes).map((item, i) => {
            if (item === true) {
                checksCount += 1;
            }
        })
        if(checksCount >= 3 && e.target.checked || checksCount === 1 && !e.target.checked) {
            e.preventDefault()
        } else {
            let newCheckboxes = checkboxes;
            let comboCol = null;
            let newCombobox = combobox;
            Object.assign(newCheckboxes, {
                [catagory]: !eval(`newCheckboxes.${catagory}`)
            })
            if (e.target.checked) {
                comboCol = React.createElement("div", { className: "col-12 col-md-4 animated rotateInDownLeft", key: Date.now()}, React.createElement("p", {}, this.setTitle(catagory)));
                newCombobox.push(comboCol)
                this.setState({
                    combobox: newCombobox
                })
                if (searchValue !== "") {
                    let waitingElement = <Spinner key={Date.now()} color="black" />;
                    let TitleObj = comboCol.props.children[0] ? comboCol.props.children[0] : comboCol.props.children;
                    let newChildren = [TitleObj, waitingElement];
                    comboCol = React.cloneElement(comboCol, {
                        children: newChildren
                    })
                    newCombobox.splice(newCombobox.length - 1, 1, comboCol)
                    this.setState({
                        combobox: newCombobox
                    })
                    let search_url = `${search(searchValue)}?model[]=${catagory}&orderby=created_at&desc=true`;
                    Axios.get(search_url, null).then(res => {
                        let searchResults = res.data;
                        let links = []
                        if (typeof Object.values(searchResults)[0] !== "undefined" && Object.values(searchResults)[0].length !== 0) {
                            Object.values(searchResults)[0].map((link, j) => {
                                if (j < 3) {
                                    links.push(React.createElement("li", { key: j, id: "link-li", ref: eval(`thisCopy.linksRef.${Object.keys(searchResults)[0]}[${j}]`) }, React.createElement("a", { id: "link", ref: eval(`thisCopy.linksRef.${Object.keys(searchResults)[0]}[${j + 3}]`), href: `/${this.setSlug(Object.keys(searchResults)[0])}/${link.slug}` }, link.title ? link.title : link.nickname)));
                                }
                            })   
                        } else {
                            links.push(React.createElement("li", { key: Date.now(), className: "not-found animated swing" }, React.createElement("p", { className: "text-dark" }, "نتیجه ای یافت نشد")))
                        }
                        let colLinks = React.createElement("ul", {key: Date.now()}, links.map((link, index) => (link)));
                        let TitleObj = comboCol.props.children[0] ? comboCol.props.children[0] : comboCol.props.children;
                        let newChildren = [TitleObj, colLinks];
                        comboCol = React.cloneElement(comboCol, {
                            children: newChildren
                        })
                        newCombobox.splice(newCombobox.length - 1, 1, comboCol)
                        this.setState({
                            combobox: newCombobox
                        })
                    })
                }
            } else {
                newCombobox.map((col, index) => {
                    if (Array.isArray(col.props.children) && this.setTitle(e.target.id) === col.props.children[0].props.children) {
                        newCombobox.splice(newCombobox.indexOf(col), 1)
                    } else if(Array.isArray(col.props.children) === false && this.setTitle(e.target.id) === col.props.children.props.children) {
                        newCombobox.splice(newCombobox.indexOf(col), 1)
                    }
                })
                this.setState({
                    firstTrueIndex: 0,
                    hoveredCat: null,
                    hoveredIndex: null
                })
            }
            this.setState(prevState => ({
                checkboxes: newCheckboxes,
                combobox: newCombobox
            }))   
        }
    }

    inputOnFocused = (e) => {
        let thisCopy = this;
        let { hoveredCat, hoveredIndex, firstTrueIndex } = this.state;
        eval(`thisCopy.linksRef.${hoveredCat}`) && eval(`thisCopy.linksRef.${hoveredCat}.current`) !== null ? eval(`thisCopy.linksRef.${hoveredCat}[${hoveredIndex}].current`).classList.remove("hovered") : null
        this.setState({
            hoveredCat: null,
            hoveredIndex: null,
            firstTrueIndex: 0
        })
    }

    render() {
        let { checkboxes, combobox, searchValue } = this.state;
        return (
            <div className="search-section-container">
                <div className="catagory-section">
                    <div className="check-container col-6 col-md-1">
                        <input className="checkboxes" type="checkbox" id="book" onChange={this.onChangeCheck.bind(this)} checked={checkboxes.book} />
                        <label className="checkboxes text-dark" htmlFor="book">داستان ها</label>
                    </div>
                    <div className="check-container col-6 col-md-1">
                        <input className="checkboxes" type="checkbox" id="writer" onChange={this.onChangeCheck.bind(this)} checked={checkboxes.writer} />
                        <label className="checkboxes text-dark" htmlFor="writer">نویسندگان</label>
                    </div>
                    <div className="check-container col-6 col-md-1">
                        <input className="checkboxes" type="checkbox" id="serie" onChange={this.onChangeCheck.bind(this)} checked={checkboxes.serie} />
                        <label className="checkboxes text-dark" htmlFor="serie">سری ها</label>
                    </div>
                    <div className="check-container col-6 col-md-1">
                        <input className="checkboxes" type="checkbox" id="podcast" onChange={this.onChangeCheck.bind(this)} checked={checkboxes.podcast} />
                        <label className="checkboxes text-dark" htmlFor="podcast">پادکست ها</label>
                    </div>
                    <div className="check-container col-6 col-md-1">
                        <input className="checkboxes" type="checkbox" id="post" onChange={this.onChangeCheck.bind(this)} checked={checkboxes.post} />
                        <label className="checkboxes text-dark" htmlFor="post">مقالات</label>
                    </div>
                </div>
                <div className="input-group input-section col-12 col-md-8 offset-md-2">
                    <div className="input-group-prepend">
                        <button id="react-search-button" onClick={this.sendResults.bind(this)} className="search-button btn btn-dark" type="button"><i id="react-search-icon" className="fas fa-search"></i></button>
                    </div>
                    <input
                        id="react-search-input"
                        autoFocus={true}
                        autoComplete="off"
                        value={searchValue}
                        className="form-control text-dark"
                        placeholder="دنبال چی میگردی؟"
                        onKeyDown={this.sendResults.bind(this)}
                        onChange={this.onChangeInput.bind(this)}
                        onFocus={this.inputOnFocused.bind(this)}
                        onClick={this.inputOnFocused.bind(this)}
                    />
                </div>
                <div className="col-12 col-md-8 offset-md-2 combo-container pt-3">
                    {combobox.map((col, i) => (col))}
                </div>
            </div>
        )
    }
}
