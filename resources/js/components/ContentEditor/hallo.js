


import React, { Component } from 'react';
import CKEditor from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import Axios from 'axios';

const editorConfiguration = {
    language: 'fa',
    toolbar: ['heading', '|', 'bold', 'italic', 'link', '|', 'bulletedList', 'numberedList', '|', 'indent', 'outdent', '|', 'blockQuote', 'insertTable', 'mediaEmbed', '|', 'redo','undo']
};

var SelecteddFiles = [];
export default class ContentEditorContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            editor: null,
            filePicker: false,
            data: "<p class='col-12 p-3'>در حال بارگذاری ...</p>"
        };
        // console.log(this.props.TargetURL);
        Axios.get(this.props.TargetURL).then(res => {
            let {body} = res.data;
            this.setState(prevState => ({
                data: body
            }));
        });
        $(document).ready(function(){
            $('.ck-toolbar__items').append('<span class="ck ck-toolbar__separator"></span><button class="ck ck-button ck-off" type="button" tabindex="-1" id="kir" aria-pressed="false"><i style="font-family: \'Font Awesome 5 Free\' !important;" class="far fa-images"></i><span class="ck ck-tooltip ck-tooltip_s"><span class="ck ck-tooltip__text">اضافه کردن عکس</span></span><span class="ck ck-button__label">عکس</span></button>');
        });
        $(document).on('click', "#kir", this.insertImage);

    }
    insertImage = () => {
        // const imageUrl = prompt( 'Image URL' );
        // let { editor } = this.state;
        // editor.model.change( writer => {
        // const imageElement = writer.createElement( 'image', {
        //     src: imageUrl
        // } );

        // // Insert the image in the current selection location.
        // editor.model.insertContent( imageElement, editor.model.document.selection );
        // } );
    }
    insertSelectedImages = (selectedImages) => {
        console.log(selectedImages);
    }
    saveContent = (e) => {
        e.preventDefault();
        let buttonForm = $(e.target);
        Swal.fire({
            icon: 'info',
            title: 'در حال ذخیره سازی',
            html: '<p>کمی صبر کنید ...</p>',
            showConfirmButton	: false,
            customClass: {
                content: 'persian-text'
            }
        });
        Axios.post(this.props.TargetURL, {body: this.state.data}).then(res => {
            $('#update-form').submit();
        });
    }
    render() {
        return (
            <div className="rich-text-editor">
                <CKEditor
                    editor={ ClassicEditor }
                    data={this.state.data}
                    config={editorConfiguration}
                    onInit={ editor => {
                        // You can store the "editor" and use when it is needed.
                        let EDT = editor;

                        this.setState(prevState => ({
                            editor: EDT
                        }));
                        console.log( 'Editor is ready to use!', editor );
                    } }
                    onChange={ (e, editor ) => {
                        const data = editor.getData();
                        this.setState(prevState => ({
                            data: data
                        }));
                        {/* console.log({ e, editor, data } ); */}
                    } }
                    onBlur={ (e, editor ) => {
                        console.log('Blur.', editor );
                        console.log(editor.ui.componentFactory.names);
                    } }
                    onFocus={ (e, editor ) => {
                        console.log('Focus.', editor );
                    } }
                />
            </div>
        );
    }
}
