import React, { Component } from 'react';
import { connect } from 'react-redux';
import Axios from 'axios';
import { toggleGallery } from '../actions'
import InfiniteScroll from 'react-infinite-scroller';
import File from '../../PickFiles/File'


const mapStateToProps = (state) => {
    let { editor, images, active } = state.ContentEditorReducer;
    return {
        editor: editor,
        images: images,
        active: active
    };
}

const mapDispatchToProps = (dispatch) => ({
    toggleGallery: () => dispatch(toggleGallery())
})

class ChooseFile extends Component {
    constructor(props) {
        super(props)
        this.state = {
            files: [],
            nextPage: 1,
            hasMore: true,
            selectedFiles: [],
        }
    }
    handleLoadMore = () => {
        let target = `${this.props.Target}/image?page=${this.state.nextPage}&api_token=${this.props.ApiToken}`;
        console.log(target);
        Axios.defaults.headers.post['Content-Type'] ='application/x-www-form-urlencoded';
        Axios.get(target)
        .then(res => {
            let { current_page, last_page, data } = res.data;
            this.setState(prevState => ({
                files: [...prevState.files, ...data],
                hasMore: false,
                nextPage: current_page + 1
            }));
        }).catch(e => { console.log(e); });
    }
	handleClickImage = (path) => {
		let { MainPage, editor } = this.props;
        let imageUrl = `${MainPage}/${path}`;
        editor.model.change(writer => {
            let imageElement = writer.createElement('image', {src: imageUrl, alt: prompt('محتوای تگ alt', '')});

            editor.model.insertContent( imageElement, editor.model.document.selection );
        });
	}
    render() {
		let {active, toggleGallery, MainPage} = this.props;
        return (
            <div className={`choose-file-container ${active ? 'd-block' : 'd-none'} p-3`}>
                <div className="col-12 mb-4 float-left">
                    <button type="button" className="btn btn-danger" onClick={toggleGallery}>
                        <i className="fas fa-2x fa-times"></i>
                    </button>
                </div>
                <div className="col-12 float-left" style={{ height: '600px', overflow: 'auto'}}>
                    <InfiniteScroll pageStart={1} loadMore={this.handleLoadMore} hasMore={this.state.hasMore} useWindow={true}>
                    {this.state.files.map((file, i) => {
                        return (
                            <File key={i} Linker={MainPage} FileType={file.type} onClick={this.handleClickImage.bind(this, file.path)} {...file}/>
                        )
                    })}
                    </InfiniteScroll>
                </div>
            </div>
        )
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ChooseFile);