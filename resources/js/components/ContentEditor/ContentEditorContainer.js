import React, { Component } from 'react';
import { connect } from 'react-redux';
import RichTextEditor from './RichTextEditor';
import Axios from 'axios';

const mapStateToProps = (state) => {
    let { editor, data, images, active } = state.ContentEditorReducer;
    return {
        editor: editor,
        data: data,
        images: images,
        active: active
    };
}

class ContentEditorContainer extends Component {
    constructor(props) {
        super(props);
        $(document).on('click', "#savebtn", this.saveContent);
    }
    saveContent = (e) => {
        e.preventDefault();
        let buttonForm = $(e.target);
        Swal.fire({
            icon: 'info',
            title: 'در حال ذخیره سازی',
            html: '<p>کمی صبر کنید ...</p>',
            showConfirmButton	: false,
            customClass: {
                content: 'persian-text'
            }
        });
        Axios.post(this.props.TargetURL, {body: this.props.data}).then(res => {
            $('#update-form').submit();
        });
    }
    render() {
        return (
            <RichTextEditor {...this.props}/>
        );
    }
}

export default connect(mapStateToProps)(ContentEditorContainer);