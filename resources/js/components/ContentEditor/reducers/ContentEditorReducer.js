import { SET_SELECTED_IMAGES, TOGGLE_GALLERY, SET_EDITOR, CHANGE_DATA } from '../actions/actionTypes'

const firstState = {
    images: [],
    active: false,
    editor: null,
    data: "<p class='col-12 p-3'>در حال بارگذاری ...</p>"
}

const ContentEditorReducer = (state=firstState, action) => {
    switch (action.type) {
        case SET_SELECTED_IMAGES:
            return Object.assign({}, state, {
                image: action.images
            })
        case TOGGLE_GALLERY:
            return Object.assign({}, state, {
                active: ! state.active
            })
        case SET_EDITOR:
            return Object.assign({}, state, {
                editor: action.editor
            })
        case CHANGE_DATA:
            return Object.assign({}, state, {
                data: action.data
            })
        default:
            return state
    }
};

export default ContentEditorReducer;