import { combineReducers } from "redux";
import ContentEditorReducer from './ContentEditorReducer';

export default combineReducers({
    ContentEditorReducer
});