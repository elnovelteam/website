import React, { Component } from 'react';
import { connect } from 'react-redux';
import CKEditor from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import Axios from 'axios';
import { changeData, setEditor, toggleGallery } from './actions'
import ChooseFile from './ChooseFile';

const editorConfiguration = {
    language: 'fa',
    toolbar: ['heading', '|', 'bold', 'italic', 'link', '|', 'bulletedList', 'numberedList', '|', 'indent', 'outdent', '|', 'blockQuote', 'insertTable', 'mediaEmbed', '|', 'redo','undo']
};

const mapStateToProps = (state) => {
    let { editor, data, images, active } = state.ContentEditorReducer;
    return {
        editor: editor,
        data:   data,
        images: images,
        active: active
    };
}

const mapDispatchToProps = (dispatch) => ({
    changeData: data => dispatch(changeData(data)),
    setEditor: editor => dispatch(setEditor(editor)),
    toggleGallery: () => dispatch(toggleGallery())
})

class ContentEditorContainer extends Component {
    constructor(props) {
        super(props)
        let { changeData, toggleGallery } = this.props;
        Axios.get(this.props.TargetURL).then(res => {
            changeData(res.data.body)
        });
        $(document).ready(function(){
            $('.ck-toolbar__items').append('<span class="ck ck-toolbar__separator"></span><button class="ck ck-button ck-off" type="button" tabindex="-1" id="ck-gallery" aria-pressed="false"><i style="font-family: \'Font Awesome 5 Free\' !important;" class="far fa-images"></i><span class="ck ck-tooltip ck-tooltip_s"><span class="ck ck-tooltip__text">اضافه کردن عکس</span></span><span class="ck ck-button__label">عکس</span></button>');
        });
        $(document).on('click', '#ck-gallery', () => toggleGallery())
    }

    render() {
        let { data, setEditor, changeData } = this.props;
        return (
            <div className="rich-text-editor">
                <CKEditor
                    editor={ClassicEditor}
                    data={data}
                    config={editorConfiguration}
                    onInit={ editor => { setEditor(editor) } }
                    onChange={ (e, editor ) => {
                        let body = editor.getData();
                        changeData(body)
                    } }
                    onBlur={ (e, editor ) => {
                        // console.log('Blur.', editor );
                        // console.log(editor.ui.componentFactory.names);
                    } }
                    onFocus={ (e, editor ) => {
                        // console.log('Focus.', editor );
                    } }
                />
            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ContentEditorContainer);