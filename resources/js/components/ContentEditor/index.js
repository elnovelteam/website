import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import ContentEditorContainer from './ContentEditorContainer'
import reducers from './reducers'
import ChooseFile from './ChooseFile';


class ContentEditor extends Component {

    store = createStore(reducers, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());
    render() {
        let {mainpage, target, api_token} = this.props;
        return (
            <Provider store={this.store}>
                <ChooseFile MainPage={mainpage} Target={target} ApiToken={api_token} FileType="image" />
                <ContentEditorContainer {...this.props}/>
            </Provider>
        );
    }
}

export default ContentEditor;