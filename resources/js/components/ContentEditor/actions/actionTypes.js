export const SET_SELECTED_IMAGES = 'SET_SELECTED_IMAGES'
export const TOGGLE_GALLERY      = 'TOGGLE_GALLERY'
export const CHANGE_DATA         = 'CHANGE_DATA'
export const SET_EDITOR          = 'SET_EDITOR'