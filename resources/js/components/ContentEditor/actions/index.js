
import { SET_SELECTED_IMAGES, TOGGLE_GALLERY, CHANGE_DATA, SET_EDITOR } from './actionTypes'

export const setSelectedImages = (images) => ({
    type: SET_SELECTED_IMAGES,
    images: images
})

export const toggleGallery = () => ({
    type: TOGGLE_GALLERY
})

export const changeData = (body) => {
    return {
        type: CHANGE_DATA,
        data: body
    }
}

export const setEditor = (editor) => ({
  type: SET_EDITOR,
  editor: editor  
})