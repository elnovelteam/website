import React, { Component } from 'react'
import InfiniteScroll from 'react-infinite-scroller';
import Axios from 'axios';
import File from './File';
import Thumbnail from './Thumbnail';

export default class PickFiles extends Component {
    constructor(props) {
        super(props);
        let {Usage} = this.props;
        this.state = {
            files: [],
            nextPage: 1,
            hasMore: true,
            selectedFiles: [],
            active: (Usage.substring(0, Usage.indexOf(':')) === 'select') ? false : true
        }
    }
    handleClickFile = (file) => {
        let {Usage} = this.props;
        let select = (Usage.substring(0, Usage.indexOf(':')) === 'select') ? true : false;
        let multiple = (Usage.substring(Usage.indexOf(':') + 1, Usage.length) === 'multiple') ? true : false;
        if (multiple) {
            this.setState(prevState => ({
                selectedFiles: [...prevState.selectedFiles, file],
                title: '',
                alt: ''
            }));
        } else {
            this.setState(prevState => ({
                selectedFiles: [file],
                title: '',
                alt: ''
            }));
        }
    }
    handleSaveFiles = () => {
        let {Usage, Main, ApiToken, CallBack, TargetHTML, CSRF} = this.props;
        let target = `${Main}?api_token=${ApiToken}`;
        let select = (Usage.substring(0, Usage.indexOf(':')) === 'select') ? true : false;
        let multiple = (Usage.substring(Usage.indexOf(':') + 1, Usage.length) === 'multiple') ? true : false;
        let props = {_token: CSRF};
        if (! multiple) {
            props.file_id = this.state.selectedFiles[0].id;
            this.handleToggleFilePicker();
        } else {
            props.selected_files = this.state.selectedFiles[0].id;
        }
        if (select) {
            Axios.defaults.headers.post['Content-Type'] ='application/x-www-form-urlencoded';

            Axios.post(target, props)
            .then(response => {
                let clb = window[CallBack];
                clb.call(undefined, response.data, TargetHTML);
                this.setState(prevState => ({
                    selectedFiles: []
                }));
            })
            .catch(error => {
                console.log('err => ' + error);
            });
        } else {
            let clb= window[CallBack];
            clb.call(undefined, this.state.selectedFiles);
            this.setState(prevState => ({
                selectedFiles: []
            }));
        }
    }
    handleLoadMore = () => {
        const target = `${this.props.Target}/${this.props.FileType}?page=${this.state.nextPage}&api_token=${this.props.ApiToken}`;
        Axios.defaults.headers.post['Content-Type'] ='application/x-www-form-urlencoded';
        Axios.get(target)
        .then(response => {
            const { current_page, last_page, data } = response.data;
            this.setState(prevState => ({
                files: [...prevState.files, ...data],
                hasMore: (current_page === last_page) ? false : true,
                nextPage: current_page + 1
            }));
            console.log(response)
        })
        .catch(error => {
            console.log(error);
        });
    }
    handleToggleFilePicker = () => {
        this.setState(prevState => ({active: (prevState.active) ? false : true}));
    }
    saveButton = () => {
        if (this.state.selectedFiles.length > 0) {
            return (
                <button className="btn btn-sm btn-primary" onClick={this.handleSaveFiles}><i className="fas fa-save"></i></button>
            )
        }
        return ( <div></div> )
    }
    filePickerButton = () => {
        let {FileType, Usage} = this.props;
        let select = (Usage.substring(0, Usage.indexOf(':')) === 'select') ? true : false;
        if (select) {
            return (
                <button className="btn btn-warning" onClick={this.handleToggleFilePicker}><i className={`fas fa-file-${FileType}`}></i></button>
            )
        }
        return (<div></div>)
    }
    render() {
        let {FileType, MainPage} = this.props;
        return (
            <div className="p-3">
                {this.filePickerButton()}
                <div className={`pickfile p-3 ${(this.state.active) ? "active" : false}`}>
                    <button className="btn btn-sm btn-danger" onClick={this.handleToggleFilePicker}><i className="fas fa-times"></i></button>
                    <div className="col-12 p-3 selected-files">
                    {this.state.selectedFiles.map((file, i) => {
                        return (
                            <Thumbnail key={i} Linker={MainPage} FileType={FileType} {...file}/>
                        )
                    })}
                    <div className="col-12 px-0 py-2 float-left">{this.saveButton()}</div>
                    </div>
                    <div style={{ height: '600px', overflow: 'auto' }} className="col-12 mt-3 flaot-left">
                        <InfiniteScroll pageStart={0} loadMore={this.handleLoadMore} hasMore={this.state.hasMore} useWindow={false}>
                        {this.state.files.map((file, i) => {
                            return (
                                <File key={i} Linker={MainPage} FileType={FileType} onClick={() => this.handleClickFile(file)} {...file}/>
                            )
                        })}
                        </InfiniteScroll>
                    </div>
                </div>
            </div>
        )
    }
}
