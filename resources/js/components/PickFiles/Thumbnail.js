import React, { Component } from 'react'

export default class Thumbnail extends Component {
    constructor(props) {
        super(props);
    }
    renderThumbnail = (file) => {
        let {type, thumbnail_path} = file;
        let { FileType, Linker } = this.props;
        return (type !== 'image') ? (<i className={`fas fa-4x fa-file-${FileType}`}></i>) : (<img src={`${Linker}/${thumbnail_path}`}/>);
    }
    render() {
        let {Linker, name, path} = this.props;
        return (
            <a title={name} target="_blank" href={`${Linker}/${path}`} className="thumbnail">{this.renderThumbnail(this.props)}</a>
        )
    }
}
