import React, { Component } from 'react';

class File extends Component {
    constructor(props) {
        super(props);
    }
    renderThumbnail = (file) => {
        let {type, thumbnail_path} = file;
        let { FileType, Linker } = this.props;
        return (type !== 'image') ? (<i className={`fas fa-4x fa-file-${FileType}`}></i>) : (<img src={`${Linker}/${thumbnail_path}`}/>);
    }
    render() {
        let {Linker, onClick, name, path} = this.props;
        return (
            <div className="card-file col-md-2 col-3 float-left mb-3">
                <div className="inner" onClick={onClick}>
                    <div className="cover text-center bg-dark">{this.renderThumbnail(this.props)}</div>
                    <p className="col-12 float-left text-center desc">{name}</p>
                </div>
            </div>
        );
    }
}

export default File;