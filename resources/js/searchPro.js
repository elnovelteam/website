import React, { Component } from 'react'
import ReactDom from 'react-dom';
import SearchProContainer from './components/search/searchPro';

let target = document.getElementById('react-search-screen');
let dataKeyword = target !== null ? target.getAttribute("data-keyword") : null; 
let search = (keyword) => {
    let search_uri = target.getAttribute("search");
    return search_uri.replace('keyword', keyword)
}
let searchPro = (keyword) => {
    let searchPro_uri = target.getAttribute("searchPro");
    return searchPro_uri.replace('keyword', keyword)
}

if(target) {
    ReactDom.render(<SearchProContainer search={search} searchPro={searchPro} dataKeyword={dataKeyword} />, target)
}