import React from 'react';
import ReactDOM from 'react-dom';
import RequestFormContainer from './components/RequestForm';

var targetId    = 'react-request-form';
var RequestForm = document.getElementById(targetId);

if (RequestForm && AuthenticatedEmail.length > 0) {
    ReactDOM.render(<RequestFormContainer Email={AuthenticatedEmail} Quizzes={QuizURLS} FinalURL={FinalURL} />, RequestForm);
}