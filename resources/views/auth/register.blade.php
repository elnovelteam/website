@extends('layouts.auth')

@section('form')
<div class="title col-12 mb-3 p-3 text-center">@lang('auth.register') / <a href="{{ route('login') }}">@lang('auth.login')</a></div>
<form class="col-12" action="{{ route('register') }}" method="post">
	@csrf
    <div class="input-group mb-3 col-12">
        <input type="text" name="name" class="form-control" placeholder="@lang('auth.name')" value="{{ old('name') }}" required autocomplete="name" autofocus>
		<div class="input-group-append">
            <span class="input-group-text" id="basic-addon1"><i class="fas fa-user"></i></span>
        </div>
	</div>
    <div class="input-group mb-3 col-12">
        <input type="text" name="email" class="form-control" placeholder="@lang('auth.email')" value="{{ old('email') }}" required autocomplete="email">
		<div class="input-group-append">
            <span class="input-group-text" id="basic-addon1"><i class="fas fa-envelope"></i></span>
        </div>
	</div>
	<div class="input-group mb-3 col-12">
        <input type="password" name="password" class="form-control" placeholder="@lang('auth.password')" required autocomplete="current-password">
		<div class="input-group-append">
            <span class="input-group-text" id="basic-addon1"><i class="fas fa-lock"></i></span>
        </div>
	</div>
    <div class="input-group mb-3 col-12">
        <input type="password" name="password_confirmation" class="form-control" placeholder="@lang('auth.passwordconfirm')" required>
		<div class="input-group-append">
            <span class="input-group-text" id="basic-addon1"><i class="fas fa-lock"><span class="badge badge-dark">2</span> </i></span>
        </div>
	</div>
    <div class="col-12 input-group mb-3 rtl">
        <p class="float-right">جنسیت : </p>
        <div class="sex-checkbox col-md-6 col-9 float-right">
            <i class="fas fa-2x fa-male ml-3"></i>
            <label class="checkbox-c float-right" for="active">
                <input type="checkbox" name="active" id="active" value="true" checked>
                <span class="check-handle"></span>
            </label>
            <i class="fas fa-2x fa-female mr-3"></i>
        </div>
    </div>
    <div class="input-group mb-3 col-12">
        <button type="submit" class="btn btn-primary col-md-4 offset-md-4 col-12">@lang('auth.register')</button>
	</div>
</form>
<div class="col-md-6 col-12 p-3 float-left oauth">
    <a class="col-12 btn linkedin" href="{{ route('login.driver', ['driver'  => 'linkedIn']) }}">
        <span><i class="fab fa-2x fa-linkedin-in"></i></span>
        <p>@lang('auth.signinLinkedIn')</p>
    </a>
</div>
<div class="col-md-6 col-12 p-3 float-left oauth">
    <a class="col-12 btn google" href="{{ route('login.driver', ['driver'  => 'Google']) }}">
        <span class="p-1 br"><img class="px-2" height="40" width="40" src="{{ asset('images/inc/google-logo.svg') }}" alt=""></span>
        <p>@lang('auth.signinGoogle')</p>
    </a>
</div>
@endsection
