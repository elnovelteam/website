@extends('layouts.auth')

@section('form')
<div class="title col-12 mb-3 p-3 text-center">@lang('auth.resetpassword')</a></div>
<form class="col-12" action="{{ route('password.update') }}" method="post">
	@csrf
    <input type="hidden" name="token" value="{{ $token }}">
    <div class="input-group mb-3 col-12">
        <input id="email" type="email" class="form-control" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>
		<div class="input-group-append">
            <span class="input-group-text" id="basic-addon1"><i class="fas fa-user"></i></span>
        </div>
	</div>
    <div class="input-group mb-3 col-12">
        <input type="password" name="password" class="form-control" placeholder="@lang('auth.password')" required autocomplete="current-password">
		<div class="input-group-append">
            <span class="input-group-text" id="basic-addon1"><i class="fas fa-lock"></i></span>
        </div>
	</div>
    <div class="input-group mb-3 col-12">
        <input type="password" name="password_confirmation" class="form-control" placeholder="@lang('auth.passwordconfirm')" required>
		<div class="input-group-append">
            <span class="input-group-text" id="basic-addon1"><i class="fas fa-lock"><span class="badge badge-dark">2</span> </i></span>
        </div>
	</div>
	<div class="input-group mb-3 col-12">
        <button type="submit" class="btn btn-primary col-md-4 offset-md-4 col-12">ورود</button>
	</div>
</form>
@endsection