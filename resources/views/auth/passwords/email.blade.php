@extends('layouts.auth')

@section('form')
<div class="title col-12 mb-3 p-3 text-center">@lang('auth.resetpassword')</a></div>
<form class="col-12" action="{{ route('password.email') }}" method="post">
	@csrf
    <div class="input-group mb-3 col-12">
        <input type="text" name="email" class="form-control" placeholder="نام کاربری یا ایمیل" aria-label="Username" aria-describedby="basic-addon1">
		<div class="input-group-append">
            <span class="input-group-text" id="basic-addon1"><i class="fas fa-user"></i></span>
        </div>
	</div>
	<div class="input-group mb-3 col-12">
        <button type="submit" class="btn btn-primary col-md-4 offset-md-4 col-12">ورود</button>
	</div>
</form>
@endsection
