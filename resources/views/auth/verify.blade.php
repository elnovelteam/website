@extends('layouts.auth')

@section('form')
<div class="title col-12 mb-3 p-3 text-center">@lang('auth.verifyemail')</div>
@if (session('resent'))
    <div class="col-12 mb-3 p-3 float-left alert alert-dark text-right" role="alert">@lang('auth.freshLinkMessage')</div>
@else
@endif
<form class="col-12" action="{{ route('verification.resend') }}" method="post">
	@csrf
    <div class="input-group mb-3 col-12">
        <p class="text-center col-12 c-white">@lang('auth.checkYourEmailMessage')</p>
        <button type="submit" class="btn btn-dark p-3 m-auto align-baseline">@lang('auth.ifDidnotRecieveEmail')<br>@lang('auth.requestAnother')</button>
	</div>
</form>
@endsection
