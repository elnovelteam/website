<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{ asset('css/auth.css') }}">
    @yield('head')
</head>
<body>
    <div class="content auth">
        <div class="col-md-6 col-12 float-left">
            <div class="login-people col-12">
                <img src="{{ asset('images/inc/login-people.svg') }}" alt="">
            </div>
        </div>
        <div class="col-md-6 col-12 float-right">
            <div class="form-container">
            <div class="logo">
                <img src="{{ asset('images/logo2.png') }}" alt="">
            </div>
            @yield('form')
            </div>
        </div>
    </div>
    <script src="{{ asset('js/app.js') }}"></script>
    @include('partials.error')
</body>
</html>