<?xml version="1.0" encoding="UTF-8"?>

<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:image="http://www.google.com/schemas/sitemap-image/1.1">
    @foreach ($results as $result)
    <url>
        <loc>{{ asset($prefix . $result->slug) }}</loc>
        <lastmod>{{ $result->updated_at->format('Y-m-d') }}</lastmod>
        @if ($result->seo)
        <changefreq>{{ $result->seo->changefreq }}</changefreq>
        <priority>{{ $result->seo->priority }}</priority>
        @endif
        @if ($result->photo)
        <image:image>
            <image:loc>{{ asset($result->photo->file->path) }}</image:loc>
            <image:caption>{{ $result->photo->alt }}</image:caption>
            <image:title>{{ $result->photo->title }}</image:title>
        </image:image>
        @endif
    </url>
    @endforeach
</urlset>