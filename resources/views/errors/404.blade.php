@extends('errors::minimal')

@section('head')
    <title>@lang('messages.errors.404')</title>
@endsection

@section('body')
    <div class="people-middle">
        <h1 class="font title">@lang('messages.errors.404').</h1>
        <img src="{{ asset('images/inc/404-main.svg') }}" alt="">
    </div>
    <div class="logo-container">
        <a href="{{ asset('/') }}"><img src="{{ asset('images/elnovel-logo-100.png') }}" alt="Elnovel"></a>
    </div>
@endsection