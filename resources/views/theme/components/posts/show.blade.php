@extends('theme.page')

@section('header')
    <title>{{ $post->title }}</title>
    @component('theme.components.metas', ['seo' => $post->seo, 'target' => $post]) @endcomponent
@endsection

@section('main-content')
    <div class="content-container col-12 col-md-9 col-lg-7">
        <h1>{{ $post->title }}</h1>
        @if ($post->photo)
            <img class="mb-4" src="{{ asset($post->photo->file->thumbnail_path) }}" data-src="{{ asset($post->photo->file->path) }}" alt="{{ $post->photo->file->alt }}">
        @endif
        <span class="date">{{ $post->created_at->diffForHumans() }}</span>
        @guest
            <span>{{ $post->likes_count }} <i class="fa fa-heart text-danger"></i></span>
        @else
            <span>{{ $post->likes_count }} <i class="fa fa-heart text-danger"></i></span>
            <div id="react-like-section"
                data-likes="{{ route('api.userApi.liking', ['target' => 'post', 'targetId' => 'targetId']) }}"
                data-apiKey="{{ auth()->user() ? auth()->user()->api_token : null }}"
            ></div>
        @endguest
        <div class="body-container mt-3">
            {!! $post->body !!}
        </div>
        <h3>سوالات و نظرات</h3>
        <div id="react-comments-section"
            data-comments="{{ route('api.userApi.comments', ['target' => 'post', 'targetId' => $post->id]) }}"
            data-commenting="{{ route('api.userApi.commenting', ['target' => 'post', 'targetId' => $post->id]) }}"
            data-likes="{{ route('api.userApi.liking', ['target' => 'comment', 'targetId' => 'commentId']) }}"
            data-apiKey="{{ auth()->user() ? auth()->user()->api_token : null }}"
        ></div>
    </div>
@endsection

@section('scripts')

@endsection