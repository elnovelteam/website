@extends('theme.page')

@section('header')
    <title>وبلاگ</title>
    {{-- @component('theme.components.metas', ['seo' => $post->seo, 'target' => $post]) @endcomponent --}}
@endsection

@section('main-content')
    <div class="col-12 mb-4 section posts-page">
        <div class="title-row float-left mb-5">
            <h1 class="title">نوشته ها</h1>
            <span class="underline col-sm-2 offset-sm-5 col-8 offset-2 mt-3">
                <span class="circular-icon"><i class="fas fa-book-open"></i></span>
            </span>
        </div>
        <div class="posts-container col-12 col-xl-10">
            @foreach ($posts as $post)
            <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                <div class="animated flash">
                    @if ($post->photo)
                        <img src="{{ $post->photo->file->thumbnail_path }}" data-src="{{ $post->photo->file->path }}" alt="{{ $post->photo->file->alt }}">
                    @endif
                    <div class="overlay">
                        <div class="float-right badge badge-sm badge-pill">
                            <span>{{ $post->likes_count }}</span>
                            <i class="fas fa-heart text-danger"></i>
                        </div>
                        <div class="float-left badge badge-sm badge-pill">
                            <span>{{ $post->last_views_count }}</span>
                            <i class="fas fa-eye text-dark"></i>
                        </div>
                    </div>
                </div>
                <div class="post-info-container p-2 animated bounce">
                    <div class="p-1">
                        <h3 class="text-dark post-title"><a href="{{ route('blog.show', ['blog' => $post->slug]) }}">{{ $post->title }}</a></h3>
                        <p class="text-secondary post-desc">{{ $post->summary }}</p>
                    </div>
                    <div class="see-section col-12 float-left mb-2">
                        <a href="{{ route('blog.show', ['blog' => $post->slug]) }}" class="btn btn-outline-dark btn-sm pd-3 float-left">مشاهده <i class="fas fa-eye"></i></a>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
        <div class="col-12 pagination-section">
            {{ $posts->links() }}
        </div>
    </div>
@endsection