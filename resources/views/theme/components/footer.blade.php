<footer>
    <div class="col-md-3 col-12">
        <h6 class="footer-title">ال ناول</h6>
        <p>این جمله ای که داری میخونی قراره بعدا بشه درباره ما ولی خب فعلا به دلیل کمبود وقت مجبوریم جاش همین کلمات رو جایگزین کنیم.
            خلاصه فعلا با همین حال کنید تا یک چیزی واسه جایگزین پیدا کنیم
        </p>
    </div>
    <div class="col-md-3 col-12">
        <h6 class="footer-title">صفحات</h6>
        <ul class="col-md-6 float-right">
            <li><a href="/">خانه</a></li>
            <li><a href="/novels">رمان ها</a></li>
            <li><a href="/generes">ژانرها</a></li>
        </ul>
        <ul class="col-md-6 float-right">
            <li><a href="/podcasts">پادکست ها</a></li>
            <li><a href="/profile">حساب کاربری</a></li>
        </ul>
    </div>
    <div class="col-md-3 col-12">
        <h6 class="footer-title">ارتباط با ما</h6>
        <ul class="non-dots">
            <li><i class="fas fa-building ml-3"></i>دفتر مرکزی : مشهد ، سه راه ادبیات ، کوچه کتاب ، پلاک 22</li>
            <li><a class="phone" href="mailto:info@elnovel.net"><i class="fas fa-envelope-open"></i>info(@)elnovel(.)net</a></li>
            <li><a class="phone" href="tel:+989150013422"><i class="fas fa-phone"></i>0915 001 34 22</a></li>
            <li><a class="phone" href="tel:+989015074027"><i class="fas fa-phone"></i>0901 507 40 27</a></li>
            <li><a class="phone" href="tel:+985138859010"><i class="fas fa-tty"></i>051-38 85 90 10</a></li>
        </ul>
    </div>
    <div class="col-md-3 col-12">
        <h6 class="footer-title">شبکه های اجتماعی</h6>
        <ul class="social-media">
            <li><a href="https://instagram.com/elnovelofficial"><i class="fab fa-instagram"></i></a></li>
            <li><a href="https://t.me/elnovel"><i class="fas fa-paper-plane"></i></a></li>
            <li><a href="https://www.youtube.com/channel/UCDpTgLgveZqx7le4PYQfHfA"><i class="fab fa-youtube"></i></a></li>
            <li><a href="https://twitter.com/ElnovelOfficial"><i class="fab fa-twitter"></i></a></li>
            {{-- <li><a href="#social-media"><i class="fab fa-facebook-f"></i></a></li> --}}
            <li><a href="https://api.whatsapp.com/send?phone=989150013422&text=%D8%B3%D9%84%D8%A7%D9%85"><i class="fab fa-whatsapp"></i></a></li>
            <li><a href="https://www.linkedin.com/company/elnovel/"><i class="fab fa-linkedin"></i></a></li>
			<li><a href="https://soundcloud.com/elnovel"><i class="fab fa-soundcloud"></i></a></li>
        </ul>
    </div>
    <div class="col-12 pt-3 bottom-footer">
        <p class="col-12 text-center copyright">COPYRIGHT 2020 &COPY; ELNOVEL Co.<br>ALL RIGHTS RESERVED</p>
    </div>
</footer>
