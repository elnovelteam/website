@if ($seo)
    <meta name="description" content="{{ $seo->description }}">
    <meta name="keywords" content="{{ organize_keywords_for_seo($seo->keywords) }}">
    <meta name="robots" cotnent="{{ $seo->robots }}">
    <meta name="copyright" content="{{ config('app.name') }}">
    <meta name="author" content="{{ config('app.author') }}">
    <meta name="application-name" content="{{ config('app.name') }}">
    <meta name="shortcut icon" type="image/x-icon" href="">
    <link rel="canonical" href="{{ config('app.url') }}">
    <meta property="og:site_name" content="{{ config('app.name') }}" />
    <meta property="og:type" content="article" />
    <meta property="og:title" content="{{ $target->title }}" />
@if($target->photo)
    <meta property="og:image" content="{{ asset($target->photo->file->path) }}" />
@endif
    <meta property="og:url" content="{{ url()->current() }}" />
@if($target->description)
    <meta property="og:description" content="{{ $target->description }}" />
@endif
    <meta name="twitter:title" content="{{ $target->title }}">
@if($target->description)
    <meta name="twitter:description" content="{{ $target->description }}">
@endif
@if($target->photo)
    <meta name="twitter:image" content="{{ asset($target->photo->file->path) }}">
@endif
    <meta name="twitter:site" content="@USERNAME">
    <meta name="twitter:creator" content="@USERNAME">
@endif
{{-- <link rel="alternate" type="application/rss+xml" href="{{ asset('/events.rss') }}" title="Upcoming Events"> --}}