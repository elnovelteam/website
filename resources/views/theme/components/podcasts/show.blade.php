@extends('theme.page')
@section('header')
    <title>{{ $podcast->title }}</title>
    @component('theme.components.metas', ['seo' => $podcast->seo, 'target' => $podcast]) @endcomponent
    <link rel="stylesheet" href="{{ asset('css/audio-player.css') }}">
@endsection
@section('main-content')
<div class="col-12 mb-4 section podcast-top-section">
    <div class="col-md-6 col-12 float-left">
        <h1 class="title float-left text-center col-12" itemprop="headline">{{ $podcast->title }}</h1>
        <div class="col-12 text-center float-left m-0"><i class="fas fa-podcast fa-4x"></i></div>
        <div class="col-lg-4 offset-lg-4 col-md-6 offset-md-3 col-sm-10 offset-sm-1 col-12 mb-4 float-left categories">
            @foreach ($podcast->categories as $cat)
                <a href="#{{ $cat->info->title }}" class="badge badge-pill badge-dark">{{ $cat->info->title }}</a>
            @endforeach
            <div class="col-12 float-left text-center"><span class="badge badge-pill badge-info"><i class="fas fa-eye"></i>{{ views($podcast)->unique()->count() }}</span></div>
        </div>
        <p class="float-left top-description text-right">{{ $podcast->description }}</p>
    </div>
    <div class="col-md-6 col-12 float-left">
        <audio class="float-left d-none" id="podcast-player" preload="metadata" controls>
            <source src="https://anchor.fm/s/7db9a50/podcast/play/2723195/https%3A%2F%2Fd3ctxlq1ktw2nl.cloudfront.net%2Fproduction%2F2019-2-23%2F11785436-48000-2-0f5e79df47ceb.mp3">
        </audio>
        <div class="col-12 float-left podcast-container" id="react-audio-player"
        data-avatar="{{ asset($podcast->photo->file->thumbnail_path) }}" data-title="{{ $podcast->title }}" data-subtitle=""
        target-audio="podcast-player"></div>
    </div>
</div>
<div class="col-12 mb-4 section text-center">
    <div class="photo-cover mb-4">
        <img src="{{ asset($podcast->photo->file->path) }}" alt="{{ asset($podcast->photo->alt) }}">
        <span class="curl"></span>
    </div>
    {!! $podcast->body !!}
</div>
@endsection
@section('scripts')
    <script src="{{ asset('js/audio-player.js') }}"></script>
    <script>
        $('.volume-slider').on('dragstart', (e) => {
           console.log(e.dataTransfer);
        });
    // let podcast = document.getElementById('podcast-player');
    // podcast.addEventListener("canplay",function(){
    //     console.log(podcast.duration);
    //     $("#length").text("Duration:" + podcast.duration + " seconds");
    //     $("#source").text("Source:" + podcast.src);
    //     $("#status").text("Status: Ready to play").css("color","green");
    // });
    </script>
@endsection