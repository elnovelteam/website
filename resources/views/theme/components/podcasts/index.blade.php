@extends('theme.page')
@section('header')
    <title>پادکست ها</title>
@endsection
@section('main-content')
<div class="col-12 mb-4 section svg-section">
    <div class="col-md-6 col-12 p-3">
        <img src="{{ asset('images/inc/two-people-podcasting.svg') }}" alt="">
    </div>
    <div class="col-md-6 col-12 p-3">
        <div class="title-row mb-3 float-left">
            <h1 class="title">پادکست Elnovel</h1>
            <span class="underline col-sm-4 offset-sm-4 col-8 offset-2 mt-3">
                <span class="circular-icon"><i class="fas fa-podcast"></i></span>
            </span>
        </div>
        <p>با توجه به اهداف Elnovel درحوزه ادبیات – به ویژه ادبیات داستانی – تیم ما بسترسازی های گسترده ایی در زمینه نشر صوتی فراهم کرده است . در صفحه پادکست می توانید ازاین خدمات استفاده کنید , در این راستا علاوه بر پلتفرم سایت , اپلیکشین اختصاصی " ELNOVEL " نیز تنظیم و ساخته شده است تا شما بتوانید ضمن بهره مندی از پادکست های گوناگون ادبی , به سادگی از دیگر دسترسی های موجود در سایت ما استفاده کنید . شما می توانید از این لینک اپلیکیشن ما را دانلود کنید به صورت مستقیم دانلود کنید یا از طریق .... اقدام به دانلود از اپلیکیشن ما کنید .</p>
        <p> در این جا مطالب مختلفی نظیر خوانش برخی از داستان های سایت , مقالات , مناظره های ادبی , اخبار کتاب
        ، معرفی داستان نویسان سایت , پادکست های کارگاه نقد و خوانش داستان , تازه های ال ناول و ...گردآوری شده است تا در دسترسی کامل شما عزیزان قرار گیرد . می توانید از دسته بندی های مورد نظر برای پیدا کردن پادکست مورد نظر خود استفاده کنید .
        </p>
        <p>ضمنا علاوه بر سایت و اپلیکیشن می توانید پادکست های اختصاصی تیم Elnovelرا  از سازوکار های زیر دنبال کنید .</p>
        <div class="platforms float-left mt-4 col-12">
            <a href="#" class="btn btn-outline-dark platform"><img src="{{ asset('images/inc/castbox-logo.svg') }}" alt=""><span>Castbox</span></a>
            <a href="#" class="btn btn-outline-dark platform"><img src="{{ asset('images/inc/soundcloud-logo.svg') }}" alt=""><span>SoundCloud</span></a>
            <a href="#" class="btn btn-outline-dark platform"><img src="{{ asset('images/inc/google-podcasts.svg') }}" alt=""><span>G Podcast</span></a>
            <a href="#" class="btn btn-outline-dark platform"><img src="{{ asset('images/inc/apple-podcast-logo.svg') }}" alt=""><span>iTunes</span></a>
            <a href="#" class="btn btn-outline-dark platform"><img src="{{ asset('images/inc/overcast.svg') }}" alt=""><span>Overcast</span></a>
            <a href="#" class="btn btn-outline-dark platform"><img src="{{ asset('images/inc/spotify-logo.svg') }}" alt=""><span>Spotify</span></a>
            <a href="#" class="btn btn-outline-dark platform rght"><img src="{{ asset('images/inc/namlik-logo.png') }}" alt=""><span>ناملیک</span></a>
        </div>
    </div>
</div>
<div class="col-12 mb-4 section">
    <div class="title-row mb-3 float-left">
        <h4 class="title">برنامه ها</h4>
        <span class="underline col-sm-2 offset-sm-5 col-8 offset-2 mt-3">
            <span class="circular-icon"><i class="fas fa-headphones-alt"></i></span>
        </span>
    </div>
    <div class="swiper-container itemslider podcastslider col-12 mt-3 float-left">
        <div class="swiper-wrapper">
            <div class="swiper-slide">
                <div class="cover">
                    <div class="toolbox">
                        <i class="fas fa-share-alt sharethis"></i>
                        <i class="fas fa-heart likethis"></i>
                        <i class="far fa-bookmark bookmarkthis"></i>
                    </div>
                    <img src="./images/podcast-cover.jpg" alt="">
                </div>
                <div class="info">
                    <h3 class="book-title">اسپارک</h3>
                    <p class="book-subtitle">داستانی بهر آنهایی که نمیدانند چگونه است ...</p>
                    <div class="generes">
                        <a href="/categories/fantasy" class="badge badge-secondary badge-pill">فانتزی</a>
                        <a href="/categories/fiction" class="badge badge-secondary badge-pill">تخیلی</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- Add Pagination -->
        <div class="swiper-button-prev"><i class="fas fa-3x fa-angle-left"></i></div>
        <div class="swiper-button-next"><i class="fas fa-3x fa-angle-right"></i></div>
    </div>
</div>
<div class="col-12 mb-4 section">
    <div class="title-row mb-3 float-left">
        <h4 class="title">اپیزود ها</h4>
        <span class="underline col-sm-2 offset-sm-5 col-8 offset-2 mt-3">
            <span class="circular-icon"><i class="fas fa-play"></i></span>
        </span>
    </div>
    <div class="col-12 float-left p-2 podcastlist">
        @foreach ($podcasts as $podcast)
        <div class="item-container col-lg-3 col-md-4 col-sm-6 col-12">
            <div class="item p-0 float-left col-12">
                <div class="cover p-0 float-left col-12">
                    <img src="{{ asset($podcast->photo->file->thumbnail_path) }}" title="{{ $podcast->photo->title }}" alt="{{ $podcast->photo->alt }}">
                </div>
                <div class="info p-3 float-left col-12">
                    <a href="{{ route('podcasts.show', ['podcast' => $podcast->slug]) }}" class="text-center mb-3 info-title">{{ $podcast->title }}</a>
                    <p class="col-12 text text-right float-left">{{ $podcast->description }}</p>
                </div>
                <div class="col-12 p-3 float-left">
                    <a href="{{ route('podcasts.show', ['podcast' => $podcast->slug]) }}" class="btn btn-outline-dark float-left">بشنوید <i class="fas fa-volume-up float-left mr-1"></i></a>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>
<div class="col-12 mb-4 section">
    <div class="title-row mb-3 float-left">
        <h4 class="title">محبوب ترین اپیزود ها</h4>
        <span class="underline col-sm-2 offset-sm-5 col-8 offset-2 mt-3">
            <span class="circular-icon"><i class="fas fa-headphones-alt"></i></span>
        </span>
    </div>
    <div class="swiper-container itemslider showslider col-12 mt-3 float-left">
        <div class="swiper-wrapper">
            <div class="swiper-slide">
                <div class="cover">
                    <div class="toolbox">
                        <i class="fas fa-share-alt sharethis"></i>
                        <i class="fas fa-heart likethis"></i>
                        <i class="far fa-bookmark bookmarkthis"></i>
                    </div>
                    <img src="./images/podcast-cover.jpg" alt="">
                </div>
                <div class="info">
                    <h3 class="book-title">اسپارک</h3>
                    <p class="book-subtitle">داستانی بهر آنهایی که نمیدانند چگونه است ...</p>
                    <div class="generes">
                        <a href="/categories/fantasy" class="badge badge-secondary badge-pill">فانتزی</a>
                        <a href="/categories/fiction" class="badge badge-secondary badge-pill">تخیلی</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- Add Pagination -->
        <div class="swiper-button-prev"><i class="fas fa-3x fa-angle-left"></i></div>
        <div class="swiper-button-next"><i class="fas fa-3x fa-angle-right"></i></div>
    </div>
</div>
<div class="col-12 mb-4 section">
    <div class="title-row mb-3 float-left">
        <h4 class="title">جدیدترین اپیزود ها</h4>
        <span class="underline col-sm-2 offset-sm-5 col-8 offset-2 mt-3">
            <span class="circular-icon"><i class="fas fa-headphones-alt"></i></span>
        </span>
    </div>
    <div class="swiper-container itemslider newslider col-12 mt-3 float-left">
        <div class="swiper-wrapper">
            <div class="swiper-slide">
                <div class="cover">
                    <div class="toolbox">
                        <i class="fas fa-share-alt sharethis"></i>
                        <i class="fas fa-heart likethis"></i>
                        <i class="far fa-bookmark bookmarkthis"></i>
                    </div>
                    <img src="./images/podcast-cover.jpg" alt="">
                </div>
                <div class="info">
                    <h3 class="book-title">اسپارک</h3>
                    <p class="book-subtitle">داستانی بهر آنهایی که نمیدانند چگونه است ...</p>
                    <div class="generes">
                        <a href="/categories/fantasy" class="badge badge-secondary badge-pill">فانتزی</a>
                        <a href="/categories/fiction" class="badge badge-secondary badge-pill">تخیلی</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- Add Pagination -->
        <div class="swiper-button-prev"><i class="fas fa-3x fa-angle-left"></i></div>
        <div class="swiper-button-next"><i class="fas fa-3x fa-angle-right"></i></div>
    </div>
</div>
<div class="col-12 mb-4 section">
    <div class="title-row mb-3 float-left">
        <h3 class="title">همکاری با ما</h3>
        <span class="underline col-sm-4 offset-sm-4 col-8 offset-2 mt-3">
            <span class="circular-icon"><i class="fas fa-podcast"></i></span>
        </span>
    </div>
    <p style="line-height: 36px;" class="col-md-8 offset-md-2 col-12 float-left text-justify"> اگر شما هم در زمینه تهیه و ساخت پادکست با موضوعات ادبی اعم از خوانش داستان یا مقالات فعالیت دارید بهتر است بدانید ما از نشر پادکست و برنامه های صوتی شما به صورت رایگان و قانون مند چه در سایت و چه در اپلیکیشن Elnovelحمایت می کنیم , ما در ال ناول پشتیبان تمامی دوستداران پیشرفت ادبیات هستیم و نشر و حمایت از انجمن ها و استارت آپ های ادبی را بر عهده می گیریم . تنها کافیست برای مشارکت و آغاز همکاری دوطرفه به ادرس podcast[@]elnovel[.]net  یک ایمیل ارسال کنید یا با ما از طریق ارسال درخواست پشتیبانی به تیم پادکست  و یا  تماس تلفنی ارتباط  برقرار کنید در ال ناول تمام تلاش ما برای پیشرفت ادبیات داستانی در کنار حفظ قانون نشر و حق کپی است .</p>
</div>
@endsection
@section('scripts')
    <script>
        var swiper2 = new Swiper.default('.podcastslider', {
            slidesPerView: 4,
            spaceBetween: 10,
            breakpoints: {
                320: {
                    slidesPerView: 1,
                    spaceBetween: 20,
                },
                500: {
                    slidesPerView: 2,
                    spaceBetween: 20,
                },
                768: {
                    slidesPerView: 5,
                    spaceBetween: 20,
                },
                2560: {
                    slidesPerView: 6,
                    spaceBetween: 40
                }
            },
            navigation: {
                nextEl: '.podcastslider > .swiper-button-next',
                prevEl: '.podcastslider > .swiper-button-prev',
            },
        });
        var swiper3 = new Swiper.default('.showslider', {
            slidesPerView: 4,
            spaceBetween: 10,
            breakpoints: {
                320: {
                    slidesPerView: 1,
                    spaceBetween: 20,
                },
                500: {
                    slidesPerView: 2,
                    spaceBetween: 20,
                },
                768: {
                    slidesPerView: 5,
                    spaceBetween: 20,
                },
                2560: {
                    slidesPerView: 6,
                    spaceBetween: 40
                }
            },
            navigation: {
                nextEl: '.showslider > .swiper-button-next',
                prevEl: '.showslider > .swiper-button-prev',
            },
        });
        var swiper4 = new Swiper.default('.newslider', {
            slidesPerView: 4,
            spaceBetween: 10,
            breakpoints: {
                320: {
                    slidesPerView: 1,
                    spaceBetween: 20,
                },
                500: {
                    slidesPerView: 2,
                    spaceBetween: 20,
                },
                768: {
                    slidesPerView: 5,
                    spaceBetween: 20,
                },
                2560: {
                    slidesPerView: 6,
                    spaceBetween: 40
                }
            },
            navigation: {
                nextEl: '.showslider > .swiper-button-next',
                prevEl: '.showslider > .swiper-button-prev',
            },
        }); </script>
@endsection
