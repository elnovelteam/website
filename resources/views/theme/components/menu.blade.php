<nav class="menu">
    <div class="logo">
        <a href="{{ route('home') }}"><img src="{{ route('home') }}/images/logo.png" alt=""></a>
    </div>
    <ul>
        <li><a href="{{ asset('/') }}">خونه</a></li>
        <li><a href="{{ asset('/novels') }}">رمان ها</a></li>
        <li><a href="{{ asset('/writers') }}">داستان نویس ها</a></li>
        <li><a href="{{ asset('/podcasts') }}">پادکست ها</a></li>
        <li><a href="{{ route('blog.index') }}">وبلاگ</a></li>
        <li><a href="{{ asset('/contact') }}">تماس با ما</a></li>
        <li><a href="{{ route('search') }}">جستجو</a></li>
    </ul>
    <div class="last col-sm-6 col-12">
    <div class="btn btn-dark float-left d-sm-none d-block" id="menu-opener">
        <i class="fas fa-2x fa-bars"></i>
    </div>
    @guest
    <p class="float-left text-center col-lg-12 col-8 white h-100">
        <span id="search-icon" class="search-icon"><i class="fas fa-search"></i></span>
        <span class="search-icon">
            <a class="mt-0" href="{{ route('register') }}"><i class="fas fa-user"></i> عضویت</a>
            <a class="mt-0" href="{{ route('login') }}"><i class="fas fa-lock"></i> ورود</a>
        </span>
    </p>
    @else
        <p class="float-left text-center col-lg-12 col-8 white h-100">
            <a class="float-left text-center search-icon d-sm-block d-none" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
            @lang('auth.logout') <i class="fas fa-sign-out-alt"></i>
            </a>
            <span id="search-icon" class="search-icon"><i class="fas fa-search"></i></span>
            <a href="{{ route('userarea.') }}" class="search-icon">{{ ucfirst(auth()->user()->name) }} <i class="fas fa-user"></i></a>
        </p>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
        </form>
    @endguest
    </div>
</nav>
