<div
    id="react-search" 
    class="search-section col-12"
    search={{ route('api.platform.search', ['keyword' => 'keyword']) }}
    searchPro={{ route('api.platform.search.pro', ['keyword' => 'keyword']) }}
></div>