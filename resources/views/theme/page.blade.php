@extends('theme.default')
@section('content')
<header class="page">
    @include('theme.components.menu')
</header>
<div class="content margined">
    @yield('main-content')
</div>
@include('theme.components.footer')
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/comments.js') }}"></script>
<script src="{{ asset('js/searchPro.js') }}"></script>
@endsection