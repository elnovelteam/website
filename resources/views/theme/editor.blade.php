@extends('theme.default')
@section('header')
    <title>El Novel</title>
@endsection

@section('content')
<h1>Editor</h1>
<div id="react-ck-editor"></div>
@endsection

@section('scripts')
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/ck-editor.js') }}"></script>
@endsection
