@extends('theme.default')
@section('header')
    <title>El Novel</title>
@endsection
@section('content')
{{-- HEADER --}}
@include('theme.components.header')
{{-- CONTENT --}}
<div class="content m-0">
    <div class="col-12 boxes-container">
        <div class="col-md-4 col-12 box">
            <div class="inner">
                <div class="icon"><i class="fas fa-book"></i></div>
                <h3 class="title">رمان نویسی</h3>
                <p class="justify-text">رمان های خودتون رو به اسم خودتون در سایت ما منتشر کنید.</p>
                <button class="btn btn-primary">بیشتر</button>
            </div>
        </div>
        <div class="col-md-4 col-12 box">
            <div class="inner">
                <div class="icon"><i class="fas fa-headphones"></i></div>
                <h3 class="title">پادکست ها</h3>
                <p class="justify-text">لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.</p>
                <button class="btn btn-purple">بیشتر</button>
            </div>
        </div>
        <div class="col-md-4 col-12 box">
            <div class="inner">
                <div class="icon"><i class="fas fa-pencil-alt"></i></div>
                <h3 class="title">لورم ایپسوم</h3>
                <p class="justify-text">لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.</p>
                <button class="btn btn-secondary">بیشتر</button>
            </div>
        </div>
    </div>
    <div class="col-12 mb-4 section">
        <div class="title-row float-left">
            <h4 class="title">رمان ها</h4>
            <span class="underline col-sm-2 offset-sm-5 col-8 offset-2 mt-3">
                <span class="circular-icon"><i class="fas fa-book-open"></i></span>
            </span>
        </div>
        <div class="swiper-container itemslider bookslider col-12 mt-3 float-left">
            <div class="swiper-wrapper">
                <div class="swiper-slide">
                    <div class="cover">
                        <div class="toolbox">
                            <i class="fas fa-share-alt sharethis"></i>
                            <i class="fas fa-heart likethis"></i>
                            <i class="far fa-bookmark bookmarkthis"></i>
                        </div>
                        <img src="./images/cover1.png" alt="">
                    </div>
                    <div class="info">
                        <h3 class="book-title">اسپارک</h3>
                        <p class="book-subtitle">داستانی بهر آنهایی که نمیدانند چگونه است ...</p>
                        <div class="generes">
                            <a href="/categories/fantasy" class="badge badge-secondary badge-pill">فانتزی</a>
                            <a href="/categories/fiction" class="badge badge-secondary badge-pill">تخیلی</a>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="cover">
                        <div class="toolbox">
                            <i class="fas fa-share-alt sharethis"></i>
                            <i class="far fa-heart likethis"></i>
                            <i class="far fa-bookmark bookmarkthis"></i>
                        </div>
                        <img src="./images/cover2.jpg" alt="">
                    </div>
                    <div class="info">
                        <h3 class="book-title">تراژدی تلخ من</h3>
                        <p class="book-subtitle">تراژدی تلخ من داستان زندگانی است که به مانند قبرستان سرد و تاریک بود و به مانند استخوان شکننده و بی پروا ...</p>
                        <div class="generes">
                            <a href="/categories/fantasy" class="badge badge-secondary badge-pill">تراژدی</a>
                            <a href="/categories/fiction" class="badge badge-secondary badge-pill">عاشقانه</a>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="cover">
                        <div class="toolbox">
                            <i class="fas fa-share-alt sharethis"></i>
                            <i class="far fa-heart likethis"></i>
                            <i class="far fa-bookmark bookmarkthis"></i>
                        </div>
                        <img src="./images/cover3.jpg" alt="">
                    </div>
                    <div class="info">
                        <h3 class="book-title">هری پاتر و بذر جادویی</h3>
                        <p class="book-subtitle">ماجراجویی های ترسناک و عاشقانه هری پاتر</p>
                        <div class="generes">
                            <a href="/categories/horror" class="badge badge-secondary badge-pill">ترسناک</a>
                            <a href="/categories/love" class="badge badge-secondary badge-pill">عاشقانه</a>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="cover">
                        <div class="toolbox">
                            <i class="fas fa-share-alt sharethis"></i>
                            <i class="far fa-heart likethis"></i>
                            <i class="far fa-bookmark bookmarkthis"></i>
                        </div>
                        <img src="./images/cover4.jpg" alt="">
                    </div>
                    <div class="info">
                        <h3 class="book-title">انکلادوس</h3>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="cover">
                        <div class="toolbox">
                            <i class="fas fa-share-alt sharethis"></i>
                            <i class="far fa-heart likethis"></i>
                            <i class="far fa-bookmark bookmarkthis"></i>
                        </div>
                        <img src="./images/cover5.jpg" alt="">
                    </div>
                    <div class="info">
                        <h3 class="book-title">عصر جادوگران</h3>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="cover">
                        <div class="toolbox">
                            <i class="fas fa-share-alt sharethis"></i>
                            <i class="far fa-heart likethis"></i>
                            <i class="far fa-bookmark bookmarkthis"></i>
                        </div>
                        <img src="./images/cover6.jpg" alt="">
                    </div>
                    <div class="info">
                        <h3 class="book-title">قبلا نامش هریت بود</h3>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="cover">
                        <div class="toolbox">
                            <i class="fas fa-share-alt sharethis"></i>
                            <i class="far fa-heart likethis"></i>
                            <i class="far fa-bookmark bookmarkthis"></i>
                        </div>
                        <img src="./images/cover7.jpg" alt="">
                    </div>
                    <div class="info">
                        <h3 class="book-title">راس نستولد</h3>
                    </div>
                </div>
            </div>
            <!-- Add Pagination -->
            <div class="swiper-button-prev"><i class="fas fa-3x fa-angle-left"></i></div>
            <div class="swiper-button-next"><i class="fas fa-3x fa-angle-right"></i></div>
        </div>
    </div>
    <div class="col-12 mb-4 section">
        <div class="title-row float-left">
            <h4 class="title">پادکست ها</h4>
            <span class="underline col-sm-2 offset-sm-5 col-8 offset-2 mt-3">
                <span class="circular-icon"><i class="fas fa-headphones-alt"></i></span>
            </span>
        </div>
        <div class="swiper-container itemslider podcastslider col-12 mt-3 float-left">
            <div class="swiper-wrapper">
                <div class="swiper-slide">
                    <div class="cover">
                        <div class="toolbox">
                            <i class="fas fa-share-alt sharethis"></i>
                            <i class="fas fa-heart likethis"></i>
                            <i class="far fa-bookmark bookmarkthis"></i>
                        </div>
                        <img src="./images/podcast-cover.jpg" alt="">
                    </div>
                    <div class="info">
                        <h3 class="book-title">اسپارک</h3>
                        <p class="book-subtitle">داستانی بهر آنهایی که نمیدانند چگونه است ...</p>
                        <div class="generes">
                            <a href="/categories/fantasy" class="badge badge-secondary badge-pill">فانتزی</a>
                            <a href="/categories/fiction" class="badge badge-secondary badge-pill">تخیلی</a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Add Pagination -->
            <div class="swiper-button-prev"><i class="fas fa-3x fa-angle-left"></i></div>
            <div class="swiper-button-next"><i class="fas fa-3x fa-angle-right"></i></div>
        </div>
    </div>
    <div class="col-12 p-0 inside-section">
        <div class="section-content p-4">
            <div class="cooperation-content-container">
                <div class="float-right col-12 col-md-6 text-right">
                    <h2 class="text-primary">همکاری با Elnovel</h2>
                    <p>ما در Elnovel فراتر از یک شبکه اجتماعی برای ادبیات داستانی و کتاب , بلکه یک خانواده هستیم ...
                        خانواده اِل ناول در جهت حمایت و توسعه ادبیات داستانی و فعالیت های مربوط به حوزه داستان از تمامی علاقه مندان داستان دعوت به عمل می آورد .
                        در ال ناول ما همیشه آماده به همکاری برای بهسازی ادبیات داستانی فارسی هستیم , از ایده های شما حمایت می کنیم و در جهت گسترش و عملی کردن آن ها پشتیبان شما هستیم .
                        بیایید در کنار هم به دنبال ترقی نهال داستان , این دانه انسان باشیم , تنها کافیست اطلاعات و نوع همکاری خود را برای ما ارسال کنید , در کمترین زمان ممکن ما با شما در ارتباط خواهیم بود .
                        خواهش مندیم به طور خلاصه از ایده ها و توانمندی هایتان در جهت بستر سازی همکاری برای ما بنویسید و ارسال کنید .
                    </p>
                </div>
                <div class="float-left col-12 col-md-6">
                    <form action="#" method="post">
                        <div class="input-group mb-3">
                            <input type="text" class="form-control cooperation-input" placeholder="نام و نام خانوادگی" aria-label="نام و نام خانوادگی" aria-describedby="basic-addon2">
                            <div class="input-group-append">
                                <span class="input-group-text" id="basic-addon2"><i class="fas fa-user"></i></span>
                            </div>
                        </div>
                        <div class="input-group mb-3">
                            <input type="email" class="form-control cooperation-input" placeholder="آدرس ایمیل" aria-label="آدرس ایمیل" aria-describedby="basic-addon2">
                            <div class="input-group-append">
                                <span class="input-group-text" id="basic-addon2"><i class="fas fa-at"></i></span>
                            </div>
                        </div>
                        <div class="input-group lilselect">
                            <select class="form-control" name="cooperation" id="cooperation">
                                <option></option>
                                <option value="1">نقدنامه نویسی</option>
                                <option value="2">جریده نگاری ادبی</option>
                                <option value="3">گویندگی پادکست</option>
                                <option value="4">نشر پادکست</option>
                                <option value="5">نویسندگی</option>
                                <option value="6">فیلم برداری</option>
                                <option value="7">وبلاگ نویسی</option>
                                <option value="8">کاریکلیماتور</option>
                                <option value="9">مترجم ادبی</option>
                                <option value="10">سرمایه گذاری</option>
                            </select>
                            <div class="input-group-append float-right">
                                <span class="input-group-text"><i class="fas fa-handshake "></i></span>
                            </div>
                        </div>
                        <div class="input-group">
                            <textarea placeholder="اطلاعات تکمیلی و توضیحات" name="cooperation-txtarea" id="cooperation-txtarea" class="form-control mt-3"></textarea>
                        </div>
                        <input type="submit" class="btn btn-outline-primary mt-3 submit-cooperation float-right" value="ارسال">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
{{-- ! FOOTER ! --}}
@include('theme.components.footer')

@endsection

@section('scripts')
    <script src="{{ asset('js/app.js') }}"></script>
    <script>
    var mySwiper = new Swiper.default('#slider', {
        slidesPerView: 1,
        spaceBetween: 0,
        loop: true,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
        navigation: {
            nextEl: '#slider > .swiper-button-next',
            prevEl: '#slider > .swiper-button-prev',
        },
        autoplay: {
            delay: 5000,
            disableOnInteraction: false,
        }
    });
    var swiper = new Swiper.default('.bookslider', {
        slidesPerView: 4,
        spaceBetween: 10,
        breakpoints: {
            320: {
                slidesPerView: 1,
                spaceBetween: 20,
            },
            500: {
                slidesPerView: 2,
                spaceBetween: 20,
            },
            768: {
                slidesPerView: 5,
                spaceBetween: 20,
            },
            2560: {
                slidesPerView: 6,
                spaceBetween: 40
            }
        },
        navigation: {
            nextEl: '.bookslider > .swiper-button-next',
            prevEl: '.bookslider > .swiper-button-prev',
        },
    });
    var swiper2 = new Swiper.default('.podcastslider', {
        slidesPerView: 4,
        spaceBetween: 10,
        breakpoints: {
            320: {
                slidesPerView: 1,
                spaceBetween: 20,
            },
            500: {
                slidesPerView: 2,
                spaceBetween: 20,
            },
            768: {
                slidesPerView: 5,
                spaceBetween: 20,
            },
            2560: {
                slidesPerView: 6,
                spaceBetween: 40
            }
        },
        navigation: {
            nextEl: '.podcastslider > .swiper-button-next',
            prevEl: '.podcastslider > .swiper-button-prev',
        },
    });
    $('#cooperation').select2({placeholder: 'زمینه همکاری'});
    </script>
@endsection
