@extends('theme.page')
@section('header')
    <title>جستجو</title>
@endsection

@section('main-content')
<div class="col-12 section search-background">
    <div
        id="react-search-screen"
        data-keyword="{{ $keyword }}"
        search="{{ route('api.platform.search', ['keyword' => 'keyword']) }}"
        searchPro="{{ route('api.platform.search.pro', ['keyword' => 'keyword']) }}"></div>
    </div>
@endsection
