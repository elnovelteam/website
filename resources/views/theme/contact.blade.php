@extends('theme.page')
@section('header')
    <title>تماس با ما</title>
@endsection
@section('main-content')
<div class="col-12 mb-4 section svg-section pl-0 bg-white">
    <div class="col-md-6 col-12 p-3 pl-0 float-left">
        <img src="{{ asset('images/inc/contact-us.svg') }}" alt="">
    </div>
    <div class="col-md-6 col-12 p-3 float-left">
        <div class="title-row mb-3 float-left">
            <h1 class="title">ارتباط با Elnovel</h1>
            <span class="underline col-sm-4 offset-sm-4 col-8 offset-2 mt-3">
                <span class="circular-icon"><i class="fas fa-phone"></i></span>
            </span>
        </div>
        <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با استفاده از طراحان گرافیک است، چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است، و برای شرایط فعلی تکنولوژی مورد نیاز، و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد، کتابهای زیادی در شصت و سه درصد گذشته حال و آینده، شناخت فراوان جامعه و متخصصان را می طلبد، تا با نرم افزارها شناخت بیشتری را برای طراحان رایانه ای علی الخصوص طراحان خلاقی، و فرهنگ پیشرو در زبان فارسی ایجاد کرد، در این صورت می توان امید داشت که تمام و دشواری موجود در ارائه راهکارها، و شرایط سخت تایپ به پایان رسد و زمان مورد نیاز شامل حروفچینی دستاوردهای اصلی، و جوابگوی سوالات پیوسته اهل دنیای موجود طراحی اساسا مورد استفاده قرار گیرد.</p>
    </div>
</div>
<div class="col-12 mb-4 section">

</div>
@endsection