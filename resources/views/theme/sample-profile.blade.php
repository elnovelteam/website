@extends('theme.default')
@section('header')
    <title>{{ $user->name }}</title>
@endsection
@section('content')
    <h1 class="col-12 text-right float-right my-3">
        @if ($user->id !== auth()->user()->id)
            @if ($user->followers()->where(['user_id' => auth()->user()->id, 'followable_id' => $user->id])->first())
                <a class="btn-sm btn-danger" href="/unfollow/{{ $user->id }}">unfollow</a>
            @else
                <a class="btn-sm btn-primary" href="/follow/{{ $user->id }}">follow</a>
            @endif
        @endif
        {{ $user->name }}
    </h1>

    <h3 class="col-12 text-right float-right">دنبال شونده ها: </h1>
    <ol style="direction: rtl;" class="text-right rtl float-right p-3 col-12">
        @foreach ($user->followings as $following)
        <li class="text-right mb-3 mr-3">{{ $following->followable->name }}
        @if ($following->followable_id !== auth()->user()->id)
            @if ($following->FollowedByUser($following->followable_id, $following->followable) || $self)
                <a class="btn-sm btn-danger" href="/unfollow/{{ $following->followable->id }}">unfollow</a>
            @else
                <a class="btn-sm btn-primary" href="/follow/{{ $following->followable->id }}">follow</a>
            @endif
        @else
        @endif
        </li>
        @endforeach
    </ol>
    <h3 class="col-12 text-right float-right">دنبال کننده ها: </h1>
    <ol style="direction: rtl;" class="text-right rtl float-right p-3 col-12">
        @foreach ($user->followers as $follower)
        <li class="text-right mb-3 mr-3">{{ $follower->user->name }}
        @if ($follower->user->id !== auth()->user()->id)
            @if ($follower->FollowedByUser())
                <a class="btn-sm btn-danger" href="/unfollow/{{ $follower->user->id }}">unfollow</a>
            @else
                <a class="btn-sm btn-primary" href="/follow/{{ $follower->user->id }}">follow</a>
            @endif
        @endif
        </li>
        @endforeach
    </ol>
@endsection