@extends('theme.default')
@section('header')
    <title>Writers</title>
    <style>
        nav.menu {
            background: #543F40;
        }
        nav.menu ul li {
            padding: 6px 12px;
        }
        nav.menu .logo {
            background: rgba(255,255,255,.3);
            box-shadow: -2px -5px 7px 1px #222;
        }
    </style>
@endsection
@section('content')
{{-- HEADER --}}
<header>
    @include('theme.components.menu')
</header>
@php
    $p = 0;
@endphp
<div class="content margined">
    <div class="col-12 col-md-10 p-0 header-img-container">
        <img src="/images/woman-hand-is-writing-notebook-with-pen-office_35708-1089.jpg" alt="">
    </div>
    <div class="profile-section-container col-12 col-md-11">
        <div class="profile-photo-section">
            <div class="mr-2 mb-3">
                <div>
                    <h1>آوا سجادی</h1>
                    <i class="fas fa-feather-alt fa-2x feather"></i>
                </div>
                <div class="show-confirm">

                </div>
                <button type="button" class="btn btn-sm btn-primary">دنبال کردن <i class="mr-1 mt-1 fas fa-plus float-left"></i></button>
                <button type="button" class="btn btn-sm btn-danger">دنبال می کنید<i class="mr-1 mt-1 fas fa-times float-left"></i></button>
            </div>
            <img src="{{ asset($writer->photo->file->path) }}" alt="">
        </div>
    </div>
    <div class="writer-statistics col-12 col-sm-10 offset-sm-1 col-md-8 float-left offset-md-2 mt-4">
        <div class="col-6 col-md-4 col-lg-3 mt-3">
            <div>
                <span>8</span>
                <p class="d-block d-sm-inline">کتاب</p>
            </div>
            <i class="fas fa-book fa-2x ml-2"></i>
        </div>
        <div class="col-6 col-md-4 col-lg-3 mt-3">
            <div>
                <span>200</span>
                <p class="d-block d-sm-inline">دنبال کننده</p>
            </div>
            <i class="fas fa-user-friends fa-2x ml-2"></i>
        </div>
        <div class="col-6 col-md-4 col-lg-3 mt-3">
            <div>
                <span>400</span>
                <p class="d-block d-sm-inline">دنبال شونده</p>
            </div>
            <i class="fas fa-users fa-2x ml-2"></i>
        </div>
        <div class="col-6 col-md-4 col-lg-3 mt-3">
            <div>
                <span>2 سال و 4 ماه</span>
                <p class="d-block d-sm-inline">مدت عضویت</p>
            </div>
            <i class="fas fa-hourglass-half fa-2x ml-2"></i>
        </div>
        <div class="col-6 col-md-4 col-lg-3 mt-3">
            <div>
                <span>لیسانس</span>
                <p class="d-block d-sm-inline">تحصیلات</p>
            </div>
            <i class="fas fa-graduation-cap fa-2x ml-2"></i>
        </div>
        <div class="col-6 col-md-4 col-lg-3 mt-3">
            <div>
                <span>2498</span>
                <p class="d-block d-sm-inline">تعداد بازدید</p>
            </div>
            <i class="fas fa-eye fa-2x ml-2"></i>
        </div>
    </div>
    <div class="writer-info-container col-12 col-md-10 offset-md-1 mt-4">
        <div class="bio-container col-12 col-md-6 pt-5">
            <p><i class="fas fa-feather text-info"></i> معرفی : </p>
            {{-- <p class="mr-3">اینجانب یونس طهرانیم متخصص داستان نویسی پست مدرن و دارای مدرک دادن از دانشگاه کونیا هستم هر روز کون میدم و از این بابت بسیار خوشحالم یروز کیرمو کردم تو سوراخ فن کامپیوترم خیلی کوچولو شد میخام تغییر جنسیت بدم و تا ابد چوچول بزارم کیر خر کضصک کص کص مدرک کص کنیم دارم تازه جنده خانومای کیریه خارکصته کص کله کونده ننه ی مکتلشتملسبل دیگه میخام کسشعر بزارم کیر تو لورم اپیزوم</p> --}}
            <p class="mr-3">اینجانب آوا سجادی دارای مدرک فوق لیسانس در آنال سکس و دابل پنیتریشن از دوسلدورف آلمان هستم. تا حالا به پنج شیش تا از شاگردام دادم و حتی تجربه برده جنسی شدن در شهر کلن آلمان رو هم دارا هستم. پدربنده از کص بازهای معروف مشهد هستند و تا حالا همه خاله های من و از جمله خود من رو با دسته مبل گاییدن. از افتخارات بزرگ من دادن به مدیر موسسه حافظ و نشون دادن ممه هایم به آقای یونس طهرانی هست.</p>
        </div>
        <div class="guide-section col-12 col-md-2">
            <div>
                <p>نظر کاربران</p>
            </div>
            <div>
                <p>امتیاز الناول</p>
            </div>
        </div>
        <div class="circular-svg col-12 col-md-4">
             {{-- <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 600 300" width="400" height="200">
                <linearGradient id="lg" x1="0.5" y1="1" x2="0.5" y2="0">
                    <stop offset="0%" stop-opacity="1" stop-color="royalblue"/>
                    <stop offset="40%" stop-opacity="1" stop-color="royalblue"/>
                    <stop offset="40%" stop-opacity="0" stop-color="royalblue"/>
                    <stop offset="100%" stop-opacity="0" stop-color="royalblue"/>
                </linearGradient>
                <circle cx="250" cy="150" r="145" fill="url(#lg)" stroke="crimson" stroke-width="5"/>
            </svg> --}}
            <svg viewBox="0 0 50 50" stroke="#000" stroke-offset="1">
                <path
                  d="M18 2.0845
                    a 15.9155 15.9155 0 0 1 0 31.831
                    a 15.9155 15.9155 0 0 1 0 -31.831"
                  fill="transparent"
                  stroke-dasharray="100, 100"
                  stroke-dashoffset="0"
                  stroke="#999"
                  stroke-width="1"
                  {{-- class="progress-bar" --}}
                />
                <path
                  d="M18 2.0845
                    a 15.9155 15.9155 0 0 1 0 31.831
                    a 15.9155 15.9155 0 0 1 0 -31.831"
                  fill="none"
                  stroke-dasharray="{{ $p }}, 100"
                  class="progress-bar"
                />

            </svg>
            <div class="elnovel-rate">
                <div class="img">
                    <img src="{{ asset('images/inc/ink.png') }}" alt="">
                    <div class="colored" style="height: {{ $p / 2.5 }}%;margin-top: -{{ $p / 2.5 }}%;"></div>
                    <span class="{{ ($p >= 50) ? 'text-light' : 'text-dark mb-1' }}">{{ $p }}%</span>
                </div>
            </div>
        </div>
    </div>
    {{-- <hr class="col-10 col-md-8 offset-md-2"> --}}
    <div id="react-writer-info" class="col-12"></div>
</div>
{{-- FOOTER --}}
{{-- @include('theme.components.footer') --}}
@endsection
@section('scripts')
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/writer-profile.js') }}"></script>
@endsection