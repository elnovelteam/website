<?php

namespace ElAdmin\UA\Facades;

use Illuminate\Support\Facades\Facade;


class ElUA extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'ElUA';
    }
}

