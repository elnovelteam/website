<?php

namespace ElAdmin\UA\Http\Controllers;

use App\BackupSection;
use App\Events\TicketCreated;
use App\Notifications\TicketCreatedNotification;
use App\Ticket;
use App\TicketMessage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Lang;
use NotificationChannels\Telegram\TelegramChannel;

class TicketController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tickets = auth()->user()->tickets()->with('section')->orderBy('updated_at', 'desc')->paginate(10);
        return view('UAV::pages.tickets.index', compact('tickets'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $sections = BackupSection::all();
        return view('UAV::pages.tickets.create', compact('sections'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
           'question' => 'required|string|min:3|max:750',
           'title' => 'required|string|max:65',
           'priority' => 'required|integer',
           'section' => 'required|integer'
        ]);
        $ticket = new Ticket;
        $ticket->title = $request->title;
        $ticket->question = $request->question;
        $ticket->force_rate = (int) $request->priority;
        $ticket->status = 'OPEN';
        $ticket->code = time();
        $ticket->backup_section_id = (int) $request->section;
        $res = auth()->user()->tickets()->create($ticket->toArray());
        event(new TicketCreated(auth()->user(), $res));
        flash()->success('موفقیت!','درخواست پشتیبانی ثبت شد');
        return redirect()->to(route('userarea.tickets.show', [$res->id]));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Ticket $ticket)
    {
        if (Gate::denies('view', $ticket)) {
            abort(403, Lang::get('messages.errors.403'));
        }
        TicketMessage::where(['ticket_id' => $ticket->id, 'type' => 'answer', 'read_at' => null])->update(['read_at' => now()]);
        $ticket->load('section', 'messages.user');
        $messages = $ticket->messages;
        // $messages = $ticket->messages()->orderBy('created_at', 'asc')->with('user')->get();
        return view('UAV::pages.tickets.show', compact('ticket', 'messages'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Ticket $ticket)
    {
        $request->validate([
           'message' => 'required|string|min:3|max:750'
        ]);
        $message = new TicketMessage;
        $message->text = $request->message;
        $message->user_id = $ticket->user_id;
        $ticket->messages()->create($message->toArray());
        $ticket->updated_at = now();
        $ticket->save();
        return back();
    }
}
