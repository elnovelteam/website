<?php

namespace ElAdmin\UA\Http\Controllers;

use Illuminate\Auth\Access\Response;
use Illuminate\Http\Request;
use Illuminate\Http\Response as HttpResponse;

class DashboardController extends BaseController
{
    public function index()
    {
        $user = auth()->user();
        $user->load('writer');
        return view('UAV::dashboard', compact('user'));
    }
    public function settings()
    {
        $user = auth()->user();
        // return $user;
        return view('UAV::setting', compact('user'));
    }
    public function updateProfile(Request $request)
    {
        $request->validate([
           'username' => 'required|unique:users,name|string',
           'email' => 'required|email',
           'phone_number' => 'required|integer',
           'sex' => 'required|boolean'
        ]);
        $user = auth()->user();
        $user->name = $request->username;
        $user->email = $request->email;
        $user->phone_number = $request->phone_number;
        $user->sex = ($request->sex) ? 'male' : 'female';
        $user->save();
        return back();
        flash()->success('موفقیت','ذخیره شد !');
    }
}