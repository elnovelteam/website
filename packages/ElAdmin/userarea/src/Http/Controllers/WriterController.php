<?php

namespace ElAdmin\UA\Http\Controllers;

use App\Definition;
use App\Writer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Lang;

class WriterController extends BaseController
{
    public function show()
    {

    }
    public function create()
    {
        $user = auth()->user();
        $user->load('writer');
        if ($user->writer) {
            return redirect()->to(route('userarea.writers.edit', ['writer' => $user->name]));
        }
        $educations = Definition::whereType('education')->get();
        $expriences = Definition::whereType('exprience')->get();
        $interests = Definition::whereType('interest')->get();
        return view("UAV::pages.writers.create", compact('user', 'educations', 'expriences', 'interests'));
    }
    public function edit($writer)
    {
        $user = auth()->user()->load('writer');
        if (Gate::allows('view', $user->writer) && $writer == $user->name) {
            $writer     = $user->writer;
            $educations = Definition::whereType('education')->get();
            $expriences = Definition::whereType('exprience')->get();
            $interests  = Definition::whereType('interest')->get();
            return view("UAV::pages.writers.edit", compact('educations', 'expriences', 'interests', 'writer'));
        }
        abort(403);
    }
    public function store(Request $request)
    {
        $request->validate([
            'firstname' => 'required|min:2|max:22',
            'lastname'  => 'required|min:2|max:22',
            'nickname'  => 'required|min:3|max:40',
            'bio'       => 'max:300',
            'education' => 'required',
            'exprience' => 'required',
            'interest'  => 'required'
        ]);
        $user = auth()->user();
        $user->load('writer');
        if ($user->writer) {
            return back();
        }
        $writer = new Writer;
        $writer->first_name = $request->firstname;
        $writer->last_name  = $request->lastname;
        $writer->nickname   = $request->nickname;
        $writer->bio        = $request->bio ?: '';
        $writer->active     = false;
        $writer->education_id = $request->education;
        $writer->exprience_id = $request->exprience;
        $writer->interest_id  = $request->interest;
        $writer = auth()->user()->writer()->create($writer->toArray());
        flash()->overlay('موفقیت', 'فرم درخواست با موفقیت ثبت شد', 'success');
        return redirect()->to(route('userarea.writers.edit', ['writer' => $user->name]));
    }
    public function update(Request $request, $writer)
    {
        $writer = Writer::find($writer)->firstOrFail();
        $request->validate([
            'firstname' => 'required|min:2|max:22',
            'lastname'  => 'required|min:2|max:22',
            'nickname'  => 'required|min:3|max:40',
            'bio'       => 'max:300',
            'education' => 'required',
            'exprience' => 'required',
            'interest'  => 'required'
        ]);
        $writer->first_name = $request->firstname;
        $writer->last_name  = $request->lastname;
        $writer->nickname   = $request->nickname;
        $writer->bio        = $request->bio ?: '';
        $writer->education_id = $request->education;
        $writer->exprience_id = $request->exprience;
        $writer->interest_id  = $request->interest;
        $writer->save();
        flash()->success('موفقیت', 'فرم درخواست با موفقیت ویرایش شد');
        return redirect()->to(route('userarea.writers.edit', ['writer' => auth()->user()->name]));
    }
}
