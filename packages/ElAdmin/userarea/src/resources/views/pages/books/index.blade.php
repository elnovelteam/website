@extends('UAV::layouts.default')

@section('header')
<title>کتاب ها</title>
@endsection

@section('content')
    <h1 class="title mt-3 mb-3">کتاب ها</h1>
    <div class="p-3 analysis-container books-container">
        <div class="float-left col-md-4 col-12 series">
            <i class="float-right fas fa-th-large fa-3x"></i>
            <div class="float-right">
                <span class="float-right">2</span>
                <span class="float-right">سری ها</span>
            </div>
            <a href="#"><i class="float-left fas fa-plus-square fa-3x plus-icon"></i></a>
        </div>
        <div class="float-right col-md-4 col-12 books">
            <i class="float-right fas fa-book fa-3x"></i>
            <div class="float-right">
                <span class="float-right">8</span>
                <span class="float-right">کتاب ها</span>
            </div>
            <a href="{{ route('userarea.books.create') }}"><i class="float-left fas fa-plus fa-3x plus-icon"></i></a>
        </div>
    </div>
@endsection