@extends('UAV::layouts.default')

@section('header')
<title>درخواست های پشتیبانی</title>
@endsection

@section('content')
    <h1 class="title mt-3 mb-3"><a class="btn btn-sm btn-primary ml-3" href="{{ route('userarea.tickets.create') }}"><i class="mt-1 fas fa-1x fa-plus"></i></a>درخواست های پشتیبانی</h1>
    <table class="table table-bordered text-center rtl">
        <thead class="thead-dark">
            <tr>
            <th scope="col">#</th>
            <th scope="col">شماره تیکت</th>
            <th scope="col">اولویت</th>
            <th scope="col">بخش مربوطه</th>
            <th scope="col">وضعیت</th>
            <th scope="col">آخرین بروزرسانی</th>
            <th scope="col">مشاهده</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($tickets as $ticket)
            <tr title="{{ $ticket->question }}">
            <th scope="row">{{ $loop->index + 1 }}</th>
            <td>{{ $ticket->code }}</td>
            <td>@lang('UAL::userarea.ticket.' . $ticket->force_rate)</td>
            <td>{{ $ticket->section->name }}</td>
            <td>@lang('UAL::userarea.ticket.' . $ticket->status)</td>
            <td>{{ $ticket->updated_at->diffForHumans() }}</td>
            <td>
                <a class="btn btn-sm btn-outline-dark" href="{{ route('userarea.tickets.show', ['ticket' => $ticket->id]) }}">
                <i class="fas fa-eye"></i>
                </a>
            </td>
            </tr>
            @endforeach
        </tbody>
    </table>
@endsection