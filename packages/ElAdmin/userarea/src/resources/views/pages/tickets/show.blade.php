@extends('UAV::layouts.default')

@php $user = auth()->user(); $user->load('photo'); @endphp

@section('header')
<title>درخواست پشتیبانی شماره #{{ $ticket->code }}</title>
@endsection

@section('content')
    <h4 class="title">تیکت شماره #{{ $ticket->code   }}</h4>
    <div class="col-12 p-0 float-left ticket-area">
        <div class="messages col-12 px-3 py-3 float-left">
            <div class="message question">
                <img class="photo" src="@if($user->photo) {{ asset('images/profile-test.jpg') }} @else {{ asset('images/inc/' . $user->sex . '-avatar.svg') }} @endif" alt="">
                <p class="text">{{  $ticket->question  }}<span class="time"><i class="far fa-clock"></i>{{ $ticket->created_at->format('H:i') }}</span></p>
            </div>
            @foreach ($messages as $message)
                <div class="message {{ $message->type }}">
                @if ($message->type == 'question')
                <img title="{{ $user->name }}" class="photo" src="@if($user->photo) {{ asset('images/profile-test.jpg') }} @else {{ asset('images/inc/' . $user->sex . '-avatar.svg') }} @endif" alt="">
                @else
                <img title="{{ $message->user->name }}" class="photo" src="@if($message->user->photo) {{ asset('images/profile-test.jpg') }} @else {{ asset('images/inc/' . $message->user->sex . '-avatar.svg') }} @endif" alt="">
                @endif
                <p class="text">{{  $message->text  }}<span class="time"><i class="far fa-clock"></i>{{ $message->created_at->format('H:i') }}</span></p>
                </div>
            @endforeach
        </div>
        <form action="{{ route('userarea.tickets.update', ['ticket' => $ticket->id]) }}" method="POST" class="messaging-area col-12 p-0 float-left">
            @csrf
            @method('PUT')
            <div class="col-12 h-12 p-0 input-group">
                <textarea required name="message" id="message" placeholder="@lang('UAL::userarea.form.message')"
                class="form-control text-right rtl" cols="30" rows="4">{{ old('message') }}</textarea>
                <div class="input-group-prepend float-right">
                    <button class="btn px-4 btn-dark"><i class="fas fa-paper-plane"></i></button>
                </div>
            </div>
        </form>
    </div>
@endsection