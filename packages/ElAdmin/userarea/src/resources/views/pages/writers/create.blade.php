@extends('UAV::layouts.default')

@section('header')
<title>درخواست نویسندگی</title>
@endsection

@section('content')
<h3 class="title request-form-title mt-4 mb-4">درخواست نویسندگی<i class="float-right ml-3 fas fa-pen-fancy fa-1x"></i></h3>
<div class="col-12 float-left">
<form class="col-12 float-right" action="{{ route('userarea.writers.store') }}" method="post">
    @csrf
    <div class="col-lg-5 col-md-6 col-12 input-group mb-3">
        <input type="text" name="firstname" id="title" value="{{ old('firstname') }}" class="form-control text-right rtl" placeholder="@lang('UAL::userarea.form.firstname')">
        <div class="input-group-append float-right">
            <span class="input-group-text"><i class="far fa-user"></i></span>
        </div>
    </div>
    <div class="col-lg-5 col-md-6 col-12 input-group mb-3">
        <input type="text" name="lastname" id="lastname" value="{{ old('lastname') }}" class="form-control text-right rtl" placeholder="@lang('UAL::userarea.form.lastname')">
        <div class="input-group-append float-right">
            <span class="input-group-text"><i class="far fa-user"></i></span>
        </div>
    </div>
    <div class="col-lg-5 col-md-6 col-12 input-group mb-3">
        <input type="text" name="nickname" id="nickname" value="{{ old('nickname') }}" class="form-control text-right rtl" placeholder="@lang('UAL::userarea.form.nickname')">
        <div class="input-group-append float-right">
            <span class="input-group-text"><i class="fas fa-user-edit"></i></span>
        </div>
    </div>
    <div class="col-lg-5 col-md-6 col-12 input-group mb-3 lilselect">
        <select class="form-control" name="education" id="education" data-placeholder="میزان تحصیلات">
            <option></option>
            @foreach ($educations as $education)
                <option class="text-right rtl" value="{{ $education->id }}">{{ $education->title }} ({{ $education->description }})</option>
            @endforeach
        </select>
        <div class="input-group-append float-right">
            <span class="input-group-text"><i class="fas fa-university"></i></span>
        </div>
    </div>
    <div class="col-lg-5 col-md-6 col-12 input-group mb-3 lilselect">
        <select class="form-control" name="exprience" id="exprience" data-placeholder="تجربه داستان نویسی">
            <option></option>
            @foreach ($expriences as $exprience)
                <option class="text-right rtl" value="{{ $exprience->id }}">{{ $exprience->title }} ({{ $exprience->description }})</option>
            @endforeach
        </select>
        <div class="input-group-append float-right">
            <span class="input-group-text"><i class="fas fa-pen-fancy"></i></span>
        </div>
    </div>
    <div class="col-lg-5 col-md-6 col-12 input-group mb-3 lilselect">
        <select class="form-control" name="interest" id="interest" data-placeholder="علاقه به نوشتن">
            <option></option>
            @foreach ($interests as $interest)
                <option class="text-right rtl" value="{{ $interest->id }}">{{ $interest->title }} ({{ $interest->description }})</option>
            @endforeach
        </select>
        <div class="input-group-append float-right">
            <span class="input-group-text"><i class="fas fa-heart"></i></span>
        </div>
    </div>
    <div class="col-md-8 col-12 input-group mb-3">
        <textarea type="text" name="bio" id="bio" class="form-control text-right rtl" rows="6" placeholder="@lang('UAL::userarea.form.bio')">{{ old('bio') }}</textarea>
    </div>
    <div class="col-lg-8 col-12 float-right">
        <button type="submit" class="btn btn-secondary float-left">ثبت</button>
    </div>
</form>
</div>
@endsection

@section('script')
<script>
$('#education,#exprience,#interest').each(function(){
    $(this).select2({placeholder: $(this).attr('data-placeholder')});
});
var AuthenticatedEmail = "{{ $user->email }}";
</script>
<script src="{{ asset('js/request-form.js') }}"></script>
@endsection