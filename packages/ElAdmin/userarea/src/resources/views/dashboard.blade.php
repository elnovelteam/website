@extends('UAV::layouts.default')

@section('content')
    <div class="p-3 analysis-boxes analysis-container">
        <div class="float-left col-md-4 col-12 newCustomers">
            <i class="float-right fas fa-users fa-3x"></i>
            <div>
                <span class="float-right">146</span>
                <span class="float-right">بازدید کننده های جدید</span>
            </div>
        </div>
        <div class="float-left col-md-4 col-12 visits">
            <i class="float-right fas fa-globe fa-3x"></i>
            <div>
                <span class="float-right">346</span>
                <span>بازدید های امروز</span>
            </div>
        </div>
        <div class="float-left col-md-4 col-12 tickets">
            <i class="float-right fas fa-user-tag fa-3x"></i>
            <div>
                <span class="float-right">59</span>
                <span>درخواست های پشتیبانی</span>
            </div>
        </div>
    </div>
@endsection