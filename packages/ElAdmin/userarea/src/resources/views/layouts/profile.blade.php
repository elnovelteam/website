<div class="col-12 float-left p-3 profile-info">
    <div class="col-12 py-2 profile-img">
        <img src="@if($user->writer) {{ asset('images/profile-test.jpg') }}  @else {{ asset('images/inc/' . $user->sex . '-avatar.svg') }}  @endif" alt="">
    </div>
    <p>{{ $user->name }}</p>
    <p>{{ $user->email }}</p>
</div>
<div class="col-12 float-left p-0 profile-menu">
    <ul class="col-12 m-0">
        <li><a href="{{ route('userarea.') }}">داشبورد<i class="fa fa-tachometer-alt"></i></a></li>
        @if ($user->hasRole('RDR'))
        <li><a href="#">نظرات<i class="far fa-comments"></i></a></li>
        <li><a href="#">بوکمارک ها<i class="far fa-bookmark"></i></a></li>
        <li><a href="{{ route('userarea.settings.index') }}">تنظیمات<i class="fas fa-cog"></i></a></li>
            @if ($user->hasRole('WRT'))
            <li><a href="{{ route('userarea.books.index') }}">کتابها<i class="fas fa-book-open"></i></a></li>
            @else
            <li><a href="{{ route('userarea.writers.request') }}">درخواست نویسندگی<i class="fas fa-cog"></i></a></li>
            @endif
        @else
        <li><a href="{{ route('userarea.settings.index') }}">تکمیل پروفایل<i class="fas fa-user"></i></a></li>
        @endif
        <li><a href="{{ route('userarea.tickets.index') }}">درخواست های پشتیبانی<i class="fas fa-info-circle"></i></a></li>
        <li><a id="logout" href="#logout">خروج<i class="fas fa-power-off"></i></a></li>
    </ul>
</div>