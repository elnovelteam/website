@php $user = auth()->user(); @endphp
<!DOCTYPE html>
<html lang="{{ App::getLocale() }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    @yield('header')
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link rel="stylesheet" href="{{ asset('css/userarea.css') }}">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body>
    <div class="userarea col-12 p-0 m-0 full-sc">
        <div id="sidebar" class="col-md-2 col-12 p-0 float-right sidebar active animated fadeInRight">
            @include('UAV::layouts.profile')
        </div>
        <div class="col-md-10 col-12 p-0 contentbar">
            <div class="col-12 top-bar px-2">
                <button id="menu-toggle" class="float-right btn btn-sm btn-secondary mt-2 pt-2"><i class="fas fa-1x fa-angle-double-right"></i></button>
            </div>
            <div class="col-12 main p-3">
                @yield('content')
            </div>
        </div>
        {{-- Logout Form --}}
        <form action="{{ route('logout') }}" id="logout-form" method="post">@csrf</form>
    </div>
    <script src="{{ asset('js/app.js') }}"></script>
    @include('partials.flash')
    @include('partials.error')
    @yield('script')
    <script>$('#menu-toggle').click(function(){
        $('#sidebar').toggleClass('active');
        $(' > i', this).toggleClass('fa-angle-double-right fa-angle-double-left');
        $('.contentbar').toggleClass('col-md-10');
    });
     $('.DelayClose').each(function(){
        $(this).delay(Number($(this).attr('ResTime'))).slideUp(300);
    });
    $('#logout').click(() => $("#logout-form").submit())</script>
</body>
</html>