@extends('UAV::layouts.default')

@section('header')
<title>@if($user->hasRole('RDR')) تنظیمات @else تکمیل پروفایل @endif</title>
@endsection

@section('content')
    <h1 class="title">@lang('UAL::userarea.settings')</h1>
    <div class="col-12 float-left">
        <form class="col-12 float-right" action="{{ route('userarea.settings.update') }}" method="post">
            <div class="col-lg-5 col-md-6 col-12 input-group mb-3">
                <input type="text" name="username" id="username" class="form-control" value="{{ old('username') ?: $user->name }}"
                placeholder="@lang('UAL::userarea.form.username')">
                <div class="input-group-append float-right">
                    <span class="input-group-text"><i class="fas fa-user"></i></span>
                </div>
            </div>
            <div class="col-lg-5 col-md-6 col-12 input-group mb-3">
                <input type="text" name="email" id="email" class="form-control" value="{{ old('email') ?: $user->email }}"
                placeholder="@lang('UAL::userarea.form.email')">
                <div class="input-group-append float-right">
                    <span class="input-group-text"><i class="far fa-envelope"></i></span>
                </div>
            </div>
            <div class="col-lg-5 col-md-6 col-12 input-group mb-3">
                <input type="text" name="phone_number" id="phone_number" class="form-control" value="{{ old('phone_number') ?: $user->phone_number }}"
                placeholder="@lang('UAL::userarea.form.phone_number')">
                <div class="input-group-append float-right">
                    <span class="input-group-text"><i class="fas fa-mobile-alt"></i></span>
                </div>
            </div>
            <div class="col-lg-5 col-md-6 col-12 input-group mb-3 rtl">
                <p class="float-right">جنسیت : </p>
                <div class="sex-checkbox col-md-6 col-9 float-right">
                    <i class="fas fa-2x fa-male float-right ml-3"></i>
                    <label class="checkbox-c float-right" for="sex">
                        <input type="checkbox" name="sex" id="sex" value="true" @if($user->sex == 'male') checked @endif>
                        <span class="check-handle"></span>
                    </label>
                    <i class="fas fa-2x fa-female mr-3"></i>
                </div>
            </div>
            <div class="col-12 rtl input-group">
                <button type="submit" class="btn btn-primary float-right">ذخیره</button>
            </div>
        </form>
    </div>
@endsection