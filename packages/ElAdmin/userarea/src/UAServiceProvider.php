<?php

namespace ElAdmin\UA;

use Illuminate\Support\ServiceProvider;

class UAServiceProvider extends ServiceProvider
{
    protected $mainpath = __DIR__ . DIRECTORY_SEPARATOR;
    public function register()
    {
        $this->app->bind('UA', function(){
            return new UA;
        });
        $this->mergeConfigFrom($this->mainpath . "Config/main.php",'cmsv');

    }
    public function boot()
    {
        // require __DIR__ . '/Http/routes.php';
        $this->loadTranslationsFrom($this->mainpath . 'resources/lang', 'UAL');
        $this->loadViewsFrom($this->mainpath . "resources/views", 'UAV');
    }
}
