<?php

namespace ElAdmin\Cms\Http\Controllers;

use App\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class TagsController extends BaseController
{
    public function index()
    {
        $tags = Tag::orderBy('created_at','desc')->paginate(10);
        return view('CMSV::Pages.Tags.index',compact('tags'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		return view('CMSV::Pages.Tags.create');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
            'name.required' => 'فیلد نام اجباری است',
            'name.unique' => 'این نام تکراری است',
            'name.min' => 'حداقل سه کاراکتر برای فیلد نام پر کنید',
            'name.max' => 'حداکثر تعداد مجاز این فیلد 60 کاراکتر است'
        ];
        $rules = [
            'name' => 'required|min:3|max:60|unique:tags',
        ];
        $this->validate($request,$rules,$messages);
        $tag = new Tag;
        $tag->name = str_replace(" ","_",$request->name);
        $latestName = Tag::whereRaw("`id` != '{$tag->id}' and `name` RLIKE '^{$tag->name}(-[0-9]*)?$'")
        ->orderBy('id')->pluck('name');
        if (isset($latestName[0])) {
            $pieces = explode('-',$latestName);
            $number = intval(end($pieces));
            $tag->name .= ($number - 1);
        }
        $tag->save();
        flash()->success('!انجام شد','برچسب با موفقیت ساخته شد');
        return redirect()->to(route('Admin.tags.index'));
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($category)
    {
        return Tag::findOrFail($category);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($tag)
    {
        $tag = Tag::findOrFail($tag);
        $types = ['product','page','post'];
        return view('CMSV::Pages.Tags.edit', compact('tag','types'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $tag)
    {
        $messages = [
            'name.required' => 'فیلد نام اجباری است',
            'name.min' => 'حداقل سه کاراکتر برای فیلد نام پر کنید',
            'name.max' => 'حداکثر تعداد مجاز این فیلد 60 کاراکتر است'
        ];
        $rules = [
            'name' => 'required|min:3|max:60',
        ];
        $this->validate($request,$rules,$messages);
        if (Tag::whereName(str_replace(" ","-",$request->name))->first()) {
            return back();
        }
        $tag = Tag::findOrFail($tag);
        $tag->name = str_replace(" ","_",$request->name);
        $latestName = Tag::whereRaw("`id` != '{$tag->id}' and `name` RLIKE '^{$tag->name}(-[0-9]*)?$'")
        ->orderBy('id')->pluck('name');
        if (isset($latestName[0])) {
            $pieces = explode('-',$latestName);
            $number = intval(end($pieces));
            $tag->name .= ($number - 1);
        }
        $tag->save();
        flash()->success('!انجام شد','برچسب با موفقیت بروزرسانی شد');
        return back();
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($tag)
    {
        Tag::findOrFail($tag)->delete();
        return back();
    }
}
