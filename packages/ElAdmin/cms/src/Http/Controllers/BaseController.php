<?php

namespace ElAdmin\Cms\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Keyword;
use App\Menu;
use App\Tag;

class BaseController extends Controller
{
    public function getAllTags() {
        return Tag::all();
    }
    public function getAllMenus() {
        return Menu::all();
    }
    public function getAllKeywords() {
        return Keyword::all();
    }
    public function configSEO($target, $request) {
        $target->load('seo');
        if ($target->seo) {
            $target->seo->update(['description' => $request->seo_description, 'robots' => $request->seo_robots, 'priority' => $request->seo_priority, 'changefreq' => $request->seo_change ]);
            $target->seo->keywords()->sync($request->seo_keywords);
        } else {
            $seo = $target->seo()->create(['description' => $request->seo_description, 'robots' => $request->seo_robots, 'priority' => $request->seo_priority, 'changefreq' => $request->seo_change ]);
            $seo->keywords()->sync($request->seo_keywords);
        }
    }
    public function check_slug($model, $column, $value, $id = false) {
        if ($id) {
            $wr = "`id` != $id AND ";
        }
        if ($model == 'post') {
            $wr .= "`$column` RLIKE '^{$value}(-[0-9]*)?$'";
            return \App\Post::whereRaw($wr)->orderBy('id')->pluck($column);
        }
    }
    
}
