<?php

namespace ElAdmin\Cms\Http\Controllers;

use App\Events\TicketClosed;
use App\Events\TicketUpdated;
use App\Notifications\TicketUpdatedNotification;
use App\Ticket;
use App\TicketMessage;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class TicketController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tickets = Ticket::where('status', '!=', 'CLOSED')->with(['section', 'user.roles'])->orderBy('updated_at', 'desc')->paginate(20);
        return view('CMSV::Pages.Tickets.index', compact('tickets'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Ticket $ticket)
    {
        TicketMessage::where(['ticket_id' => $ticket->id, 'type' => 'question', 'read_at' => null])->update(['read_at' => now()]);
        $ticket->load(['section', 'user']);
        $messages = $ticket->messages()->orderBy('created_at', 'asc')->get();
        return view('CMSV::Pages.Tickets.show', compact('ticket', 'messages'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Ticket $ticket)
    {
        $request->validate([
           'message' => 'required|string|min:3|max:750'
        ]);
        $message = new TicketMessage;
        $message->text = $request->message;
        $message->user_id = auth()->user()->id;
        $message->type = 'answer';
        $message = $ticket->messages()->create($message->toArray());
        $ticket->updated_at = now();
        $ticket->save();
        $ticket->load('user');
        event(new TicketUpdated($ticket->user, $ticket, $message));
        return back();
    }

    /**
     * Close the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function change(Request $request, Ticket $ticket, $status)
    {
        if ($status == $ticket->status) {
            $ticket->status = 'OPEN';
        } else {
            $ticket->status = $status;
        }
        $ticket->save();
        $ticket->load('user');
        if ($ticket->status == 'CLOSED') {
            event(new TicketClosed($ticket->user, $ticket));
        }
        return back();
    }
}
