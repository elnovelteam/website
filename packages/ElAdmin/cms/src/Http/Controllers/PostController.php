<?php

namespace ElAdmin\Cms\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Tag;
use App\Menu;
use App\Keyword;
use App\Seodata;

class PostController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::paginate(12);
        return view('CMSV::Pages.Posts.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tags     = Tag::all();
		$menus    = Menu::all();
        $robots   = ['index, follow','index, nofollow','noindex, follow','noindex, nofollow'];
        $keywords = Keyword::all();
        return view('CMSV::Pages.Posts.create',compact('tags', 'menus', 'robots', 'keywords'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
            'title.required' => 'فیلد نام اجباری است',
            'title.min' => 'حداقل سه کاراکتر برای فیلد نام پر کنید',
            'title.max' => 'حداکثر تعداد مجاز این فیلد 60 کاراکتر است'
        ];
        $rules = [
            'title' => 'required|min:3|max:60',
        ];
        $this->validate($request,$rules,$messages);
        $post = new Post;
        $post->title = $request->title;
        $post->slug  = make_slug($request->title);
        $post->lang  = $request->lang;
        $post->body  = $request->body;
        $post->menu_id = $request->menu;
        $latestName = $this->check_slug('post', 'title', $post->title);
        if (isset($latestName[0])) {
            $pieces = explode('-',$latestName);
            $number = intval(end($pieces));
            $post->slug .= ($number - 1);
        }
        $post->save();
        $post->tags()->sync($request->input('tags'));
        $this->configSEO($post, $request);
        // $post->seodata()->create([
        //     'robots' => $request->robots,
        //     'description' => $request->description
        // ]);
        // $seodata = Seodata::findOrFail($post->seodata->id);
        // $seodata->keywords()->sync($request->input('keywords'));
        flash()->success('!انجام شد','نوشته با موفقیت منتشر شد');
        return redirect()->to(route('Admin.posts.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        $tags  = $this->getAllTags();
        $menus = $this->getAllMenus();
        $keywords = $this->getAllKeywords();
        $robots = robots();
        return view('CMSV::Pages.Posts.edit', compact('post', 'tags', 'menus', 'keywords', 'robots'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        $post->title = $request->title;
        $post->slug = make_slug($request->title);
        $post->summary = $request->summary;
        $latestName = $this->check_slug('post', 'title', $post->title, $post->id);
        if (isset($latestName[0])) {
            $pieces = explode('-',$latestName);
            $number = intval(end($pieces));
            $post->slug .= ($number - 1);
        }
        $post->save();
        $post->tags()->sync($request->tags);
        $this->configSEO($post, $request);
        flash()->success("موفقیت !", "پست آپدیت شد");
        return redirect()->to(route('Admin.posts.edit', ['post' => $post->id]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Post::findOrfail($id)->delete();
        return back();
    }
}
