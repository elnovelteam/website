<?php

namespace ElAdmin\Cms\Http\Controllers;

use App\Order;

class TrashController extends BaseController
{
    public function customer_orders()
    {
        $orders = Order::onlyTrashed()->has('user.customer')->orderBy('deleted_at', 'DESC')->paginate(12);
        return view('CMSV::Pages.Orders.trash', compact('orders'));
    }
    public function return_orders($id)
    {
        $order = Order::onlyTrashed()->findOrFail($id);
        $order->deleted_at = null;
        $order->status = 'returned';
        $order->save();
        return back();
    }
}
