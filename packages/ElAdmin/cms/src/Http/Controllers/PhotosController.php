<?php

namespace ElAdmin\Cms\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ChangeProductRequest;
use App\Photo;

// use App\Http\Controllers\Traits\AuthorizesUsers;
// should be outcommand with trait way
class PhotosController extends BaseController
{
    // use AuthorizesUsers; should be outcommand with trait way
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($target,Request $request){
        if ($target) {
            $photo = Photo::fromFile($request->file('photo'))->Upload();
            return $target->addPhoto($photo);
        }
        return false;
    }
    public function destroy($id)
    {
        Photo::findOrfail($id)->delete();
        return back();
    }
}
