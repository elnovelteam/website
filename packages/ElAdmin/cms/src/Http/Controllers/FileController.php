<?php

namespace ElAdmin\Cms\Http\Controllers;

use Illuminate\Http\Request;
use App\File;

class FileController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $files = File::paginate(20);
        return view('CMSV::Pages.Files.index', compact('files'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $audio = ['mp3'];
        $video = ['mkv','mp4'];
        $image = ['jpg','png', 'jpeg', 'svg'];
        $ext = $request->file('file')->getClientOriginalExtension();
        switch ($ext) {
            case in_array($ext, $image):
                $type = 'image';
            break;
            case in_array($ext, $audio):
                $type = 'audio';
            break;
            case in_array($ext, $video):
                $type = 'video';
            break;
            default:
                return abort(403);
        }
        $document = File::process($request->file('file'), $type)->Upload();
        $file = new File;
        $file->name = $document->name;
        $file->path = $document->path;
        $file->thumbnail_path = $document->thumbnail_path;
        $file->type = $type;
        $file->save();
        // flash()->overlay('موفقیت!', "فایل {$request->file('file')->getClientOriginalName()} با موفقیت آپلود شد");
        return ['okay' => true];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        File::findOrfail($id)->delete();
        return back();
    }
}
