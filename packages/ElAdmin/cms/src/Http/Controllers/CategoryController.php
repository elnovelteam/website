<?php

namespace ElAdmin\Cms\Http\Controllers;

use Illuminate\Http\Request;
use App\Maincategory;

class CategoryController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Maincategory::withCount('children')->with((['parent']))->paginate(12);
        return view('CMSV::Pages.Categories.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $types = Maincategory::whereType(null)->get();
		return view('CMSV::Pages.Categories.create', compact('types'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $messages = [
        //     'title.required' => 'فیلد نام اجباری است',
        //     'title.unique' => 'این نام تکراری است',
        //     'title.min' => 'حداقل سه کاراکتر برای فیلد نام پر کنید',
        //     'title.max' => 'حداکثر تعداد مجاز این فیلد 60 کاراکتر است'
        // ];
        $rules = [
            'title' => 'required|min:3|max:60|unique:maincategories',
        ];
        $request->validate($rules);
        $category = new Maincategory;
        $category->title = $request->title;
        $category->type  = $request->type ?: null;
        $category->description = ($request->description) ? $request->description : '';
        $category->save();
        flash()->success('!انجام شد','دسته بندی با موفقیت ساخته شد');
        return redirect()->to(route('Admin.categories.index'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Maincategory $category)
    {
        $types = Maincategory::whereType(null)->get();
        $parentables = Maincategory::where([['parent_id' , null], ['type' , '=', $category->type], ['id', '!=', $category->id ]])->get();
        return view('CMSV::Pages.Categories.edit', compact('category', 'types', 'parentables'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Maincategory $category)
    {
        $rules = [
            'title' => 'required|min:3|max:60',
        ];
        $request->validate($rules);
        $category->title = $request->title;
        $category->type  = $request->type ?: null;
        $category->parent_id  = $request->parent ?: null;
        $category->description = ($request->description) ? $request->description : '';
        $category->save();
        flash()->success('!انجام شد','دسته بندی با موفقیت بروزرسانی شد');
        return redirect()->to(route('Admin.categories.edit', ['category' => $category->id]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Maincategory::findOrfail($id)->delete();
        return back();
    }
}
