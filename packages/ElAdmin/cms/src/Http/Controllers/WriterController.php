<?php

namespace ElAdmin\Cms\Http\Controllers;

use App\Definition;
use Illuminate\Http\Request;
use App\Writer;

class WriterController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filter = collect([]);
        $title = null;
        foreach ($request->query as $key => $value) {
            if ($key == 'title') {
                $title = $value;
                continue;
            }
            $value = is_numeric($value) ? (int) $value : $value;
            $value = $value == "false" ? false : $value;
            $value = $value == "true" ? true : $value;
            $filter->push([$key, '=', $value]);
        }
        $writers = Writer::with('user')->where($filter->toArray())->orderBy('created_at', 'desc')->paginate(12);
        return view('CMSV::Pages.Writers.index', compact('writers'))->with('custom_title', $title);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // $customer = Customer::find($id)->firstOrFail()->load('state.country');
        // return $customer;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Writer $writer)
    {
        $writer->load('photo.file');
        $active_modes = ['درخواست تکمیل نشده' , 'فعال', 'در انتظار تایید'];
        return view('CMSV::Pages.Writers.edit', compact('writer', 'active_modes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Writer $writer)
    {
        $writer->first_name = $request->first_name;
        $writer->last_name = $request->last_name;
        $writer->nickname = $request->nickname;
        $writer->active = (int) $request->active;
        $writer->certified = $request->certified ? true : false;
        $writer->admin_rate = $request->admin_rate;
        $writer->education_id = $request->education;
        $writer->interest_id = $request->interest;
        $writer->exprience_id = $request->exprience;
        $writer->save();
        flash()->success("موفقیت !", "نویسنده آپدیت شد");
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Writer $writer)
    {
        $writer->delete();
        return back();
    }
}
