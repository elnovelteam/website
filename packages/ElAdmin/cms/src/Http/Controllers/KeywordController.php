<?php

namespace ElAdmin\Cms\Http\Controllers;

use App\Keyword;
use Illuminate\Http\Request;

class KeywordController extends BaseController
{
    public function index()
    {
        $keywords = Keyword::paginate(12);
        return view('CMSV::Pages.Keywords.index', compact('keywords'));
    }
    public function create()
    {
        return view('CMSV::Pages.Keywords.create');
    }
    public function store(Request $request)
    {
        $keyword = Keyword::whereValue($request->value)->first();
        if (! $keyword) {
            $keyword = new Keyword;
            $keyword->value = $request->value;
            flash()->success("کلمه کلیدی ایجاد شد", "");
            $keyword->save();
        }
        return redirect()->to(route("Admin.keywords.index"));
    }
    public function edit(Keyword $keyword)
    {
        return view('CMSV::Pages.Keywords.edit', compact('keyword'));
    }
    public function update(Request $request, Keyword $keyword)
    {
        $latestName = Keyword::whereRaw("`id` != '{$keyword->id}' and `value` = '{$keyword->value}'")
        ->orderBy('id')->pluck('value');
        if (! $latestName) {
            $keyword->value = $request->value;
            $keyword->save();
        }
        return redirect()->to(route("Admin.keywords.index"));
    }
    public function destroy($id)
    {
        Keyword::findOrfail($id)->delete();
        return back();
    }
}
