<?php

namespace ElAdmin\Cms\Http\Controllers;

use Illuminate\Http\Request;
use App\Gallery;

class GalleryController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('CMSV::Pages.Gallery.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $photo = Gallery::fromFile($request->file('photo'));
        return $photo;
        // ->Upload('/images');
        $gallery = new Gallery;
        $gallery->name = $photo->name;
        $gallery->path = $photo->path;
        $gallery->thumbnail_path = $photo->thumbnail_path;
        $gallery->save();
        return 'okay';
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Gallery::findOrfail($id)->delete();
        return back();
    }
}
