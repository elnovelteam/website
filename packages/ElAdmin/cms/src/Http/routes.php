<?php
Route::namespace('ElAdmin\Cms\Http\Controllers')->middleware(['web','auth','roler'])->prefix('eladmin')->name('Admin.')->group(function () {
    Route::get('/', ['uses' => 'AdminPanelController@index']);
    Route::resource('tags', 'TagsController')->except(['show']); // Tags
    Route::resource('roles', 'RoleController')->except(['show']);
    Route::resource('tickets', 'TicketController');
    Route::resource('writers', 'WriterController');
    Route::post('tickets/{ticket}/change/{status}', 'TicketController@change')->name('tickets.change');
    Route::resource('permissions', 'PermissionController')->except(['show']);
    Route::resource('comments', 'CommentController')->except(['show']);
    Route::resource('users', 'UsersController')->except(['show']);
    Route::resource('categories', 'CategoryController');
	Route::resource('pages', 'PageController');
	Route::resource('posts', 'PostController');
    Route::resource('sliders', 'SliderController');
    Route::resource('menus', 'MenuController');
    Route::resource('keywords', 'KeywordController');
    Route::resource('podcasts', 'PodcastController');
    Route::resource('files', 'FileController');
});
