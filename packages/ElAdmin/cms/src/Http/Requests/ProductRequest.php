<?php

namespace ElAdmin\Cms\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            '_token' => 'required',
            'title' => 'required',
            'description' => 'required',
            'price' => 'required|integer',
            'special_price' => 'required|integer',
            'count' => 'integer',
            'max_allowed_count_to_show' => 'integer',
            'discount' => 'integer',
        ];
    }
}
