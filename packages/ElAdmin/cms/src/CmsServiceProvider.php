<?php

namespace ElAdmin\Cms;

use Illuminate\Support\ServiceProvider;

class CmsServiceProvider extends ServiceProvider
{
    protected $mainpath = __DIR__ . DIRECTORY_SEPARATOR;
    public function register()
    {
        $this->app->bind('Cms', function(){
            return new Cms;
        });
        $this->mergeConfigFrom($this->mainpath . "Config/main.php",'cmsv');

    }
    public function boot()
    {
        require __DIR__ . '/Http/routes.php';
        $this->loadTranslationsFrom($this->mainpath . 'Resources/Lang', 'CMSL');
        app()->setLocale('en');
        $this->loadViewsFrom($this->mainpath . "Resources/Views", 'CMSV');

        $this->publishes([
            $this->mainpath . 'Http\Requests' => base_path('app\Http\Requests'),
        ], 'requests');
    }
}
