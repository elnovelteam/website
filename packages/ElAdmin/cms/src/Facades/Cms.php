<?php

namespace ElAdmin\Cms\Facades;

use Illuminate\Support\Facades\Facade;


class ElCms extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'ElCms';
    }
}

