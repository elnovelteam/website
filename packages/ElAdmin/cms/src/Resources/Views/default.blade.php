<!DOCTYPE html>
<html lang="{{ App::getLocale() }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    @yield('title')
    <link rel="stylesheet" href="{{ asset('css/admin.css') }}">
    @if (app()->getLocale() == 'fa')
        <link rel="stylesheet" href="{{ asset('css/rtl.css') }}">
    @endif
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body>
    <div class="col-12 p-0 m-0 full-sc">
        @yield('content')
    </div>
    <script src="{{ asset('js/app.js') }}"></script>
    <script>
        var currentURI = "{{ url()->full() }}";
        currentURI = currentURI.replace("&amp;", "&");
        var anchor     = $(`a[href="${currentURI}"]`);
        if (anchor.parent().hasClass("sub")) {
            anchor.parent().parent().parent().addClass("active");
        } </script>
    @yield('script')
</body>
</html>
