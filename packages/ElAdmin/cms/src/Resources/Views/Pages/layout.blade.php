@extends('CMSV::default')
@section('content')
    <div class="col-12 admin-area p-0 m-0 h-12">
        <div class="col-lg-2 col-md-3 d-md-block d-none h-12 p-0" id="left-menu">
            <div class="top-section col-12 text-center float-left p-3">
                <p class="col-12 mt-3 p-0 float-right text-center"><i class="fas fa-pencil-alt"></i> @lang('CMSL::admin.eladmin')</p>
            </div>
            @include('CMSV::Utils.menu')
        </div>
        <div class="col-lg-10 col-md-9 col-12 h-12"
        id="mainpage">
            <div class="col-12 navbar-top">
                <button id="collapser" class="btn btn-secondary">
                    <i class="fas fa-angle-double-right"></i>
                </button>
                <button data-event="max" class="btn scr-controller">
                    <i class="fas fa-expand-arrows-alt fa-compress"></i>
                </button>
                @if(request()->headers->has('referer'))
                    <a class="btn btn-dark" href="{{ request()->headers->get('referer') }}">بازگشت به صفحه قبلی</a>
                @endif 
            </div>
            <div class="col-12 contentbar float-left" id="orderforprint">
                @include('partials.error')
                @yield('pagecontent')
            </div>
        </div>
    </div>
@endsection

@section('script')
    {{-- <script src="{{ asset('js/swal.js') }}"></script> --}}
    <script>
    var elem = document.documentElement;
    /* View in fullscreen */
    function openFullscreen() {
    if (elem.requestFullscreen) {
        elem.requestFullscreen();
    } else if (elem.mozRequestFullScreen) { /* Firefox */
        elem.mozRequestFullScreen();
    } else if (elem.webkitRequestFullscreen) { /* Chrome, Safari and Opera */
        elem.webkitRequestFullscreen();
    } else if (elem.msRequestFullscreen) { /* IE/Edge */
        elem.msRequestFullscreen();
    }
    }
    /* Close fullscreen */
    function closeFullscreen() {
    if (document.exitFullscreen) {
        document.exitFullscreen();
    } else if (document.mozCancelFullScreen) { /* Firefox */
        document.mozCancelFullScreen();
    } else if (document.webkitExitFullscreen) { /* Chrome, Safari and Opera */
        document.webkitExitFullscreen();
    } else if (document.msExitFullscreen) { /* IE/Edge */
        document.msExitFullscreen();
    }
    }
    $('.scr-controller').click(function(){
        $(' > i',this).toggleClass('fa-expand-arrows-alt');
        if ($(this).attr('data-event') == 'max') {
            $(this).attr('data-event','min');
            return openFullscreen();
        }
        $(this).attr('data-event','max');
        return closeFullscreen();
    });
    $('#collapser').click(function(){
        $(' > i',this).toggleClass('fa-angle-double-right fa-angle-double-left');
        $("#left-menu").toggleClass('collapsed');
        $("#mainpage").toggleClass('col-lg-10 col-md-9')
    });
    $('nav li').click(function() {
        $(' i.opener',this).toggleClass('opened');
        $(this).toggleClass('active');
    });
    function printdiv(printdivname)
    {
        var footstr = "</body>";
        var newstr = document.getElementById(printdivname).innerHTML;
        var oldstr = document.body.innerHTML;
        document.body.innerHTML = newstr+footstr;
        window.print();
        document.body.innerHTML = oldstr;
        return false;
    }
    $(document).on('click','#printOrder', function(){
        $('.hideforprint').hide();
        printdiv('orderforprint');
        $('.hideforprint').show();
    }); </script>
    @include('partials.flash')
    @yield('scripts')
@endsection
