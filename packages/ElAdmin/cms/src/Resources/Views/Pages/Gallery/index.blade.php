@extends('CMSV::Pages.layout')
@section('title')
    <title>Gallery</title>
@endsection
@section('pagecontent')
    <div class="col-12 float-left p-3 mt-4">
        <h1 class="float-left">Gallery</h1>
        <div class="col-12 p-4 float-left">
            <form id="addPhotosForm" action="{{ route('Admin.gallery.store') }}" class="dropzone" method="POST">
            @csrf
            </form>
        </div>
    </div>
    <div class="col-12 float-left p-0">
        <div id="react-gallery" data-delete="{{ route('Admin.gallery.index') }}"
        data-csrf={{ csrf_token() }} data-mainpage="{{ route('home') }}">
        </div>
    </div>
@endsection
@section('scripts')
    <script src="{{ asset('js/dropzone.js') }}"></script>
    <script src="{{ asset('js/Gallery.js') }}"></script>
    <script>
    $('#react-gallery .btn-warning').remove();
    $('#react-gallery .btn-danger:first').remove();</script>
@endsection
