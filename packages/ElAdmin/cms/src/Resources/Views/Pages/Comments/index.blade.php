@extends('CMSV::Pages.layout')
@section('title')
    <title>فهرست کامنت ها</title>
    <style>
        .checkbox-c input:checked+.check-handle:before { transform: translateX(-41px) !important; }
    </style>
@endsection
@section('pagecontent')
    <div class="col-12 float-left p-3">
        <h1 class="float-right text-right">کامنت ها</h1>
    </div>
    <table class="table mt-4 text-center">
        <thead class="thead-dark">
            <tr>
                <th scope="col">نام</th>
                <th scope="col">ایمیل</th>
                <th scope="col">بدنه</th>
                <th scope="col">تایید</th>
                <th scope="col">تاریخ</th>
                <th scope="col">پسند</th>
                <th scope="col">ویرایش</th>
                <th scope="col">حذف</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($comments as $comment)
            <tr>
                <th>{{ $comment->user->name }}</th>
                <td>{{ $comment->user->email }}</td>
                <td>{{ $comment->body }}</td>
                <td>
                    <label style="margin-left:10px;" data-api="{{ route('api.Cms.comments.toggle', ['comment' => $comment->id, 'api_token' => auth()->user()->api_token]) }}" class="checkbox-c toggle-check" for="active-comment-{{ $comment->id }}">
                        <input type="checkbox" name="active" @if ($comment->active) checked @endif id="active-comment-{{ $comment->id }}" value="true">
                        <span class="check-handle"></span>
                    </label>
                </td>
                <td>{{ $comment->created_at->diffForHumans() }}</td>
                <td class="text-danger">{{ $comment->likes_count }}<i class="fas fa-heart mr-1"></i></td>
                <td><a class="btn btn-outline-info" href="{{ route('Admin.comments.edit',['comment' => $comment->id]) }}"><i class="fas fa-pencil-alt"></i></a></td>
                <td>
                    <form action="{{ route('Admin.comments.destroy',['comment' => $comment->id]) }}" method="post">
                    @method('DELETE')
                    @csrf
                    <button type="submit" class="btn btn-outline-danger"><i class="fas fa-times"></i></button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    <div class="col-12 text-center">
        {{ $comments->links() }}
    </div>
@endsection

@section('scripts')
    <script>
        $('.toggle-check > input').change(function() {
            console.log('hello');
            axios.post($(this).parent().attr('data-api'), {}).then(res => {
                let {mode} = res.data;
                Swal.fire({
                    icon: mode ? 'success' : 'info',
                    title: mode ? 'فعال شد' : 'غیر فعال شد',
                    timer: 1000,
                    showConfirmButton	: true,
                    customClass: {
                        content: 'persian-text'
                    }
                })
            });
        });
    </script>
@endsection
