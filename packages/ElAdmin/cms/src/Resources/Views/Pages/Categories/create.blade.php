@extends('CMSV::Pages.layout')
@section('title')
    <title>اضافه کردن دسته بندی</title>
@endsection
@section('pagecontent')
    <div class="col-12 p-3">  
        <div class="col-12 float-left p-3 text-right">
            <a href="{{ route('Admin.categories.index') }}" class="btn btn-info float-right mt-2 ml-2"><i class="fas fa-angle-right"></i></a>
            <h1 class="float-right">اضافه کردن دسته بندی</h1>
        </div>
        <form action="{{ route('Admin.categories.store') }}" method="POST" role="form" class="col-12 mt-4 p-0">
        @csrf
            <div class="input-group col-12 col-md-4 col-sm-6 mb-4">
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fas fa-font"></i></span>
                </div>
                <input required type="text" class="form-control text-right" name="title" id="title"
                value="{{ old('title') }}" placeholder="عنوان">
            </div>
            <div class="input-group col-12 text-right rtl col-sm-4 mb-4">
                <div class="input-group-prepend">
                    <span class="input-group-text"><p class="m-0">نوع دسته بندی</p></span>
                </div>
                <select required class="form-control rtl text-right h-100" name="type" id="type">
                    <option>بدون نوع</option>
                    @foreach ($types as $type)
                        <option class="text-right rtl" value="{{ $type->title }}">{{ $type->description }}</option>    
                    @endforeach
                </select>
            </div>
            <div class="input-group col-12 col-sm-8 mb-4">
                <textarea class="form-control text-right" name="description" id="description" placeholder="توضیحات" cols="30" rows="10">{{ old('description') }}</textarea>
            </div>
            <div class="input-group col-12 float-left mb-4">
                <div class="col-12 p-0 mt-3 float-right text-right">
                    <button type="submit" class="btn btn-outline-primary">ذخیره</button>
                </div>
            </div>
        </form>
    </div>
@endsection
@section('scripts')
    <script>
        $('#type').select2();
    </script>
@endsection