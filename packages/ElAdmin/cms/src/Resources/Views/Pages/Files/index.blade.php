@extends('CMSV::Pages.layout')
@section('title')
    <title>فایل ها</title>
@endsection
@section('pagecontent')
<div class="col-12 p-3 float-left">
    <form class="p-3 mb-4 rtl col-12 dropzone" action="{{ route('Admin.files.store') }}" id="elnovelfileuploader"
    enctype="multipart/form-data" accept="audio/*,video/*,image/*" method="post">
    @csrf
    </form>
    @foreach ($files as $file)
    <div style="overflow:hidden;" class="col-lg-3 col-md-4 col-12 float-left">
        <a target="_blank" href="{{ asset($file->path) }}">
        <div class="cover text-center">
            <i class="fa-4x fas fa-file-{{ $file->type }}"></i>
            <p>{{ $file->name }}</p>
        </div>
        </a>
    </div>
    @endforeach
</div>
<div class="col-12 text-center float-left">
    {{ $files->links() }}
</div>
@endsection
@section('scripts')
    <script src="{{ asset('js/dropzone.js') }}"></script>
    <script>Dropzone.options.elnovelfileuploader = {
        paramName: 'file',
        maxFilesize: 200,
        acceptedFiles: ".mp3, .mkv, .mp4, .jpg, .jpeg, .png, .svg",
        dictInvalidFileType: "شما نمی توانید این فایل را آپلود کنید",
        dictDefaultMessage: "فایل ها را در اینجا آپلود کنید. \n\nحتی میتوانید آنها را در اینجا بیاندازید",
        dictFileTooBig: "حجم فایل بسیار زیاد است"
    };</script>
@endsection