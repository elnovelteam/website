@extends('CMSV::Pages.layout')
@section('title')
    <title>ویرایش داستان نویس {{ $writer->nickname }}</title>
@endsection

@section('pagecontent')
<div id="react-pick-images" data-target="{{ route('api.Cms.files') }}" data-api-token="{{ auth()->user()->api_token }}" class="react-file-picker"
    data-csrf={{ csrf_token() }} data-usage="select:singular" data-callback="loadPodcastPhoto" data-targetHTML="#image" data-filetype="image" data-mainpage="{{ route('home') }}"
    data-main="{{ route('api.Cms.changeFile', ['method' => 'writer', 'id' => $writer->id, 'type' => 'photo']) }}"></div>
<div class="col-12 float-left p-3">
    <h1 class="col-12 text-right rtl">ویرایش نویسنده : {{ $writer->nickname }}</h1>
    <div class="col-12 float-left">
        <p id="image">
            @if ($writer->photo)
                <a blank="_blank" href="{{  asset($writer->photo->file->path) }}">
                <img height="100" width="100" src="{{  asset($writer->photo->file->thumbnail_path) }}" alt="{{ $writer->photo->file->name }}">
                </a>
            @endif
        </p>
    </div>
    <form action="{{ route('Admin.writers.update', ['writer' => $writer->id]) }}" method="POST" role="form" enctype="multipart/form-data" class="col-12 mt-4 p-0">
        @csrf
        @method('PATCH')
        <div class="input-group col-12 col-md-4 col-sm-6 mb-4">
            <div class="input-group-prepend">
                <span class="input-group-text">@lang('CMSL::forms.form.firstname')</span>
            </div>
            <input required type="text" class="form-control" name="first_name" id="first_name"
            value="{{ old('firstname') ?: $writer->first_name }}" placeholder="@lang('CMSL::forms.form.firstname')">
        </div>
        <div class="input-group col-12 col-md-4 col-sm-6 mb-4">
            <div class="input-group-prepend">
                <span class="input-group-text">@lang('CMSL::forms.form.lastname')</span>
            </div>
            <input required type="text" class="form-control" name="last_name" id="last_name"
            value="{{ old('lastname') ?: $writer->last_name }}" placeholder="@lang('CMSL::forms.form.lastname')">
        </div>
        <div class="input-group col-12 col-md-4 col-sm-6 mb-4">
            <div class="input-group-prepend">
                <span class="input-group-text">@lang('CMSL::forms.form.nickname')</span>
            </div>
            <input required type="text" class="form-control" name="nickname" id="nickname"
            value="{{ old('nickname') ?: $writer->nickname }}" placeholder="@lang('CMSL::forms.form.nickname')">
        </div>
        <div class="input-group col-12 col-md-4 col-sm-6 mb-4">
            <div class="input-group-prepend">
                <span class="input-group-text">@lang('CMSL::forms.form.admin_rate')</span>
            </div>
            <input required type="number" min="0" max="100" step="1" class="form-control" name="admin_rate" id="admin_rate"
            value="{{ old('admin_rate') ?: $writer->admin_rate }}" placeholder="@lang('CMSL::forms.form.admin_rate')">
        </div>
        <div class="input-group col-12 col-md-4 col-sm-6 mb-4">
            <div class="input-group-prepend">
                <span class="input-group-text">وضعیت</span>
            </div>
            <select class="form-control" name="active" id="active">
                @foreach ($active_modes as $mode)
                    <option @if($loop->index == $writer->active) selected @endif value="{{ $loop->index }}">{{ $mode }}</option>
                @endforeach
            </select>
        </div>
        <div class="input-group col-12 col-md-4 col-sm-6 mb-4">
            <div class="input-group-prepend">
                <span class="input-group-text">تحصیلات</span>
            </div>
            <select class="form-control" name="education" id="education">
                @foreach (definitions('education') as $education)
                    <option @if($writer->education_id == $education->id) selected @endif value="{{ $education->id }}">{{ $education->title }}</option>
                @endforeach
            </select>
        </div>
        <div class="input-group col-12 col-md-4 col-sm-6 mb-4">
            <div class="input-group-prepend">
                <span class="input-group-text">انگیزه</span>
            </div>
            <select class="form-control" name="interest" id="interest">
                @foreach (definitions('interest') as $interest)
                    <option @if($writer->interest_id == $interest->id) selected @endif value="{{ $interest->id }}">{{ $interest->title }}</option>
                @endforeach
            </select>
        </div>
        <div class="input-group col-12 col-md-4 col-sm-6 mb-4">
            <div class="input-group-prepend">
                <span class="input-group-text">تجربه</span>
            </div>
            <select class="form-control" name="exprience" id="exprience">
                @foreach (definitions('exprience') as $exprience)
                    <option @if($writer->exprience_id == $exprience->id) selected @endif value="{{ $exprience->id }}">{{ $exprience->title }}</option>
                @endforeach
            </select>
        </div>
        <div class="input-group col-12 col-md-4 col-sm-6 p-2 mb-4">
            <p class="ml-2"><label for="certified">قلم طلایی</label></p>
            <input type="checkbox" value="true" name="certified" id="certified" @if($writer->certified) checked @endif>
        </div>
        <div class="input-group text-left col-12 mt-3">
            <button type="submit" class="btn btn-outline-primary">بروزرسانی</button>
        </div>
    </form>
</div>
@endsection

@section('scripts')
<script src="{{ asset('js/pick.js') }}"></script>
<script>
const mainpage = "@php echo route('home') @endphp";
function loadPodcastPhoto(cb, th) {
    let {path, name, thumbnail_path} = cb;
    $(th).html(`<a target="_blank" href="${mainpage}/${path}"><img height="100" width="100" src="${mainpage}/${thumbnail_path}" alt="${name}"></a>`);
}
</script>
@endsection