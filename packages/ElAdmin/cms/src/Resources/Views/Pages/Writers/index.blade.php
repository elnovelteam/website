@extends('CMSV::Pages.layout')
@section('title')
    <title>@if($custom_title) {{ $custom_title }} @else @lang('CMSL::admin.writers-list') @endif</title>
@endsection
@section('pagecontent')
    <div class="col-12 float-left p-3">
        <h1 class="float-right text-right">@if($custom_title) {{ $custom_title }} @else @lang('CMSL::admin.writers-list') @endif</h1>
    </div>
    <table class="table table-bordered mt-4 text-center">
        <thead class="thead-dark">
            <tr>
                <th scope="col">نام و نام خانوادگی</th>
                <th scope="col">تخلص</th>
                <th scope="col">تاریخ عضویت</th>
                <th scope="col">پروفایل کاربر</th>
                <th scope="col">ویرایش</th>
                <th scope="col">حذف</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($writers as $writer)
            <tr>
                <th>{{ $writer->first_name . ' ' . $writer->last_name }}</th>
                <td><a target="_blank" href="{{ route('writers.show', ['writer' => $writer->nickname]) }}">{{ $writer->nickname }}</a></td>
                <td>{{ $writer->created_at->diffForHumans() }}</td>
                <td><a class="btn btn-sm btn-outline-primary" href="{{ route('Admin.users.edit',['user' => $writer->user->id]) }}"><i class="fas fa-user"></i></a></td>
                <td><a class="btn btn-sm btn-outline-info" href="{{ route('Admin.writers.edit',['writer' => $writer->id]) }}"><i class="fas fa-pencil-alt"></i></a></td>
                <td>
                    <form action="{{ route('Admin.writers.destroy',['writer' => $writer->id]) }}" method="post">
                    @method('DELETE')
                    @csrf
                    <button type="submit" class="btn btn-sm btn-outline-danger"><i class="fas fa-times"></i></button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    <div class="col-12 text-center">
        {{ $writers->links() }}
    </div>
@endsection
