@extends('CMSV::Pages.layout')
@section('title')
    <title>افزودن فهرست</title>
@endsection
@section('pagecontent')
    <div class="col-12 p-3">
        <h1 class="text-right">افزودن فهرست</h1>
        <form action="{{ route('Admin.menus.store') }}" id="addnewpost" method="POST" role="form" enctype="multipart/form-data"
        class="col-12 mt-4 p-0">
        @csrf
            <div class="input-group col-12 col-sm-6 float-right">
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fas fa-font"></i></span>
                </div>
                <input required type="text" class="form-control text-right" name="title" id="title"
                value="{{ old('title') }}" placeholder="عنوان فهرست">
            </div>
            <div class="input-group col-12 mt-4 p-0 AdminSection float-right">
                <div class="col-12 header">
                    <h1>ویرایش منو</h1>
                </div>
                <div class="col-12 content">
                    <div class="col-12 p-0 float-left mb-3"><button type="button" id="addMenuItem" class="btn btn-outline-secondary float-right"><i class="fas fa-plus"></i></button></div>
                    <ul class="col-12 float-left" name="body" id="menuEditor"></ul>
                </div>
            </div>
            <div class="input-group col-12 float-left mb-4">
                <div class="col-12 p-0 mt-3 float-right text-right">
                    <button type="submit" class="btn btn-outline-primary">ذخیره</button>
                </div>
            </div>
        </form>
    </div>
@endsection
@section('scripts')
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="{{ asset('js/menu.js') }}"></script>
@endsection
