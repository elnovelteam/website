@extends('CMSV::Pages.layout')
@section('title')
    <title>لیست فهرست ها</title>
@endsection
@section('pagecontent')
    <div class="col-12 p-3 float-left text-right">
        <a href="{{ route('Admin.menus.create') }}" class="btn btn-info float-right mt-2 ml-2">+</a>
        <h1 class="float-right">فهرست ها</h1>
    </div>
    <table class="table mt-4 text-center">
        <thead class="thead-dark">
            <tr>
                <th scope="col">ردیف</th>
                <th scope="col">عنوان</th>
                <th scope="col">ویرایش</th>
                <th scope="col">حذف</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($menus as $menu)
            <tr>
                <th scope="row">{{ $loop->index + 1 }}</th>
                <td>{{ $menu->title }}</td>
                <td><a class="btn btn-outline-info" href="{{ route('Admin.menus.edit',['id' => $menu->id]) }}"><i class="fas fa-pencil-alt"></i></a></td>
                <td>
                    <form action="{{ route('Admin.menus.destroy',['id' => $menu->id]) }}" method="post">
                    @method('DELETE')
                    @csrf
                    <button type="submit" class="btn btn-outline-danger"><i class="fas fa-times"></i></button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    <div class="col-12 text-center">
        {{ $menus->links() }}
    </div>
@endsection
