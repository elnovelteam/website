@extends('UAV::layouts.default')

@section('header')
<title>درخواست پشتیبانی جدید</title>
@endsection

@section('content')
<h3 class="title mb-4">درخواست پشتیبانی جدید</h3>
<div class="col-12 float-left">
    <form class="col-12 float-right" action="{{ route('userarea.tickets.store') }}" method="post">
        @csrf
        <div class="col-lg-5 col-md-6 col-12 input-group mb-3">
            <input type="text" name="title" id="title" value="{{ old('title') }}" class="form-control text-right rtl" placeholder="@lang('UAL::userarea.form.title')">
            <div class="input-group-append float-right">
                <span class="input-group-text"><i class="far fa-file-alt"></i></span>
            </div>
        </div>
        <div class="col-lg-5 col-md-6 col-12 input-group mb-3 lilselect">
            <select class="form-control" name="section" id="section">
                <option></option>
                @foreach ($sections as $section)
                    <option value="{{ $section->id }}">{{ $section->name }} {{ $section->description }}</option>
                @endforeach
            </select>
            <div class="input-group-append float-right">
                <span class="input-group-text"><i class="fas fa-building"></i></span>
            </div>
        </div>
        <div class="col-lg-5 col-md-6 col-12 input-group mb-3">
            <select class="form-control rtl text-right" name="priority" id="priority">
                <option value="1">زیاد</option>
                <option value="2">متوسط</option>
                <option value="3">کم</option>
            </select>
            <div class="input-group-append float-right">
                <span class="input-group-text"><i class="fas fa-exclamation-circle"></i></span>
            </div>
        </div>
        <div class="col-lg-8 col-md-6 col-12 input-group mb-3">
            <textarea rows="10" name="question" id="question" class="form-control text-right rtl" placeholder="@lang('UAL::userarea.form.question')">{{ old('question') }}</textarea>
        </div>
        <div class="col-lg-8 col-12 float-right">
            <button type="submit" class="btn btn-secondary float-left">ثبت</button>
        </div>
    </form>
</div>
@endsection
@section('script')
<script>$('#section').select2({placeholder: 'بخش مربوطه'});</script>
@endsection