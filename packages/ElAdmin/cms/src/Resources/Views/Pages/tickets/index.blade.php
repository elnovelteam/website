@extends('CMSV::Pages.layout')

@section('header')
<title>درخواست های پشتیبانی</title>
@endsection

@section('pagecontent')
    <h1 class="title text-right mt-3 mb-3">درخواست های پشتیبانی</h1>
    <table class="table table-bordered text-center rtl">
        <thead class="thead-dark">
            <tr>
            <th scope="col">#</th>
            <th scope="col">شماره تیکت</th>
            <th scope="col">اولویت</th>
            <th scope="col">عنوان</th>
            <th scope="col">صاحب تیکت</th>
            <th scope="col">نقش</th>
            <th scope="col">بخش مربوطه</th>
            <th scope="col">وضعیت</th>
            <th scope="col">آخرین بروزرسانی</th>
            <th scope="col">مشاهده</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($tickets as $ticket)
            <tr>
                <th scope="row">{{ $loop->index + 1 }}</th>
                <td>{{ $ticket->code }}</td>
                <td>@lang('UAL::userarea.ticket.' . $ticket->force_rate)</td>
                <td>{{ $ticket->title }}</td>
                <td>{{ $ticket->user->name }}</td>
                <td>@if (count($ticket->user->roles) > 0) @foreach ($ticket->user->roles as $role) {{ $role->name }} @endforeach @else کاربر @endif</td>
                <td>{{ $ticket->section->name }}</td>
                <td>@lang('UAL::userarea.ticket.' . $ticket->status)</td>
                <td>{{ $ticket->updated_at->diffForHumans() }}</td>
                <td>
                    <a class="btn btn-sm btn-outline-dark" href="{{ route('Admin.tickets.show', ['ticket' => $ticket->id]) }}">
                    <i class="fas fa-eye"></i>
                    </a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
@endsection