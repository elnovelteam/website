@extends('CMSV::Pages.layout')

@php $user = auth()->user(); $user->load('photo'); @endphp

@section('header')
<title>درخواست پشتیبانی شماره #{{ $ticket->code }}</title>
@endsection

@section('pagecontent')
    <h4 class="title text-right my-4">تیکت شماره #{{ $ticket->code }} - {{ $ticket->title }}</h4>
    <div class="col-12 mb-4 p-0 float-left ticket-area">
        <div class="messages col-12 px-3 py-3 float-left">
            @if ($ticket->status != 'CLOSED')
            <form method="POST" action="{{ route('Admin.tickets.change', ['ticket' => $ticket->id, 'status' => 'CLOSED']) }}" class="col-6 mb-3 p-3 text-center float-left">
                @csrf
                <button class="btn btn-sm btn-danger" type="submit">بستن این تیکت<i class="fas fa-power-off m-1"></i></button>
            </form>
            <form method="POST" action="{{ route('Admin.tickets.change', ['ticket' => $ticket->id, 'status' => 'INPROGRESS']) }}" class="col-6 mb-3 p-3 text-center float-left">
                @csrf
                <button class="btn btn-sm @if($ticket->status == 'INPROGRESS') {{ __('btn-success') }} @else {{ __('btn-secondary') }}  @endif" type="submit">در حال پیگیری<i class="fas fa-spinner m-1"></i></button>
            </form>
            @else
            <form method="POST" action="{{ route('Admin.tickets.change', ['ticket' => $ticket->id, 'status' => 'OPEN']) }}" class="col-12 mb-3 p-3 text-center float-left">
                @csrf
                <button class="btn btn-sm btn-primary" type="submit">بازگشایی تیکت<i class="fas fa-power-off m-1"></i></button>
            </form>
            @endif
            <div class="message question">
                <a href="{{ route('Admin.users.edit', ['user' => $ticket->user->id]) }}">
                    <img class="photo" src="@if($ticket->user->photo) {{ asset('images/profile-test.jpg') }} @else {{ asset('images/inc/' . $ticket->user->sex . '-avatar.svg') }} @endif" alt="">
                </a>
                <p class="text">{{ $ticket->question }}<span class="time"><i class="far fa-clock"></i>{{ $ticket->created_at->format('H:i') }}</span></p>
            </div>
            @foreach ($messages as $message)
                <div class="message {{ $message->type }}">
                    @if ($message->type == 'answer')
                    <img title="{{ $user->name }}" class="photo" src="@if($user->photo) {{ asset('images/profile-test.jpg') }} @else {{ asset('images/inc/' . $user->sex . '-avatar.svg') }} @endif" alt="">
                    @else
                    <a href="{{ route('Admin.users.edit', ['user' => $ticket->user->id]) }}">
                        <img title="{{ $ticket->user->name }}" class="photo" src="@if($ticket->user->photo) {{ asset('images/profile-test.jpg') }} @else {{ asset('images/inc/' . $ticket->user->sex . '-avatar.svg') }} @endif" alt="">
                    </a>
                    @endif
                    <p class="text">{{  $message->text  }}<span class="time">@if($message->read_at && $message->type == 'answer') <i class="fas fa-check"></i> @endif<i class="far fa-clock"></i>{{ $message->created_at->format('H:i') }}</span></p>
                </div>
            @endforeach
        </div>
        <form action="{{ route('Admin.tickets.update', ['ticket' => $ticket->id]) }}" method="POST" class="messaging-area col-12 p-0 float-left">
            @csrf
            @method('PUT')
            <div class="col-12 h-12 p-0 input-group">
                <textarea required name="message" id="message" placeholder="@lang('UAL::userarea.form.message')"
                class="form-control text-right rtl" cols="30" rows="4">{{ old('message') }}</textarea>
                <div class="input-group-prepend float-right">
                    <button class="btn px-4 btn-dark"><i class="fas fa-paper-plane"></i></button>
                </div>
            </div>
        </form>
    </div>
@endsection