@extends('CMSV::Pages.layout')
@section('title')
    <title>لیست نوشته ها</title>
@endsection
@section('pagecontent')
    <div class="col-12 p-3 float-left text-right">
        <a href="{{ route('Admin.posts.create') }}" class="btn btn-info float-right mt-2 ml-2">+</a>
        <h1 class="float-right">نوشته ها</h1>
    </div>
    <table class="table mt-4 text-center">
        <thead class="thead-dark">
            <tr>
                <th scope="col">ردیف</th>
                <th scope="col">عنوان</th>
                {{-- <th scope="col">دسته بندی ها</th> --}}
                <th scope="col">زبان</th>
                <th scope="col">ویرایش</th>
                <th scope="col">حذف</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($posts as $post)
            <tr>
                <th scope="row">{{ $loop->index + 1 }}</th>
                <td>{{ $post->title }}</td>
                {{-- <td>{{ $post->count }}</td> --}}
                <td>{{ $post->lang }}</td>
                <td><a class="btn btn-outline-info" href="{{ route('Admin.posts.edit',['post' => $post->id]) }}"><i class="fas fa-pencil-alt"></i></a></td>
                <td>
                    <form action="{{ route('Admin.posts.destroy',['post' => $post->id]) }}" method="post">
                    @method('DELETE')
                    @csrf
                    <button type="submit" class="btn btn-outline-danger"><i class="fas fa-times"></i></button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    <div class="col-12 text-center">
        {{ $posts->links() }}
    </div>
@endsection
