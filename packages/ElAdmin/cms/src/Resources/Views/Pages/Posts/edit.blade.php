@extends('CMSV::Pages.layout')
@section('title')
    <title>ویرایش پست {{ $post->name }}</title>
@endsection

@section('pagecontent')
    <div id="react-pick-images" data-target="{{ route('api.Cms.files') }}" data-api-token="{{ auth()->user()->api_token }}" class="react-file-picker"
    data-csrf={{ csrf_token() }} data-usage="select:singular" data-callback="loadPodcastPhoto" data-targetHTML="#image" data-filetype="image" data-mainpage="{{ route('home') }}"
    data-main="{{ route('api.Cms.changeFile', ['method' => 'post', 'id' => $post->id, 'type' => 'photo']) }}"></div>

    <div class="col-12 p-3 float-left">
        <h1 class="text-right">ویرایش پست {{ $post->name }}</h1>
        <p id="image">
        @if ($post->photo)
            <a blank="_blank" href="{{  asset($post->photo->file->path) }}">
            <img height="100" width="100" src="{{  asset($post->photo->file->thumbnail_path) }}" alt="{{ $post->photo->file->name }}">
            </a>
        @endif
        </p>
        <form action="{{ route('Admin.posts.update', ['post' => $post->id]) }}" method="POST" role="form" class="col-12 mt-4 p-0" id="update-form">
            @csrf
            @method("PUT")
            <div class="input-group col-12 col-md-6 mb-3 float-right">
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fas fa-font"></i></span>
                </div>
                <input required type="text" class="form-control text-right" name="title" id="title"
                value="{{ old('title') ?: $post->title  }}" placeholder="عنوان">
            </div>
            <div class="input-group col-12 col-md-8 mb-3 float-right">
                <textarea class="form-control text-right" name="summary" id="summary" placeholder="چکیده"
                cols="30" rows="10">{{ old('description') ?: $post->summary }}</textarea>
            </div>
            <div class="col-md-8 col-12 mb-3 float-right" data-main="{{ route('home') }}"
            data-target-url="{{ route('api.platform.posts.show', ['post' => $post->id]) }}" data-mainpage="{{ route('home') }}"
            data-target="{{ route('api.Cms.files') }}" data-api-token="{{ auth()->user()->api_token }}" id="react-ck-editor"></div>
            @component('CMSV::partials.seo', ['seo' => $post->seo]) @endcomponent
            <div class="input-group col-12 float-left mb-4">
                <div class="col-12 p-0 mt-3 float-right text-right">
                    <button type="submit" id="savebtn" class="btn btn-outline-primary">بروزرسانی</button>
                </div>
            </div>
        </form>
    </div>
@endsection

@section('scripts')
<script src="{{ asset('js/pick.js') }}"></script>
<script>
const mainpage = "@php echo route('home') @endphp";
function loadPodcastFile(cb, th) {
    let {path, name, thumbnail_path} = cb.file;
    $(th).html(`<a target="_blank" href="${mainpage}/${path}">${name}</a>`);
}
function loadPodcastPhoto(cb, th) {
    let {path, name, thumbnail_path} = cb;
    $(th).html(`<a target="_blank" href="${mainpage}/${path}"><img height="100" width="100" src="${mainpage}/${thumbnail_path}" alt="${name}"></a>`);
}
$("#seo-keywords").select2();
</script>
<script src="{{ asset('js/ck-editor.js') }}"></script>
@endsection