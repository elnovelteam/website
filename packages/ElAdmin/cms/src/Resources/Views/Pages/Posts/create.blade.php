@extends('CMSV::Pages.layout')
@section('title')
    <title>اضافه کردن نوشته</title>
    <link href="https://cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">
@endsection
@section('pagecontent')
    {{-- <div id="react-collapsable-gallery" data-mainpage="{{ route('home') }}" targetpage="{{ route('home') }}/api/v1/gallery/setImages/posts/{{ $post->id }}/?api_token=thisistestapitoken"></div> --}}
    <div class="col-12 p-3">
        <h1 class="text-right">اضافه کردن نوشته</h1>
        <form action="{{ route('Admin.posts.store') }}" id="addnewpostform" method="POST" role="form" enctype="multipart/form-data"
        class="col-12 col-sm-10 col-lg-8 mt-4 p-0">
        @csrf
            <div class="input-group col-12 col-sm-6 float-right mb-4">
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fas fa-font"></i></span>
                </div>
                <input required type="text" class="form-control text-right" name="title" id="title"
                value="{{ old('title') }}" placeholder="عنوان نوشته">
            </div>
            <div class="input-group col-12 float-left mb-4">
				<div class="col-12 text-right" id="toolbar"></div>
				<div class="col-12" id="editor"></div>
            </div>
            <div class="input-group col-12 float-left mb-4">
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fas fa-tag"></i> برچسب ها</span>
                </div>
                <select id="post_tags" name="tags[]" title="tags" class="form-control col-6" multiple>
                    @foreach ($tags as $tag)
                        <option value="{{ $tag->id }}">{{ str_replace('_',' ',$tag->name) }}</option>
                    @endforeach
                </select>
            </div>
            <div class="input-group col-12 float-left mb-4">
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fas fa-bars"></i> منو</span>
                </div>
                <select id="post_menus" name="menu" title="menu" class="form-control col-6">
                    @foreach ($menus as $menu)
                        <option value="{{ $menu->id }}">{{ str_replace('-',' ',$menu->title) }}</option>
                    @endforeach
                </select>
            </div>
            <br><br>
            <div class="input-group col-12 float-left mb-4">
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fas fa-bars"></i> زبان</span>
                </div>
                <select class="form-control col-6" id="post_langs" name="lang" id="lang">
                    <option value="fa">فا</option>
                    <option value="en">En</option>
                </select>
            </div>
            <div class="col-12 mt-4 p-0 AdminSection float-right">
                <div class="col-12 header">
                    <h1>تنظیمات سئو</h1>
                </div>
                <div class="col-12 content">
                    <div class="input-group col-12 float-left mb-4">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-search"></i> کلمات کلیدی</span>
                        </div>
                        <select id="post_keywords" name="keywords[]" title="keywords" class="form-control col-6" multiple>
                            @foreach ($keywords as $keyword)
                                <option value="{{ $keyword->id }}">{{ $keyword->value }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="input-group col-12 float-left mb-4">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-robot"></i> رباتهای گوگل</span>
                        </div>
                        <select id="post_robots" name="robots" title="robots" class="form-control col-6">
                            @foreach ($robots as $robot)
                                <option value="{{ $loop->index }}">{{ $robot }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="input-group col-12 float-right mb-4">
                        <textarea required type="text" class="form-control col-12 text-right" name="description" id="description"
                        maxlength="158" placeholder="متای توضیحات گوگل">{{ old('description') }}</textarea>
                    </div>
                </div>
            </div>
            <textarea name="body" id="body" cols="30" rows="10"></textarea>
            <div class="col-12 p-0 mt-4 mb-4 float-right text-right">
                <button type="submit" id="addnewpost" class="btn btn-outline-primary">ذخیره</button>
            </div>
        </form>
    </div>
@endsection
@section('scripts')
    {{-- <script src="{{ asset('js/CollapsableGallery.js') }}"></script> --}}
    <script src="{{ asset('js/quill.js') }}"></script>
    <script>
    $('#body').hide();
    var toolbarOptions = {
		container: ['bold','italic','underline','link','image','strike',
		{'header': [1,2,3,4,5,6,false]},
		{'list': 'ordered'}, {'list': 'bullet'},
		{'script': 'sub'}, {'script': 'super'},
		{'indent': '-1'}, {'indent': '+1'},
		{'direction': 'rtl'}
		],
		'handlers': {
			// handlers object will be merged with default handlers object
			'link': function(value) {
			if (value) {
				var href = prompt('Enter the URL');
				this.quill.format('link', href);
			} else {
				this.quill.format('link', false);
			}
			},
			'image': function(value) {
				this.quill.insertEmbed(10, 'image', 'https://quilljs.com/images/cloud.png');
			}
		}
	};
	var quill = new Quill('#editor', {
		modules: {
			toolbar: toolbarOptions
		},
		theme: 'snow'
	});
    $('#addnewpost').on('click', function(event){
        event.preventDefault();
        var gb = document.querySelector('.ql-editor');
        var gl = gb.innerHTML;
        $('#body').html(gl);
        gl.innerHTML = '';
        $('form#addnewpostform').submit();
    });
    $('#post_tags,#post_menus,#post_langs,#post_keywords,#post_robots').select2();
    $(".ql-direction").click(); </script>
@endsection


{{-- <select id="product_tags" name="tags[]" title="tags" class="form-control col-6" multiple>
    @foreach ($tags as $tag)
        <option @foreach ($product->tags as $ptag)
        @if ($ptag->id == $tag->id) selected @endif
        @endforeach
        value="{{ $tag->id }}">{{ $tag->name }}</option>
    @endforeach
</select> --}}
