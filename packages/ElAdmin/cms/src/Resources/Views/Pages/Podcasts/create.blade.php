@extends('CMSV::Pages.layout')
@section('title')
    <title>ایجاد پادکست جدید</title>
@endsection
@section('pagecontent')
    <div class="col-12 p-3">
        <h1 class="text-right">افزودن پادکست</h1>
        <form action="{{ route('Admin.podcasts.store') }}" method="POST" role="form" class="col-12 mt-4 p-0">
        @csrf
            <div class="input-group col-12 col-md-6 mb-3 float-right">
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fas fa-font"></i></span>
                </div>
                <input required type="text" class="form-control text-right" name="title" id="title"
                value="{{ old('title') }}" placeholder="عنوان">
            </div>
            <div class="input-group col-12 col-md-8 float-right">
                <textarea class="form-control text-right" name="description" id="description" placeholder="توضیحات"
                cols="30" rows="10">{{ old('description') }}</textarea>
            </div>
            <div class="input-group col-12 float-left mb-4">
                <div class="col-12 p-0 mt-3 float-right text-right">
                    <button type="submit" class="btn btn-outline-primary">ذخیره</button>
                </div>
            </div>
        </form>
    </div>
@endsection