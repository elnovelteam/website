@extends('CMSV::Pages.layout')
@section('title')
    <title>لیست پادکست ها</title>
@endsection
@section('pagecontent')
    <div class="col-12 p-3 float-left text-right">
        <a href="{{ route('Admin.podcasts.create') }}" class="btn btn-sm btn-info float-right mt-2 ml-2"><i class="fas fa-plus mt-1"></i></a>
        <h1 class="float-right">پادکست ها</h1>
    </div>
    <table class="table table-bordered mt-4 text-center">
        <thead class="thead-dark">
            <tr>
                <th scope="col">#</th>
                <th scope="col">عنوان</th>
                <th scope="col">توضیحات</th>
                <th scope="col">تاریخ ایجاد</th>
                <th scope="col">ویرایش</th>
                <th scope="col">حذف</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($podcasts as $podcast)
            <tr>
                <th>{{ $loop->index + 1 }}</th>
                <td>{{ $podcast->title }}</td>
                <td>{{ $podcast->description }}</td>
                <td>{{ $podcast->created_at->diffForHumans() }}</td>
                <td><a class="btn btn-outline-info" href="{{ route('Admin.podcasts.edit',['podcast' => $podcast->id]) }}"><i class="fas fa-pencil-alt"></i></a></td>
                <td>
                    <form action="{{ route('Admin.podcasts.destroy',['podcast' => $podcast->id]) }}" method="post">
                    @method('DELETE')
                    @csrf
                    <button type="submit" class="btn btn-outline-danger"><i class="fas fa-times"></i></button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    <div class="col-12 text-center">
        {{ $podcasts->links() }}
    </div>
@endsection
