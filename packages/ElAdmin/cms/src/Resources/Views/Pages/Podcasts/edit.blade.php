@extends('CMSV::Pages.layout')
@section('title')
    <title>ویرایش پادکست {{ $podcast->name }}</title>
@endsection
@section('pagecontent')
    <div id="react-pick-files" data-target="{{ route('api.Cms.files') }}" data-api-token="{{ auth()->user()->api_token }}" class="react-file-picker"
    data-csrf={{ csrf_token() }} data-usage="select:singular" data-callback="loadPodcastFile" data-targetHTML="#file" data-filetype="audio" data-mainpage="{{ route('home') }}"
    data-main="{{ route('api.Cms.changeFile', ['method' => 'podcast', 'id' => $podcast->id, 'type' => 'file']) }}"></div>

    <div id="react-pick-images" data-target="{{ route('api.Cms.files') }}" data-api-token="{{ auth()->user()->api_token }}" class="react-file-picker"
    data-csrf={{ csrf_token() }} data-usage="select:singular" data-callback="loadPodcastPhoto" data-targetHTML="#image" data-filetype="image" data-mainpage="{{ route('home') }}"
    data-main="{{ route('api.Cms.changeFile', ['method' => 'podcast', 'id' => $podcast->id, 'type' => 'photo']) }}"></div>

    <div class="col-12 p-3 float-left">
        <h1 class="text-right">ویرایش پادکست {{ $podcast->name }}</h1>
        <p id="file">@if ($podcast->file) <a blank="_blank" href="{{ asset($podcast->file->path) }}">{{ $podcast->file->name }}</a> @endif</p>
        <p id="image">
        @if ($podcast->photo)
            <a blank="_blank" href="{{  asset($podcast->photo->file->path) }}">
            <img height="100" width="100" src="{{  asset($podcast->photo->file->thumbnail_path) }}" alt="{{ $podcast->photo->file->name }}">
            </a>
        @endif
        </p>
        <form action="{{ route('Admin.podcasts.update', ['podcast' => $podcast->id]) }}" method="POST" role="form" class="col-12 mt-4 p-0">
        @csrf
        @method("PUT")
            <div class="input-group col-12 col-md-6 mb-3 float-right">
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fas fa-font"></i></span>
                </div>
                <input required type="text" class="form-control text-right" name="title" id="title"
                value="{{ old('title') ?: $podcast->title  }}" placeholder="عنوان">
            </div>
            <div class="input-group col-12 col-md-8 mb-3 float-right">
                <textarea class="form-control text-right" name="description" id="description" placeholder="توضیحات"
                cols="30" rows="10">{{ old('description') ?: $podcast->description }}</textarea>
            </div>
            @component('CMSV::partials.seo', ['seo' => $podcast->seo]) @endcomponent
            <div class="input-group col-12 float-left mb-4">
                <div class="col-12 p-0 mt-3 float-right text-right">
                    <button type="submit" class="btn btn-outline-primary">بروزرسانی</button>
                </div>
            </div>
        </form>
    </div>
@endsection
@section('scripts')
<script src="{{ asset('js/pick.js') }}"></script>
<script>
const mainpage = "@php echo route('home') @endphp";
function loadPodcastFile(cb, th) {
    let {path, name, thumbnail_path} = cb.file;
    $(th).html(`<a target="_blank" href="${mainpage}/${path}">${name}</a>`);
}
function loadPodcastPhoto(cb, th) {
    let {path, name, thumbnail_path} = cb;
    $(th).html(`<a target="_blank" href="${mainpage}/${path}"><img height="100" width="100" src="${mainpage}/${thumbnail_path}" alt="${name}"></a>`);
}
$("#seo-keywords").select2();
</script>
@endsection