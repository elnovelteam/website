@extends('CMSV::Pages.layout')
@section('title')
    <title>ویرایش صفحه {{ $page->title }}</title>
    <link href="https://cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">
@endsection
@section('pagecontent')
    <div class="col-12 overlay-block">
        <div class="col-12 text-left float-left p-3 pl-0">
            <div class="closer float-left btn btn-danger btn-sm" id="closeparent"><i class="fas fa-times"></i></div>
        </div>
        <div class="col-12 float-left blocks-parent">
            @include('CMSV::Pages.Pages.blocks')
        </div>
    </div>
    <div class="col-12 p-3">
        <h1 class="text-right">ویرایش صفحه {{ $page->title }}</h1>
        <form action="{{ route('Admin.pages.update', ['id' => $page->id]) }}" id="addnewpost" method="POST" role="form" enctype="multipart/form-data"
        class="col-12 mt-4 p-0">
        @csrf
            <div class="input-group col-12 col-sm-6 float-right">
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fas fa-font"></i></span>
                </div>
                <input required type="text" class="form-control text-right" name="title" id="title"
                value="{{ (old('title')) ? old('title') : $page->title }}" placeholder="عنوان صفحه">
            </div>
            <div class="input-group col-12 p-3 float-left">
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fas fa-language"></i></span>
                </div>
                <select class="form-control col-5 float-right" name="lang" id="lang">
                    <option value="fa">فا</option>
                    <option value="en">En</option>
                </select>
            </div>
            <div class="input-group col-12 float-left mb-4">
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fas fa-tag"></i> برچسب ها</span>
                </div>
                <select id="post_tags" name="tags[]" title="tags" class="form-control col-5" multiple>
                    @foreach ($tags as $tag)
                        <option value="{{ $tag->id }}">{{ str_replace('_',' ',$tag->name) }}</option>
                    @endforeach
                </select>
            </div>
            <div class="input-group col-8 float-right mb-4">
				<div class="col-12 text-right" id="toolbar"></div>
				<div class="col-12" id="editor"></div>
            </div>
            <div class="input-group AdminSection col-12 p-0 float-left">
                <div class="col-12 header">
                    <h1>صفحه ساز</h1>
                </div>
                <div class="col-12 content">
                    <div class="col-12 float-left pagemaker">
                        <div class="col-8 offset-2 float-left control-panel">
                            <button type="button" id="addSection" class="col-12 btn btn-outline-secondary">افزودن بخش</button>
                        </div>
                        <div class="col-12 float-left environment" id="wsh">
                            <div class="wsh-desk p-3 wsh-sortable"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="input-group col-12 float-left mb-4">
                <div class="col-12 p-0 mt-3 float-right text-right">
                    <button class="btn btn-outline-primary">ذخیره</button>
                </div>
            </div>
        </form>
    </div>
@endsection
@section('scripts')
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="{{ asset('js/quill.js') }}"></script>
    <script src="{{ asset('js/pagebuilder.js') }}"></script>
    <script>
    var toolbarOptions = {
		container: ['bold','italic','underline','link','image','strike',
		{'header': [1,2,3,4,5,6,false]},
		{'list': 'ordered'}, {'list': 'bullet'},
		{'script': 'sub'}, {'script': 'super'},
		{'indent': '-1'}, {'indent': '+1'},
		{'direction': 'rtl'}
		],
		'handlers': {
			'link': function(value) {
			if (value) {
				var href = prompt('Enter the URL');
				this.quill.format('link', href);
			} else {
				this.quill.format('link', false);
			}
			},
			'image': function(value) {
				this.quill.insertEmbed(10, 'image', 'https://quilljs.com/images/cloud.png');
			}
		}
	};
	var quill = new Quill('#editor', {
		modules: {
			toolbar: toolbarOptions
		},
		theme: 'snow'
	});
    $('#addnewpost').on('submit', function(event){
        event.preventDefault();
        $('.ql-editor').attr('name', 'body');
    });
    $('#post_tags').select2();
    $(".ql-direction").click(); </script>
@endsection
