<div class="block col-lg-2 col-md-4 col-6">
    <div id="addEmptyBlock">
        <div class="col-12 header">
            <i class="fas fa-3x fa-columns"></i>
        </div>
        <div class="col-12 title">
            <p>بلاک خالی</p>
        </div>
    </div>
</div>
<div class="block col-lg-2 col-md-4 col-6">
    <div id="addClassicTitle">
        <div class="col-12 header">
            <i class="fas fa-3x fa-crown"></i>
        </div>
        <div class="col-12 title">
            <p>عنوان کلاسیک</p>
        </div>
    </div>
</div>
<div class="block col-lg-2 col-md-4 col-6">
    <div id="addFantasyTitle">
        <div class="col-12 header">
            <i class="fas fa-3x fa-heading"></i>
        </div>
        <div class="col-12 title">
            <p>عنوان فانتزی</p>
        </div>
    </div>
</div>
