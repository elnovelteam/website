@extends('CMSV::Pages.layout')
@section('title')
    <title>لیست صفحات</title>
@endsection
@section('pagecontent')
    <div class="col-12 p-3 float-left text-right">
        <a href="{{ route('Admin.pages.create') }}" class="btn btn-info float-right mt-2 ml-2">+</a>
        <h1 class="float-right">صفحات</h1>
    </div>
    <table class="table mt-4 text-center">
        <thead class="thead-dark">
            <tr>
                <th scope="col">ردیف</th>
                <th scope="col">عنوان</th>
                <th scope="col">کلمات کلیدی</th>
                <th scope="col">متای توضیحات</th>
                <th scope="col">ویرایش</th>
                <th scope="col">حذف</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($pages as $page)
            <tr>
                <th scope="row">{{ $loop->index + 1 }}</th>
                <td>{{ $page->title }}</td>
                <td>
                    @foreach ($page->seodata->keywords as $keyword)
                        {{ $keyword->value }} @if (($loop->index + 1) < count($page->seodata->keywords)) {{ "," }} @endif
                    @endforeach
                </td>
                <td>{{ $page->seodata->description }}</td>
                <td><a class="btn btn-outline-info" href="{{ route('Admin.pages.edit',['id' => $page->id]) }}"><i class="fas fa-pencil-alt"></i></a></td>
                <td>
                    <form action="{{ route('Admin.pages.destroy',['id' => $page->id]) }}" method="post">
                    @method('DELETE')
                    @csrf
                    <button type="submit" class="btn btn-outline-danger"><i class="fas fa-times"></i></button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    <div class="col-12 text-center">
        {{ $pages->links() }}
    </div>
@endsection
