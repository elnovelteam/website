@extends('CMSV::Pages.layout')
@section('title')
    <title>اضافه کردن کلمه کلیدی</title>
@endsection
@section('pagecontent')
    <div class="col-12 p-3">
        <h1 class="text-right text-right rtl">اضافه کردن کلمه کلیدی</h1>
        <form action="{{ route('Admin.keywords.store') }}" method="POST" role="form" class="col-12 mt-4 p-0">
        @csrf
            <div class="input-group col-12 col-sm-6 mb-4">
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fas fa-font"></i></span>
                </div>
                <input required type="text" class="form-control text-right" name="value" id="name"
                value="{{ old('value') }}" placeholder="نام کلمه کلیدی">
            </div>
            <div class="input-group col-12 float-left mb-4">
                <div class="col-12 p-0 mt-3 float-right text-right">
                    <button type="submit" class="btn btn-outline-primary">ذخیره</button>
                </div>
            </div>
        </form>
    </div>
@endsection
