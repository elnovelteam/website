@extends('CMSV::Pages.layout')
@section('title')
    <title>فهرست کلمات کلیدی</title>
@endsection
@section('pagecontent')
    <div class="col-12 float-left p-3">
         <a href="{{ route('Admin.keywords.create') }}" class="btn btn-info float-right mt-2 ml-2">+</a>
        <h1 class="float-right text-right">کلمات کلیدی</h1>
    </div>
    <table class="table mt-4 text-center">
        <thead class="thead-dark">
            <tr>
                <th scope="col">ردیف</th>
                <th scope="col">کلمه کلیدی</th>
                <th scope="col">ویرایش</th>
                <th scope="col">حذف</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($keywords as $keyword)
            <tr>
                <th>{{ $loop->index + 1 }}</th>
                <td>{{ $keyword->value }}</td>
                <td><a class="btn btn-outline-info" href="{{ route('Admin.keywords.edit',['keyword' => $keyword->id]) }}"><i class="fas fa-pencil-alt"></i></a></td>
                <td>
                    <form action="{{ route('Admin.keywords.destroy',['keyword' => $keyword->id]) }}" method="post">
                    @method('DELETE')
                    @csrf
                    <button type="submit" class="btn btn-outline-danger"><i class="fas fa-times"></i></button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    <div class="col-12 text-center">
        {{ $keywords->links() }}
    </div>
@endsection
