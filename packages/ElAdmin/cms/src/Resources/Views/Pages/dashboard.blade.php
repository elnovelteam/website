@extends('CMSV::Pages.layout')
@section('title')
    <title>@lang('CMSL::admin.eladmin') - @lang('CMSL::admin.cms')</title>
@endsection
@section('pagecontent')
    <div class="col-12 text-right p-3">
        <h1>@lang('CMSL::admin.welcome')</h1>
    </div>
@endsection
