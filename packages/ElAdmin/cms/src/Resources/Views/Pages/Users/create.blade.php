@extends('CMSV::Pages.layout')
@section('title')
    <title>ایجاد کاربر جدید</title>
@endsection
@section('pagecontent')
    <div class="col-12 p-3">
        <h1 class="col-12 text-right rtl">ایجاد کاربر جدید</h1>
        <form action="{{ route('Admin.users.store') }}" method="POST" role="form" enctype="multipart/form-data" class="col-12 mt-4 p-0">
        @csrf
        <div class="input-group col-12 col-sm-6 mb-4">
            <div class="input-group-prepend">
                <span class="input-group-text"><i class="fas fa-font"></i></span>
            </div>
            <input required type="text" class="form-control" name="name" id="name"
            value="{{ old('name') }}" placeholder="@lang('auth.name')">
        </div>
        <div class="input-group col-12 col-sm-6 mb-4">
            <div class="input-group-prepend">
                <span class="input-group-text"><i class="fas fa-at"></i></span>
            </div>
            <input required type="text" class="form-control" name="email" id="email"
            value="{{ old('email') }}" placeholder="@lang('auth.email')">
        </div>
        <div class="input-group col-12 mb-4">
            <div class="input-group-prepend">
                <span class="input-group-text"><i class="fas fa-user-tag">Roles</i></span>
            </div>
            <select id="role_types" multiple name="roles[]" title="role" class="form-control col-6">
                @foreach ($roles as $role)
                    <option value="{{ $role->id }}">{{ ucfirst($role->label) }}</option>
                @endforeach
            </select>
        </div>
        <div class="input-group col-12 mt-3">
            <button type="submit" name="createbanner" class="btn btn-outline-primary float-right">ثبت</button>
        </div>
        </form>
    </div>
@endsection
@section('scripts')
    <script>
    $('#role_types').select2(); </script>
@endsection

