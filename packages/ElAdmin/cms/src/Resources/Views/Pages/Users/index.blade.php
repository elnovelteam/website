@extends('CMSV::Pages.layout')
@section('title')
    <title>Users list</title>
@endsection
@section('pagecontent')
    <div class="col-12 float-left p-3">
        <a href="{{ route('Admin.users.create') }}" class="btn btn-info float-right mt-2 ml-2">+</a>
        <h1 class="float-right text-right">فهرست کاربران</h1>
    </div>
    <table class="table table-bordered mt-4 text-center">
        <thead class="thead-dark">
            <tr>
                <th scope="col">#</th>
                <th scope="col">نام</th>
                <th scope="col">نقش</th>
                <th scope="col">سن</th>
                <th scope="col">جنسیت</th>
                <th scope="col">ایمیل</th>
                <th scope="col">تلفن</th>
                <th scope="col">ویرایش</th>
                <th scope="col">حذف</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($users as $user)
            <tr>
                <th>{{ $loop->index + 1 }}</th>
                <th>{{ $user->name }}</th>
                <td>
                    @foreach ($user->roles as $role)
                        {{ $role->label }}
                    @endforeach
                </td>
                <th>{{ $user->birthdate->age }}</th>
                <td>{!! determine_sex($user->sex) !!}</td>
                <td>{{ $user->email }}</td>
                <td>{{ $user->phone_number }}</td>
                <td><a class="btn btn-outline-info" href="{{ route('Admin.users.edit',['user' => $user->id]) }}"><i class="fas fa-pencil-alt"></i></a></td>
                <td>
                    <form action="{{ route('Admin.users.destroy',['user' => $user->id]) }}" method="post">
                    @method('DELETE')
                    @csrf
                    <button type="submit" class="btn btn-outline-danger"><i class="fas fa-times"></i></button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    <div class="col-12 text-center">
        {{ $users->links() }}
    </div>
@endsection
