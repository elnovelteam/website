@extends('CMSV::Pages.layout')
@section('title')
    <title>ویرایش کاربر {{ $user->name }}</title>
@endsection
@section('pagecontent')
    <div class="col-12 p-3">
        <h1 class="col-12 text-right rtl">ویرایش کاربر {{ $user->name }}</h1>
        <form action="{{ route('Admin.users.update', ['user' => $user->id]) }}" method="POST" role="form" enctype="multipart/form-data"
        class="col-12 mt-4 p-0">
        @csrf
        @method('PATCH')
        <div class="input-group col-12 col-sm-6 mb-4">
            <div class="input-group-prepend">
                <span class="input-group-text"><i class="fas fa-font"></i></span>
            </div>
            <input required type="text" class="form-control" name="name" id="name"
            value="{{ (old('name')) ? old('name') : $user->name }}" placeholder="Username">
        </div>
        <div class="input-group col-12 col-sm-6 mb-4">
            <div class="input-group-prepend">
                <span class="input-group-text"><i class="fas fa-at"></i></span>
            </div>
            <input required type="text" class="form-control" name="email" id="email"
            value="{{ (old('email')) ? old('email') : $user->email }}" placeholder="Country name">
        </div>
        <div class="input-group col-12 mb-4">
            <div class="input-group-prepend">
                <span class="input-group-text"><i class="fas fa-user-tag">Roles</i></span>
            </div>
            <select id="role_types" multiple name="roles[]" title="role" class="form-control col-6">
                @foreach ($roles as $role)
                    <option @if ($role->id == $user->hasRole($role->name)) selected @endif
                    value="{{ $role->id }}">{{ ucfirst($role->label) }}</option>
                @endforeach
            </select>
        </div>
        <div class="input-group col-12 mt-3">
            <button type="submit" name="createbanner" class="btn btn-outline-primary float-right">بروزرسانی</button>
        </div>
        </form>
    </div>
@endsection
@section('scripts')
    <script>  $('#role_types').select2(); </script>
@endsection

