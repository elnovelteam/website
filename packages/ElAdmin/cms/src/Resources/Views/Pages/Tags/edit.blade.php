@extends('CMSV::Pages.layout')
@section('title')
    <title>ویرایش دسته بندی : {{ str_replace('_',' ',$tag->name) }}</title>
@endsection
@section('pagecontent')
    <div class="col-12 p-3">
    <h1 class="text-right">ویرایش دسته بندی : {{ str_replace('_',' ',$tag->name) }}</h1>
    <form action="{{ route('Admin.tags.update', ['tag' => $tag->id]) }}" method="POST" role="form" enctype="multipart/form-data"
    class="col-12 col-sm-10 col-lg-8 mt-4 p-0">
    @csrf
    @method('PATCH')
    <div class="input-group col-12 col-sm-6 float-right mb-4">
        <div class="input-group-prepend">
            <span class="input-group-text"><i class="fas fa-font"></i></span>
        </div>
        <input required type="text" class="form-control" name="name" id="name"
        value="{{ (old('name')) ? old('name') : str_replace('_',' ',$tag->name) }}" placeholder="Tag name">
    </div>
    <div class="input-group col-12 col-sm-6 m-0 float-right">
        <button type="submit" name="createbanner" class="btn btn-outline-primary">بروزرسانی</button>
    </div>
    </form>
    </div>
@endsection
