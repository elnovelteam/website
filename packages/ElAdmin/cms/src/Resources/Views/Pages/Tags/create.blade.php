@extends('CMSV::Pages.layout')
@section('title')
    <title>اضافه کردن برچسب</title>
@endsection
@section('pagecontent')
    <div class="col-12 p-3">
        <h1 class="text-right">اضافه کردن برچسب</h1>
        <form action="{{ route('Admin.tags.store') }}" id="addnewpost" method="POST" role="form" enctype="multipart/form-data"
        class="col-12 col-sm-10 col-lg-8 mt-4 p-0">
        @csrf
            <div class="input-group col-12 col-sm-6 float-right mb-4">
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fas fa-font"></i></span>
                </div>
                <input required type="text" class="form-control text-right" name="name" id="name"
                value="{{ old('name') }}" placeholder="نام برچسب">
            </div>
            <div class="input-group col-12 float-left mb-4">
                <div class="col-12 p-0 mt-3 float-right text-right">
                    <button type="submit" class="btn btn-outline-primary">ذخیره</button>
                </div>
            </div>
        </form>
    </div>
@endsection
