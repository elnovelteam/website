
<div class="col-12 float-left seo-form p-3">
    <h3 class="text-center mb-4">تنظیمات سئو</h3>
    @if ($seo)
    <div class="input-group col-12 col-md-8 mb-4 float-right">
        <textarea class="form-control text-right" name="seo_description" id="seo-description" maxlength="160"
        placeholder="متا سئو" rows="5">{{ $seo->description }}</textarea>
    </div>
    <div class="input-group col-12 col-md-8 mb-4 float-right">
        <div class="input-group-prepend">
            <span class="input-group-text"><i class="fas fa-hashtag"></i></span>
        </div>
        <input class="form-control col-8" type="number" name="seo_priority" value="{{ old('seo_priority') ?: $seo->priority }}" step="0.1" min="0.1" max="1" id="seo_priority">
    </div>
    <div class="input-group col-12 col-md-8 mb-4 float-right">
        <div class="input-group-prepend">
            <span class="input-group-text">اهمیت</span>
        </div>
        <select class="form-control col-8" name="seo_change" id="seo-change">
            @foreach (changefreq() as $change)
                <option @if($change == $seo->changefreq) selected @endif value="{{ $change }}">{{ $change }}</option>
            @endforeach
        </select>
    </div>
    <div class="input-group col-12 col-md-8 mb-4 float-right">
        <div class="input-group-prepend">
            <span class="input-group-text">ربات های جستجو</span>
        </div>
        <select class="form-control col-8" name="seo_robots" id="seo-robots">
            @foreach (robots() as $robot)
                <option @if($robot == $seo->robots) selected @endif value="{{ $loop->index }}">{{ $robot }}</option>
            @endforeach
        </select>
    </div>
    <div class="input-group col-12 col-md-8 mb-4 float-right">
        <div class="input-group-prepend">
            <span class="input-group-text">کلمات کلیدی</span>
        </div>
        <select class="form-control col-8" name="seo_keywords[]" id="seo-keywords" multiple>
        @foreach (keywords() as $keyword)
            <option @foreach ($seo->keywords as $kw)
            @if($kw->id == $keyword->id) selected @endif
            @endforeach value="{{ $keyword->id }}">{{ $keyword->value }}</option>
        @endforeach
        </select>
    </div>
    @else
    <div class="input-group col-12 col-md-8 mb-4 float-right">
        <textarea class="form-control text-right" name="seo_description" id="seo-description" maxlength="160"
        placeholder="متا سئو" rows="5">{{ old('seo_description') }}</textarea>
    </div>
    <div class="input-group col-12 col-md-8 mb-4 float-right">
        <div class="input-group-prepend">
            <span class="input-group-text"><i class="fas fa-hashtag"></i></span>
        </div>
        <input class="form-control col-8" type="number" name="seo_priority" value="{{ old('seo_priority') ?: 0.1 }}" step="0.1" min="0.1" max="1" id="seo_priority">
    </div>
    <div class="input-group col-12 col-md-8 mb-4 float-right">
        <div class="input-group-prepend">
            <span class="input-group-text">اهمیت</span>
        </div>
        <select class="form-control col-8" name="seo_change" id="seo-change">
            @foreach (changefreq() as $change)
                <option value="{{ $change }}">{{ $change }}</option>
            @endforeach
        </select>
    </div>
    <div class="input-group col-12 col-md-8 mb-4 float-right">
        <div class="input-group-prepend">
            <span class="input-group-text">ربات های جستجو</span>
        </div>
        <select class="form-control col-8" name="seo_robots" id="seo-robots">
            @foreach (robots() as $robot)
                <option value="{{ $loop->index }}">{{ $robot }}</option>
            @endforeach
        </select>
    </div>
    <div class="input-group col-12 col-md-8 mb-4 float-right">
        <div class="input-group-prepend">
            <span class="input-group-text">کلمات کلیدی</span>
        </div>
        <select class="form-control col-8" name="seo_keywords[]" id="seo-keywords" multiple>
        @foreach (keywords() as $keyword)
            <option value="{{ $keyword->id }}">{{ $keyword->value }}</option>
        @endforeach
        </select>
    </div>
    @endif
</div>

{{--
<div class="input-group col-12 col-md-6 mb-3 float-right">
    <div class="input-group-prepend">
        <span class="input-group-text"><i class="fas fa-font"></i></span>
    </div>
    <input required type="text" class="form-control text-right" name="title" id="title"
    value="{{ old('title') ?: $podcast->title  }}" placeholder="عنوان">
</div>
--}}
{{-- <div class="input-group col-12 col-md-8 float-right">
    <textarea class="form-control text-right" name="description" id="description" placeholder="توضیحات"
    cols="30" rows="10">{{ old('description') ?: $podcast->description }}</textarea>
</div>
<div class="input-group col-12 float-left mb-4">
    <div class="col-12 p-0 mt-3 float-right text-right">
        <button type="submit" class="btn btn-outline-primary">بروزرسانی</button>
    </div>
</div> --}}