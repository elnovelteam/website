<?php

return [
    'welcome' => 'خوش آمدید',
    'eladmin' => 'ال ادمین',
    'cms' => 'پنل مدیریت محتوا',
    'dashboard' => 'داشبورد',
    'text' => 'متن',
    'information' => 'اطلاعات',
    'menus' => 'منوها',
    'sliders' => 'اسلایدر ها',
    'users' => 'کاربر ها',
    'gallery' => 'گالری',
    'logout' => 'خروچ',
    'podcasts' => 'پادکست ها',
    'files' => 'فایل ها',
    'tickets' => 'تیکت ها',
    'writers' => 'داستان نویسان',
    'writers-list' => 'فهرست داستان نویسان',
    'writers-request' => 'درخواست های داستان نویسی',
    'golden-writers' => 'قلم طلایی ها'
];
