<?php

return [
    'settings' => 'تنظیمات',
    'form' => [
        'username'  => 'نام کاربری',
        'firstname' => 'نام',
        'lastname'  => 'نام خانوادگی',
        'nickname'  => 'تخلص',
        'bio'       => 'بیوگرافی',
        'email' => 'آدرس ایمیل',
        'birthdate' => 'تاریخ تولد',
        'sex' => 'جنسیت',
        'country' => 'کشور',
        'state' => 'استان',
        'phone_number' => 'شماره تلفن',
        'message' => 'پیام شما',
        'title' => 'عنوان',
        'question' => 'سوال',
        'admin_rate' => 'امتیاز ادمین'
    ],
    'ticket' => [
        'OPEN' => 'در جریان',
        'CLOSED' => 'بسته شده',
        'INPROGRESS' => 'در حال پیگیری',
        '1' => 'زیاد',
        '2' => 'متوسط',
        '3' => 'کم'
    ]
];