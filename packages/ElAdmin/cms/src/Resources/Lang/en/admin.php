<?php

return [
    'welcome' => 'Welcome',
    'eladmin' => 'ElAdmin',
    'cms' => 'Content Management System',
    'dashboard' => 'Dashboard',
    'text' => 'Text',
    'information' => 'Information',
    'menus' => 'Menus',
    'sliders' => 'Sliders',
    'users' => 'Users',
    'gallery' => 'Gallery',
    'logout' => 'Logout'
];
