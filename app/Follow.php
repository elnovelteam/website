<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Follow extends Model
{
    protected $fillable = ['user_id'];
    // protected $appends  = ['followed_by_user'];
    protected $hidden   = ['followable_type', 'followable_id', 'id', 'user_id'];
    public $timestamps = false;
    public function followable()
    {
        return $this->morphTo()->select(['id', 'name']);
    }
    public function user()
    {
        return $this->belongsTo(User::class)->select(['id', 'name']);
    }
    public function FollowedByUser($target_id = false, $user = false) {
        $target_id = $target_id ?: $this->user_id;
        $target = $user ?: $this->user;
        return $target->followers()->where(['user_id' => auth()->user()->id, 'followable_id' => $target_id])->first() ? true : false;
    }
}
