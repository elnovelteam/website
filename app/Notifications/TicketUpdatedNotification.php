<?php

namespace App\Notifications;

use App\Ticket;
use App\TicketMessage;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class TicketUpdatedNotification extends Notification implements ShouldQueue
{
    use Queueable;
    public $ticket;
    public $driver;
    public $message;
    public $tries = 1;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Ticket $ticket, TicketMessage $message, $driver = ['mail', 'database'])
    {
        $this->message = $message;
        $this->ticket = $ticket;
        $this->driver = (gettype($driver) == 'array') ? $driver : [$driver];
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return $this->driver;
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $this->ticket->load(['section', 'user']);
        return (new MailMessage)
            ->subject('پاسخ جدید به درخواست پشتیبانی')
            ->greeting($this->ticket->user->name . ' عزیز سلام ')
            ->line('پاسخ جدیدی روی درخواست پشتیبانی شما ارسال شده است')
            ->line('بخش : ' . "{$this->ticket->section->name} {$this->ticket->section->description}")
            ->line('موضوع : ' . $this->ticket->title)
            ->line('پاسخ به شرح زیر است : ')
            ->line($this->message->text)
            ->line('برای مشاهده روی دکمه زیر کلیک کنید')
            ->action('مشاهده تیکت', route('userarea.tickets.show', ['ticket' => $this->ticket->id]));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
