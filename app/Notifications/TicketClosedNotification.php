<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Lang;

class TicketClosedNotification extends Notification implements ShouldQueue
{
    use Queueable;
    private $driver;
    public $ticket;
    public $tries = 1;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(\App\Ticket $ticket, $driver = ['mail', 'database'])
    {
        $ticket->load(['section', 'user']);
        $this->ticket = $ticket;
        $this->driver = (gettype($driver) == 'array') ? $driver : [$driver];
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return $this->driver;
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $url = url(route('userarea.tickets.show', ['ticket' => $this->ticket->id]));
        return (new MailMessage)
            ->subject('بسته شدن درخواست پشتیبانی')
            ->greeting($this->ticket->user->name . ' عزیز سلام ')
            ->line("درخواست پشتیبانی شما که مشخصات آن به شرح زیر می باشد به دلیل عدم ارسال پاسخ در 72 ساعت گذشته بسته شد .")
            ->line("شماره درخواست : {$this->ticket->code}")
            ->line("موضوع : {$this->ticket->title}")
            ->line("واحد مربوطه : {$this->ticket->section->name} {$this->ticket->section->description}")
            ->line("اهمیت : " . Lang::get('ticket.'.$this->ticket->force_rate))
            ->line("میتوانید با استفاده از دکمه زیر درخواست پشتیبانی خود را مشاهده کنید و خواهشمند است به این درخواست پشتیبانی در همان صفحه پاسخ بدهید")
            ->action('مشاهده تیکت', $url)
            ->line("اگر مشکل شما به هر دلیلی رفع نشده است میتوانید درخواست پشتیبانی جدیدی باز کرده و شماره این درخواست را در آن ذکر کنید تا همکاران ما سریعا پاسخگوی شما باشند.");
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
