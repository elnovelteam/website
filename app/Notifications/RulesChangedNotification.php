<?php

namespace App\Notifications;

// use Illuminate\Bus\Queueable;
// use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class RulesChangedNotification extends Notification
{
    // use Queueable;
    private $details;
    private $driver;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($details, $driver = ['mail', 'database'])
    {
        $this->details = $details;
        $this->driver = (gettype($driver) == 'array') ? $driver : [$driver];
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return $this->driver;
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('Our Rules changed successfully !');
    }
    public function toSms($notifiable)
    {
        return [
            'titles' => $this->details->title,
            'texts' => $this->details->text,
            'times' => $this->details->time,
            'visits' => $this->details->link
        ];
    }
    public function toDatabase()
    {
        return [
            'title' => $this->details->title,
            'text' => $this->details->text,
            'time' => $this->details->time,
            'visit' => $this->details->link
        ];
    }
    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray()
    {
        return [
            'title' => $this->details->title,
            'text' => $this->details->text,
            'time' => $this->details->time,
            'visit' => $this->details->link
        ];
    }
}
