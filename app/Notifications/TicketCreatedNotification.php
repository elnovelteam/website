<?php

namespace App\Notifications;

use App\Ticket;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Lang;
use NotificationChannels\Telegram\TelegramMessage;

class TicketCreatedNotification extends Notification implements ShouldQueue
{
    use Queueable;
    private $driver;
    public $ticket;
    public $tries = 1;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Ticket $ticket, $driver = ['mail', 'database'])
    {
        $ticket->load(['section', 'user']);
        $this->ticket = $ticket;
        $this->driver = (gettype($driver) == 'array') ? $driver : [$driver];
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return $this->driver;
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $url = url(route('userarea.tickets.show', ['ticket' => $this->ticket->id]));
        return (new MailMessage)
            ->subject('درخواست پشتیبانی جدید')
            ->greeting($this->ticket->user->name . ' عزیز سلام ')
            ->line('شما درخواست پشتیبانی جدیدی در وب سایت ما ارسال کرده اید.')
            ->line('به محض اینکه پاسخ جدیدی روی این تیکت ارسال شود ایمیل اطلاع رسانی از سمت ما دریافت خواهید کرد.')
            ->line("درخواست پشتیبانی شما به شرح زیر است : ")
            ->line(" به بخش : " . "{$this->ticket->section->name} {$this->ticket->section->description}")
            ->line(" موضوع : "  . $this->ticket->title)
            ->line(" سوال : "   . $this->ticket->question)
            ->line(" ساعت درخواست : " . $this->ticket->created_at->format('H:i:s'))
            ->line(" اولویت بررسی : " . Lang::get('ticket.'.$this->ticket->force_rate))
            ->action('مشاهده تیکت', $url);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return $this->ticket->toArray();
    }
    public function toTelegram($notifiable)
    {
        // $url = url(route('Admin.tickets.show', ['ticket' => $this->ticket->id]));
        $url = 'http://google.com';
        return TelegramMessage::create()
            ->to(-1001470939381) //
            ->content(
                "🙋‍♂️ درخواست پشتیبانی جدید 🙋‍♂️"
                . "\n🏢 بخش : " . "{$this->ticket->section->name} {$this->ticket->section->description}"
                . "\n📄 موضوع : " . "{$this->ticket->title}"
                . "\n🔴 اولویت بررسی : " . Lang::get('ticket.'.$this->ticket->force_rate)
                . "\n🖐 نام کاربری : " . $this->ticket->user->name
                . "\n📧 ایمیل : " . $this->ticket->user->email
                . "\n❓سوال : " . $this->ticket->question
                . "\n🕒 ساعت درخواست : " . $this->ticket->created_at->format('H:i:s')
                . "\n☎️ تلفن : " . "+98{$this->ticket->user->phone_number}"
                . "\nارسال شده توسط @elnovel\\_bot 🤖"
            )
            ->button('مشاهده', $url);
    }
}
