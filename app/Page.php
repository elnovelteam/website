<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    public function tags() {
        return $this->belongsToMany(Tag::class);
    }
    public function comments() {
        return $this->morphMany(Comment::class, 'commentable');
    }
    public function likes() {
        return $this->morphMany(Like::class, 'likeable');
    }
    public function photo() {
        return $this->morphOne(Photo::class, 'photoable');
    }
    public function category() {
        return $this->morphMany(Category::class, 'categorable');
    }
    public function bookmark() {
        return $this->morphMany(Bookmark::class, 'bookmarkable');
    }
}
