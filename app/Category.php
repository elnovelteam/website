<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public function categorable() {
        return $this->morphTo();
    }
    public function info() {
        return $this->belongsTo(Maincategory::class, 'cat_id');
    }
}
