<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Comment extends Model
{
    protected $fillable = ['body','user_id','active'];
    protected $appends  = ['liked_by_user'];
    public function commentable() {
        return $this->morphTo();
    }
    public function user() {
        return $this->belongsTo(User::class);
    }
    public function likes() {
        return $this->morphMany(Like::class, 'likeable');
    }
    public function replies() {
        return $this->morphMany(self::class, 'commentable');
    }
    public function getLikedByUserAttribute() {
        $user = request()->user('web') ?: request()->user('api2');
        if ($user) {
            $userId = $user->id;
            $like = $this->likes->first(function ($q) use ($userId) {
                return $q->user_id === $userId;
            });
            return $like ? true : false;
        }
        return null;
    }
}
