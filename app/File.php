<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\File as Document;

class File extends Model
{
    protected $fillable = ['path','name','thumbnail_path'];
    protected $baseDir;
    protected $thumbnailPath;
    protected $file_name;
    protected $path;
    public function photos() {
        return $this->hasMany(Photo::class);
    }
    public function podcasts() {
        return $this->hasMany(Podcast::class);
    }
    public static function process(UploadedFile $file, $type)
    {
        $document = new static;
        $document->file = $file;
        $document->baseDir = $type . 's';
        $document->path = $document->baseDir . '/' . $document->fileName();
        $document->thumbnailPath = $document->thumbPath();
        $document->fill([
            'name'          => $document->file_name,
            'path'          => $document->path,
            'thumbnail_path' => $document->thumbnailPath
        ]);
        return $document;
    }
    public function fileName()
    {
        $filename = pathinfo($this->file->getClientOriginalName(), PATHINFO_FILENAME);
        $ext  = $this->file->getClientOriginalExtension();
        $this->file_name = $filename;
        return "{$filename}.{$ext}";
    }
    public function thumbPath()
    {
        $thumbnail = $this->baseDir . DIRECTORY_SEPARATOR . 'tn-' . $this->fileName();
        return ($this->baseDir == 'images') ?  $thumbnail : '';
    }
    public function Upload()
    {
        $this->file->move($this->baseDir, $this->path);
        if ($this->baseDir == 'images') {
            $this->makeThumbnail();
        }
        return $this;
    }
    public function makeThumbnail()
    {
        Image::make($this->path)
        ->fit(150)->save($this->thumbnailPath);
        return $this;
    }
    public function delete()
    {
        parent::delete();
        Document::delete([
            $this->path,
            $this->thumbnail_path,
        ]);
    }
}
