<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuizQuestion extends Model
{
    protected $hidden = ['created_at', 'updated_at', 'scoreable'];
    public function options()
    {
        return $this->hasMany(QuizOption::class);
    }
}
