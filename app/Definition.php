<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Definition extends Model
{
    public function users () {
        return $this->hasMany(Writer::class);
    }
}
