<?php

namespace App\Policies;

use App\Book;
use App\Writer;
use Illuminate\Auth\Access\HandlesAuthorization;

class BookPolicy
{
    use HandlesAuthorization;

    public function edit(Writer $writer,Book $book)
    {
        return $writer->id == $book->writer_id;
    }
}
