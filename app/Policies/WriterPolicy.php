<?php

namespace App\Policies;

use App\User;
use App\Writer;
use Illuminate\Auth\Access\HandlesAuthorization;

class WriterPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any writers.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the writer.
     *
     * @param  \App\User  $user
     * @param  \App\Writer  $writer
     * @return mixed
     */
    public function view(User $user, Writer $writer)
    {
        return $writer->user_id == $user->id;
    }

    /**
     * Determine whether the user can create writers.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the writer.
     *
     * @param  \App\User  $user
     * @param  \App\Writer  $writer
     * @return mixed
     */
    public function update(User $user, Writer $writer)
    {
        //
    }

    /**
     * Determine whether the user can delete the writer.
     *
     * @param  \App\User  $user
     * @param  \App\Writer  $writer
     * @return mixed
     */
    public function delete(User $user, Writer $writer)
    {
        //
    }

    /**
     * Determine whether the user can restore the writer.
     *
     * @param  \App\User  $user
     * @param  \App\Writer  $writer
     * @return mixed
     */
    public function restore(User $user, Writer $writer)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the writer.
     *
     * @param  \App\User  $user
     * @param  \App\Writer  $writer
     * @return mixed
     */
    public function forceDelete(User $user, Writer $writer)
    {
        //
    }
}
