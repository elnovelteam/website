<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;

class Post extends Model
{
    use SearchableTrait;
    protected $searchable = [
        'columns' => [
            'title' => 10,
            'summary'=> 5,
        ],
    ];
    public function tags() {
        return $this->belongsToMany(Tag::class);
    }
    public function comments() {
        return $this->morphMany(Comment::class, 'commentable');
    }
    public function likes() {
        return $this->morphMany(Like::class, 'likeable');
    }
    public function categories() {
        return $this->morphMany(Category::class, 'categorable');
    }
    public function bookmarks() {
        return $this->morphMany(Bookmark::class, 'bookmarkable');
    }
    public function seo() {
        return $this->morphOne(SeoInformation::class, 'seoable');
    }
    public function photo() {
        return $this->morphOne(Photo::class, 'photoable');
    }
}
