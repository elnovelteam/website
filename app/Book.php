<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;

class Book extends Model
{
    use SearchableTrait;
    protected $searchable = [
        'columns' => [
            'books.title' => 10,
            'books.summary'=> 5,
            'writers.nickname' => 8,
        ],
        'joins' => [
            'writers' => ['writers.id', 'books.writer_id'],
        ]
    ];
    public function serie() {
        return $this->belongsTo(Serie::class);
    }
    public function versions() {
        return $this->hasMany(Version::class);
    }
    public function writer() {
        return $this->belongsTo(Writer::class);
    }
    public function tags() {
        return $this->belongsToMany(Tag::class);
    }
    public function comments() {
        return $this->morphMany(Comment::class, 'commentable');
    }
    public function likes() {
        return $this->morphMany(Like::class, 'likeable');
    }
    public function photo() {
        return $this->morphOne(Photo::class, 'photoable');
    }
    public function category() {
        return $this->morphMany(Category::class, 'categorable');
    }
    public function bookmark() {
        return $this->morphMany(Bookmark::class, 'bookmarkable');
    }
}
