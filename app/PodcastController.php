<?php

namespace ElAdmin\Cms\Http\Controllers;

use Illuminate\Http\Request;
use App\Podcast;

class PodcastController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $podcasts = Podcast::with(['file', 'photo.file'])->paginate(12);
        return view('CMSV::Pages.Podcasts.index', compact('podcasts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		return view('CMSV::Pages.Podcasts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
            'title.required' => 'فیلد عنوان اجباری است',
            'title.min' => 'حداقل سه کاراکتر برای فیلد عنوان پر کنید',
            'title.max' => 'حداکثر تعداد مجاز فیلد عنوان 60 کاراکتر است',
            'description.required' => 'فیلد عنوان اجباری است',
            'description.min' => 'حداقل 120 کاراکتر برای فیلد توضیحات وارد نمایید'
        ];
        $rules = [
            'title' => 'required|min:3|max:60',
            // 'description' => 'required|min:120'
        ];
        $this->validate($request,$rules,$messages);
        $podcast = new Podcast;
        $podcast->title = $request->title;
        $podcast->description = $request->description;
        $podcast->slug  = make_slug($request->title);
        $latestName = Podcast::whereRaw("`title` RLIKE '^{$podcast->title}(-[0-9]*)?$'")
        ->orderBy('id')->pluck('title');
        if (isset($latestName[0])) {
            $pieces = explode('-',$latestName);
            $number = intval(end($pieces));
            $podcast->slug .= '-' . ($number + 1);
        }
        $podcast->save();
        flash()->success("موفقیت !", "پادکست ساخته شد");
        return redirect()->to(route('Admin.podcasts.edit', ['podcast' => $podcast->id]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Podcast $podcast)
    {
        $podcast->load('seo.keywords');
        return view('CMSV::Pages.Podcasts.edit', compact('podcast'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,Podcast $podcast)
    {
        $podcast->load('seo');
        $messages = [
            'title.required' => 'فیلد عنوان اجباری است',
            'title.min' => 'حداقل سه کاراکتر برای فیلد عنوان پر کنید',
            'title.max' => 'حداکثر تعداد مجاز فیلد عنوان 60 کاراکتر است',
            'description.required' => 'فیلد عنوان اجباری است',
            'description.min' => 'حداقل 120 کاراکتر برای فیلد توضیحات وارد نمایید'
        ];
        $rules = [
            'title' => 'required|min:3|max:60',
            // 'description' => 'required|min:120'
        ];
        $this->validate($request,$rules,$messages);
        $podcast->title = $request->title;
        $podcast->description = $request->description;
        $podcast->slug  = make_slug($request->title);
        $latestName = Podcast::whereRaw("`id` != {$podcast->id} AND `title` RLIKE '^{$podcast->title}(-[0-9]*)?$'")
        ->orderBy('id')->pluck('title');
        if (isset($latestName[0])) {
            $pieces = explode('-',$latestName);
            $number = intval(end($pieces));
            $podcast->slug .= '-' . ($number + 1);
        }
        $podcast->save();
        if ($podcast->seo) {
            $podcast->seo->update(['description' => $request->seo_description, 'robots' => $request->seo_robots, 'priority' => $request->seo_priority, 'changefreq' => $request->seo_change ]);
            $podcast->seo()->keywords()->sync($request->seo_keywords);
        } else {
            $seo = $podcast->seo()->create(['description' => $request->seo_description, 'robots' => $request->seo_robots, 'priority' => $request->seo_priority, 'changefreq' => $request->seo_change ]);
            $seo->keywords()->sync($request->seo_keywords);
        }
        flash()->success("موفقیت !", "پادکست آپدیت شد");
        return redirect()->to(route('Admin.podcasts.edit', ['podcast' => $podcast->id]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Podcast::findOrfail($id)->delete();
        return back();
    }
}
