<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Keyword extends Model
{
    protected $hidden = ['pivot', 'updated_at', 'created_at'];
}
