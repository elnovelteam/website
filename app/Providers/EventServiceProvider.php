<?php

namespace App\Providers;

// use Illuminate\Auth\Events\Registered;
// use Illuminate\Auth\Listeners\SendEmailVerificationNotification;

use App\Listeners\SendBookPublishedNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        // Registered::class => [
        //     SendEmailVerificationNotification::class,
        // ],
        \App\Events\TestEvent::class => [
            'App\Listeners\TestListener'
        ],
        \App\Events\BookPublished::class => [
            SendBookPublishedNotification::class
        ],
        \App\Events\CommentPublished::class => [
            \App\Listeners\SendCommentPublishedNotification::class
        ],
        \App\Events\PoadcastPublished::class => [
            \App\Listeners\SendBookPublishedNotification::class
        ],
        \App\Events\RequestRecieved::class => [
            \App\Listeners\SendRequestRecievedNotification::class
        ],
        \App\Events\RuleChanged::class => [
            \App\Listeners\SendRuleChangedNotification::class
        ],
        \App\Events\UserCommented::class => [
            \App\Listeners\SendUserCommentedNotification::class
        ],
        \App\Events\UserSubscribed::class => [
            \App\Listeners\SendUserSubscribedNotification::class
        ],
        \App\Events\UserLiked::class => [
            \App\Listeners\SendUserLikedNotification::class
        ],
        \App\Events\UserRated::class => [
            \App\Listeners\SendUserRatedNotification::class
        ],
        \App\Events\UserVerified::class => [
            \App\Listeners\SendUserVerifiedNotification::class
        ],
        \App\Events\VersionPublished::class => [
            \App\Listeners\SendVersionPublishedNotification::class
        ],
        \App\Events\WriterPromoted::class => [
            \App\Listeners\SendWriterPromotedNotification::class
        ],
        \SocialiteProviders\Manager\SocialiteWasCalled::class => [
            'SocialiteProviders\\LinkedIn\\LinkedInExtendSocialite@handle',
            'SocialiteProviders\\Google\\GoogleExtendSocialite@handle',
        ],
        \App\Events\TicketCreated::class => [
            \App\Listeners\SendTicketCreatedNotfication::class
        ],
        \App\Events\TicketClosed::class => [
            'App\Listeners\SendTicketClosedNotfication'
        ],
        \App\Events\TicketUpdated::class => [
            \App\Listeners\SendTicketUpdatedNotfication::class
        ],
        \Illuminate\Auth\Events\Verified::class => [
            \Illuminate\Auth\Listeners\SendEmailVerificationNotification::class
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}

