<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Maincategory extends Model
{
    protected $fillable = ['title', 'note', 'parent_id', 'type'];
    public function categories() {
        return $this->hasMany(Category::class, 'cat_id');
    }
    public function children() {
        return $this->hasMany(Maincategory::class, 'parent_id')->orderBy('id');
    }
    public function parent() {
        return $this->belongsTo(Maincategory::class, 'parent_id');
    }
}
