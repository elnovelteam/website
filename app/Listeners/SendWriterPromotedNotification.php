<?php

namespace App\Listeners;

use App\Events\WriterPromoted;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendWriterPromotedNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  WriterPromoted  $event
     * @return void
     */
    public function handle(WriterPromoted $event)
    {
        //
    }
}
