<?php

namespace App\Listeners;

use App\Events\RuleChanged;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendRuleChangedNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  RuleChanged  $event
     * @return void
     */
    public function handle(RuleChanged $event)
    {
        //
    }
}
