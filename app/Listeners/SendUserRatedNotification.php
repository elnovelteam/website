<?php

namespace App\Listeners;

use App\Events\UserRated;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendUserRatedNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserRated  $event
     * @return void
     */
    public function handle(UserRated $event)
    {
        //
    }
}
