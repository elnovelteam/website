<?php

namespace App\Listeners;

use App\Comment;
use App\Events\BookPublished;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendBookPublishedNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  BookPublished  $event
     * @return void
     */
    public function handle(BookPublished $event)
    {
        $comment = new Comment;
        $comment->user_id = 1;
        $comment->body = 'new book published';
        $comment->commentable_id = 1;
        $comment->commentable_type = 'App\User';
        $comment->valid = true;
        dd($comment);
        // $comment->save();
    }
}
