<?php

namespace App\Listeners;

use App\Events\RequestRecieved;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendRequestRecievedNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  RequestRecieved  $event
     * @return void
     */
    public function handle(RequestRecieved $event)
    {
        //
    }
}
