<?php

namespace App\Listeners;

use App\Events\VersionPublished;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendVersionPublishedNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  VersionPublished  $event
     * @return void
     */
    public function handle(VersionPublished $event)
    {
        //
    }
}
