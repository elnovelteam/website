<?php

namespace App\Listeners;

use App\Events\UserCommented;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendUserCommentedNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserCommented  $event
     * @return void
     */
    public function handle(UserCommented $event)
    {
        //
    }
}
