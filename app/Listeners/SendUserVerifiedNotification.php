<?php

namespace App\Listeners;

use App\Events\UserVerified;
use App\Notifications\UserVerifiedNotification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendUserVerifiedNotification implements ShouldQueue
{
    public $connection = 'database';
    public $tries = 3;
    public $delay = 10;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserVerified  $event
     * @return void
     */
    public function handle(UserVerified $event)
    {
        $event->user->notify(new UserVerifiedNotification);
    }
}
