<?php

namespace App\Listeners;

use App\Events\TicketClosed;
use App\Notifications\TicketClosedNotification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendTicketClosedNotfication implements ShouldQueue
{
    use InteractsWithQueue;
    /**
     * The name of the connection the job should be sent to.
     *
     * @var string|null
     */
    public $connection = 'database';
    public $tries = 5;
    /**
     * The name of the queue the job should be sent to.
     *
     * @var string|null
     */
    // public $queue = 'listeners';

    /**
     * The time (seconds) before the job should be processed.
     *
     * @var int
     */
    public $delay = 20;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  TicketClosed  $event
     * @return void
     */
    public function handle(TicketClosed $event)
    {
        $event->user->notify((new TicketClosedNotification($event->ticket))->delay(now()->addMinutes(5)));
    }
}
