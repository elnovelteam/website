<?php

namespace App\Listeners;

use App\Events\CommentPublished;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendCommentPublishedNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CommentPublished  $event
     * @return void
     */
    public function handle(CommentPublished $event)
    {
        //
    }
}
