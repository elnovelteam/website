<?php

namespace App\Listeners;

use App\Events\UserLiked;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendUserLikedNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserLiked  $event
     * @return void
     */
    public function handle(UserLiked $event)
    {
        //
    }
}
