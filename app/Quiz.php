<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quiz extends Model
{
    protected $hidden = ['created_at', 'updated_at'];
    protected $fillable = ['quiz_id', 'user_id', 'total_score'];
    public function questions()
    {
        return $this->hasMany(QuizQuestion::class);
    }
    public function answers()
    {
        return $this->hasMany(QuizAnswers::class, 'quiz_user_id');
    }
    public function users()
    {
        return $this->belongsToMany(User::class);
    }
}
