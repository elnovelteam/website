<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SeoInformation extends Model
{
    protected $table    = 'seo_informations';
    protected $fillable = ['description','priority', 'changefreq', 'robots'];
    protected $hidden   = ['seoable_type', 'seoable_id'];
    public $timestamps  = false;
    public $robotsinfo  = ['index, follow', 'index, nofollow', 'noindex, follow', 'noindex, nofollow'];
    public function seoable()
    {
        return $this->morphTo();
    }
    public function getRobotsAttribute($robots)
    {
        return $this->robotsinfo[$robots] ?: null;
    }
    public function keywords()
    {
        return $this->belongsToMany(Keyword::class, 'keyword_seoinfo', 'seoinfo_id', 'keyword_id');
    }
}
