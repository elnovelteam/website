<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Section extends Model
{
    public function version() {
        return $this->belongsTo(Version::class);
    }
    public function pages() {
        return $this->hasMany(BookPage::class);
    }
}
