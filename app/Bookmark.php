<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bookmark extends Model
{
    public function bookmarkable() {
        return $this->morphTo();
    }
    public function user() {
        return $this->belongsTo(User::class);
    }
}
