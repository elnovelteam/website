<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookPage extends Model
{
    public function section() {
        return $this->belongsTo(Section::class);
    }
}
