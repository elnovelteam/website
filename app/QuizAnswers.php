<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuizAnswers extends Model
{
    public function question()
    {
        return $this->belongsTo(QuizQuestion::class, 'quiz_question_id');
    }
}
