<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class QuizController extends Controller
{
    public function show(Request $request, $quiz) {
        $quiz = \App\Quiz::whereId($quiz)->with(['questions' => function($question){
            $question->orderBy('index', 'asc');
        }])->firstOrFail();
        $quiz->questions->load('options');
        if ($request->user) {
            $user = \App\User::whereEmail($request->user)->firstOrFail();
            $quiz_user = $user->quizzes()->where('quiz_user.quiz_id', $quiz->id)->first();
            if ($quiz_user) {
                $answers = \App\QuizAnswers::where('user_quiz_id', $quiz_user->pivot->id)->get();
                $quiz->answers = $answers;
            }
        }
        return $quiz;
    }
    public function update(Request $request, $quiz) {
        if ($request->user) {
            $user = \App\User::whereEmail($request->user)->firstOrFail();
            $quiz_user = $user->quizzes()->where('quiz_user.quiz_id', $quiz)->first();
            $updatedIds = collect([]);
            if ( ! $quiz_user) {
                $quiz = \App\Quiz::whereId($quiz)->first();
                $quiz->users()->attach($user);
                $quiz_user = $user->quizzes()->where('quiz_user.quiz_id', $quiz->id)->first();
            }
            $answers = \App\QuizAnswers::where('user_quiz_id', $quiz_user->pivot->id)->get();
            foreach ($request->answers as $ans) {
                foreach ($answers as $answer) {
                    if ($answer->quiz_question_id == $ans["questionId"]) {
                        $updatedIds->push($answer->quiz_question_id);
                        if ($answer->quiz_option_id !== $ans['answerId'] || $answer->text_answer !== $ans['answerText']) {
                            $answer->quiz_option_id = $ans['answerId'];
                            $answer->text_answer = $ans['answerText'];
                            $answer->save();
                        }
                    }
                }
                if (! $updatedIds->contains($ans["questionId"])) {
                    $answer = new \App\QuizAnswers;
                    $answer->user_quiz_id = $quiz_user->pivot->id;
                    $answer->quiz_question_id = $ans["questionId"];
                    $answer->quiz_option_id = $ans["answerId"];
                    $answer->text_answer = $ans["answerText"];
                    $answer->save();
                }
            }
            return ['okay' => true];
        }
        abort(403);
    }
}
