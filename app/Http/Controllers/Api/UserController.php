<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Response;

class UserController extends Controller
{
    public function login(Request $request)
    {
        $login = $request->validate([
           'email' => 'required|string',
           'password' => 'required|string'
        ]);
        if (! Auth::attempt($login)) {
            return response()->json(['message' => 'Login is not valid'], 401);
        }
        $user = Auth::user();
        $accessToken = $user->createToken('authToken' . $user->id . time())->accessToken;
        return response(['user' => Auth::user(), 'accessToken' => $accessToken]);
    }

    public function tokens(Request $request)
    {
        $user = Auth::user();
        return $user;
        // dd($user->tokens());
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
