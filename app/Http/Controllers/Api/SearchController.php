<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Post;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function index(Request $request, $keyword)
    {
        $results = [];
        $order   = null;
        if ($request->query('order_by')) {
            $order = ($request->query('order') == 'desc') ? $request->order : 'asc';
        }
		$models = [
			'post'    => new \App\Post,
			'page'    => new \App\Page,
			'book'    => new \App\Book,
			'writer'  => new \App\Writer,
			'user'    => new \App\User,
			'serie'   => new \App\Serie,
			'podcast' => new \App\Podcast,
		];
        foreach ($request->model as $model) {
            if ($models[$model]) {
                $q = $models[$model]->limit(3)->search($keyword);
                if ($order) {
                    $q->orderBy($request->query('order_by'), $order);
                }
                $results[$model] = $q->get()->toArray();
            }
        }
        return $results;
    }
    public function show(Request $request, $keyword)
    {
        $results = [];
        $order   = null;
        if ($request->query('order_by')) {
            $order = ($request->query('order') == 'desc') ? $request->order : 'asc';
        }
		$models = [
			'post'    => \App\Post::with('photo.file')->withCount('likes'),
			'page'    => \App\Page::with('photo.file')->withCount('likes'),
			'book'    => \App\Book::with('photo.file')->withCount('likes'),
			'user'    => \App\User::with('photo.file')->withCount('likes'),
			'serie'   => \App\Serie::with('photo.file')  ->withCount('likes'),
            'writer'  => \App\Writer::with('photo.file') ->withCount('likes'),
            'podcast' => \App\Podcast::with('photo.file')->withCount('likes'),
		];
        foreach ($request->model as $model) {
            if ($models[$model]) {
                $q = $models[$model]->search($keyword);
                if ($order) {
                    $q->orderBy($request->query('order_by'), $order);
                }
                $results[$model] = $q->paginate(21)->appends($request->query())->toArray();
            }
        }
        return $results;
    }
}
