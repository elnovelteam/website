<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LikeController extends Controller
{
    public function __construct()
    {
         $this->middleware('auth:api2')->only('store');
    }
    public function store(Request $request, $target, $target_id)
    {
        $model = $this->checkTarget($target, $target_id);
        $like = $model->likes()->where('user_id', auth()->user()->id)->first();
        if ($like) {
            $like->delete();
        } else {
            $model->likes()->create(['user_id' => auth()->user()->id]);
        }
        return ['okay' => true];
    }
}
