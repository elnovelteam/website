<?php

namespace App\Http\Controllers;

use App\Podcast;
use Illuminate\Http\Request;

class PodcastController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $podcasts = Podcast::with('photo.file')->get();
        return view('theme.components.podcasts.index', compact('podcasts'));
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($podcast)
    {
        $podcast = Podcast::whereSlug($podcast)->with(['seo.keywords','file', 'photo.file', 'categories.info'])->firstOrFail();
        views($podcast)->record();
        return view('theme.components.podcasts.show', compact('podcast'));
    }
}
