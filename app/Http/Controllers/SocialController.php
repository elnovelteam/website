<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;

class SocialController extends Controller
{
    public function provider($driver)
    {
        return Socialite::driver($driver)->redirect();
    }
    public function providerRedirect($driver)
    {
        $redirectedUser = Socialite::driver($driver)->stateless()->user();
        $user = User::where(['email' => $redirectedUser->getEmail()])->first();
        if ($user) {
            Auth::login($user);
            return redirect('/');
        } else {
            $user = new User;
            $user->email = $redirectedUser->getEmail();
            $user->name = $redirectedUser->getName();
            $user->passord = \Illuminate\Support\Str::random(20);
            $user->save();
            Auth::login($user);
            return redirect('/');
        }
    }
}
