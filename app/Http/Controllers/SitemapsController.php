<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SitemapsController extends Controller
{
    public $sitemaps = ['posts', 'pages', 'podcasts', 'campaigns', 'books', 'series'];
    public function index()
    {
        $sitemaps = $this->sitemaps;
        return response()->view('layouts.sitemaps.index', compact('sitemaps'))->header('Content-Type', 'text/xml');
    }
    public function show($sitemap)
    {
        abort_if(! in_array($sitemap, $this->sitemaps), 404);
        $target = \App\Post::orderBy('updated_at', 'desc');
        switch ($sitemap) {
            case 'books':
                $target = \App\Book::orderBy('updated_at', 'desc');
            break;
            case 'series':
                $target = \App\Serie::orderBy('updated_at', 'desc');
            break;
            case 'pages':
                $target = \App\Page::orderBy('updated_at', 'desc');
            break;
            case 'podcasts':
                $target = \App\Podcast::orderBy('updated_at', 'desc')->with('file');
            break;
            case 'campaigns':
                $target = \App\Campaign::orderBy('updated_at', 'desc');
            break;
        }
        $prefix  = ($sitemap == 'pages') ? '/' : $sitemap . '/';
        $results = $target->with(['seo', 'photo.file'])->get();
        return response()->view('layouts.sitemaps.show', compact('results', 'prefix'))->header('Content-Type', 'text/xml');
    }
}
