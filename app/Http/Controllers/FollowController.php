<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class FollowController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth:api2'])->only(['follow', 'unfollow']);
    }
    public function follow(Request $request, $user)
    {
        $loggedInUser = $request->user('api2');
        $loggedInUser->followAction($user);
        return $request->wantsJson() ? ['okay' => true] : back();
    }
    public function unfollow(Request $request, $user)
    {
        $loggedInUser = $request->user('api2');
        $loggedInUser->followAction($user, false);
        return $request->wantsJson() ? ['okay' => true] : back();
    }
    public function followers(Request $request, $user_id)
    {
        $user = User::find($user_id);
        $loggedInUser = $request->user('api2');
        if ($loggedInUser) {
            $user->load(['followers.user']);
            $user->followers->map(function ($follower) use($loggedInUser){
                $follower->followed_by_user = ($follower->user->id !== $loggedInUser->id) ? $follower->FollowedByUser() : null;
                return $follower;
            });
        }
        return $user->followers;
    }
    public function followings(Request $request, $user_id)
    {
        $user = User::find($user_id);
        $loggedInUser = $request->user('api2');
        if ($loggedInUser) {
            $self = intval($user_id) === $loggedInUser->id;
            $user->load(['followings.followable']);
            $user->followings->map(function ($following) use($self, $loggedInUser) {
                if ($following->followable_id !== $loggedInUser->id) {
                    $following->followed_by_user = ($following->FollowedByUser($following->followable_id, $following->followable) || $self) ? true : false;
                }
                return $following;
            });
        }
        return $user->followings;
    }
}
