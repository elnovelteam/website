<?php

namespace App\Http\Controllers;

use GhasedakSMS;
use Illuminate\Http\Request;


class SmsController extends Controller
{
    public function index()
    {
        try {
            $message = "تست جدید";
            $lineNumber = "10008566";
            $receptor = "09150013422";
            $api = GhasedakSMS::SendSimple($receptor,$message,$lineNumber);
            dd($api);
        }
        catch(\Ghasedak\Exceptions\ApiException $e){
            echo $e->errorMessage();
        }
        catch(\Ghasedak\Exceptions\HttpException $e){
            echo $e->errorMessage();
        }
    }
}
