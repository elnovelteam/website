<?php

namespace App\Http\Controllers;

use App\User;
use App\Writer;
use Illuminate\Http\Request;

class WriterController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $writers = Writer::with('photo.file', 'user')->withCount('books')->whereActive(2)->paginate(20);
        return $writers;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($writer)
    {
        $writer = Writer::whereNickname($writer)->with(['photo.file', 'user', 'books', 'education', 'interest', 'exprience'])->firstOrFail();
        $user = $writer->user;
        return view('theme.writer', compact('writer', 'user'));
    }

}
