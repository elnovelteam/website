<?php

namespace App\Http\Controllers;

use App\Book;
use App\Comment;
use App\Page;
use App\Podcast;
use App\Post;
use App\Version;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public function checkTarget($target, $target_id)
    {
        $model = null;
        switch ($target) {
            case 'post':
                $model = Post::whereId($target_id);
            break;
            case 'page':
                $model = Page::whereId($target_id);
            break;
            case 'podcast':
                $model = Podcast::whereId($target_id);
            break;
            case 'book':
                $model = Book::whereId($target_id);
            break;
            case 'version':
                $model = Version::whereId($target_id);
            break;
            case 'comment':
                $model = Comment::whereId($target_id);
            break;
        }
        return $model->firstOrFail();
    }
}
