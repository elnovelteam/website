<?php

namespace App\Http\Controllers;

use App\Comment;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api2')->only('store');
    }
    public function show(Request $request, $target, $target_id)
    {
        $model = $this->checkTarget($target, $target_id);
        $comments = $model->comments()->whereActive(true)->withCount('likes')->with(['user', 'replies' => function($q) {
            $q->with(['user'])->withCount('likes');
        }])->latest()->paginate(20);
        return response()->json(['comments' => $comments, 'user' => $request->user('api2')]);
    }
    public function store(Request $request, $target, $target_id)
    {
        $target = ($request->reply_id) ? 'comment' : $target;
        $target_id = $request->reply_id ?: $target_id;
        $model = $this->checkTarget($target, $target_id);
        if ($model instanceof Comment) {
            $comment = $model->replies()->create([
               'body' => $request->body,
               'user_id' => auth()->user()->id,
                'active' => false
            ]);
        } else {
            $comment = $model->comments()->create([
                'body' => $request->body,
                'user_id' => auth()->user()->id,
                'active' => false
            ]);
        }

        $comment = Comment::whereId($comment->id)->with('user')->first();
        // broadcast(new NewComment($comment))->toOthers();
        return $comment->toJson();
    }
}
