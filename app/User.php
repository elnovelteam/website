<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasApiTokens, Notifiable;

    protected $fillable = ['name', 'email', 'password', 'sex', 'phone_number', 'birthdate', 'state_id', 'provider_id'];
    protected $hidden = ['password', 'remember_token'];
    protected $casts = ['email_verified_at' => 'datetime'];

    public function bookmarks() {
        return $this->hasMany(Bookmark::class);
    }
    public function comments() {
        return $this->hasMany(Comment::class);
    }
    public function likes() {
        return $this->hasMany(Like::class);
    }
    public function rates() {
        return $this->hasMany(Rate::class);
    }
    public function followings() {
        // return $this->hasManyThrough(User::class, Follow::class, 'user_id', 'id', 'id', 'followable_id');
        return $this->hasMany(Follow::class);
    }
    public function tickets() {
        return $this->hasMany(Ticket::class);
    }
    public function writer() {
        return $this->hasOne(Writer::class);
    }


    public function roles() {
        return $this->belongsToMany(Role::class);
    }
    public function permissions() {
        return $this->belongsToMany(Permission::class);
    }
    public function quizzes() {
        return $this->belongsToMany(Quiz::class, 'quiz_user')->withPivot(['total_score', 'id']);
    }
    public function state() {
        return $this->belongsTo(State::class);
    }

    public function photo() {
        return $this->morphOne(Photo::class, 'photoable');
    }
    public function followers() {
        return $this->morphMany(Follow::class, 'followable');
    }
    public function followAction($user_id, $follow = true) {
        $target = User::findOrFail($user_id);
        $follow_model = $target->followers()->where(['user_id' => $this->id])->first();
        if ($follow && ! $follow_model) {
            return $target->followers()->create(['user_id' => $this->id]);
        } elseif (! $follow && $follow_model) {
            return $follow_model->delete();
        }
    }
    public function getBirthdateAttribute($birthdate) {
        return new Carbon($birthdate);
    }
    public function hasBeenFollowed() {
        return $this->followers()->where(['user_id' => auth()->user()->id, 'followable_id' => $this->id])->first() ? true : false;
    }
}
