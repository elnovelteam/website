<?php

use App\Definition;

function flash($title = NULL,$msg = NULL)
{
    $flash = app('App\Http\Flash');
    if (func_num_args() == 0) {
        return $flash;
    }
    return $flash->info($title,$msg);
}

function organize_keywords_for_seo($keywords) {
    $keys = '';
    foreach ($keywords as $i => $keyword) {
        $keys .=  ($i + 1 == count($keywords)) ? $keyword->value : $keyword->value . ',';
    }
    return $keys;
}

function make_slug($string, $separator = '-')
{
    $string = trim($string);
	$string = mb_strtolower($string, 'UTF-8');
	// Make alphanumeric (removes all other characters)
	// this makes the string safe especially when used as a part of a URL
	// this keeps latin characters and Persian characters as well
	$string = preg_replace("/[^a-z0-9\s_-۰۱۲۳۴۵۶۷۸۹ءاآؤئبپتثجچحخدذرزژسشصضطظعغفقکكگگلمنوهی]/u", '', $string);
	// Remove multiple dashes or whitespaces or underscores

	$string = preg_replace("/\s[-_]+/", ' ', $string);
	// Convert whitespaces and underscore to the given separator
	$string = preg_replace("/[\s_]/", $separator, $string);
    $arabic_punct = ['،', '؛', '؟', '⠐', '!'];
    foreach ($arabic_punct as $punct) {
        $string = preg_replace("#".mb_strtolower($punct, 'UTF-8')."#", '', $string);
    }
	return $string;
}

function determine_sex($sex) {
    return ($sex == 'male') ? '<i class="fas fa-3x fa-mars c-blue"></i>' : '<i class="fas fa-3x fa-venus c-pink"></i>';
}

function robots() {
    return ['index, follow','index, nofollow','noindex, follow','noindex, nofollow'];
}

function changefreq() {
    return ['daily','weekly','monthly','yearly'];
}

function keywords() {
    return App\Keyword::all();
}

function definitions($type) {
    return Definition::whereType($type)->get();
}