<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    public function books() {
        return $this->belongsToMany(Book::class);
    }
    public function pages() {
        return $this->belongsToMany(Page::class);
    }
    public function posts() {
        return $this->belongsToMany(Post::class);
    }
}
