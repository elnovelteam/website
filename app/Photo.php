<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    protected $fillable = ['title', 'alt', 'file_id'];
    public function photoable() {
        return $this->morphTo();
    }
    public function file() {
        return $this->belongsTo(File::class);
    }
}
