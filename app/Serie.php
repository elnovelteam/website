<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;

class Serie extends Model
{
    use SearchableTrait;
    protected $searchable = [
        'columns' => [
            'title' => 10,
            'description' => 8
            // 'series.title' => 10,
            // 'books.title' => 8,
            // 'books.summary' => 4
        ],
    ];
    public function serieable() {
        return $this->morphTo();
    }
    public function likes() {
        return $this->morphMany(Like::class, 'likeable');
    }
    public function comments() {
        return $this->morphMany(Comment::class, 'commentable');
    }
    public function books() {
        return $this->hasMany(Book::class);
    }
    public function photo() {
        return $this->morphOne(Photo::class, 'photoable');
    }
    public function categories() {
        return $this->morphMany(Category::class, 'categorable');
    }
    public function bookmark() {
        return $this->morphMany(Bookmark::class, 'bookmarkable');
    }
}
