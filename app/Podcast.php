<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use CyrildeWit\EloquentViewable\InteractsWithViews;
use CyrildeWit\EloquentViewable\Contracts\Viewable;
use Illuminate\Notifications\Notifiable;
use Nicolaslopezj\Searchable\SearchableTrait;

class Podcast extends Model implements Viewable
{
    use InteractsWithViews, Notifiable, SearchableTrait;
    protected $searchable = [
        'columns' => [
            'title' => 10,
            'description'=> 5,
        ],
    ];
    protected $fillable = ['title', 'descripton', 'file_id'];
    public function file() {
        return $this->belongsTo(File::class);
    }
    public function photo() {
        return $this->morphOne(Photo::class, 'photoable');
    }
    public function seo() {
        return $this->morphOne(SeoInformation::class, 'seoable');
    }
    public function categories()
    {
        return $this->morphMany(Category::class, 'categorable');
    }
    public function likes() {
        return $this->morphMany(Like::class, 'likeable');
    }
}
