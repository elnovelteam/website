<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    protected $fillable = ['name', 'country_id'];
    protected $hidden = ['country_id'];
    public function country() {
        return $this->belongsTo(Country::class);
    }
    public function users() {
        return $this->hasMany(User::class);
    }
}
