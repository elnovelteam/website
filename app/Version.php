<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Version extends Model
{
    public function book() {
        return $this->belongsTo(Book::class);
    }
    public function file() {
        return $this->belongsTo(File::class);
    }
    public function sections() {
        return $this->hasMany(Section::class);
    }
}
