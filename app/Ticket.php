<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    protected $fillable = ['title', 'question', 'code', 'force_rate', 'status', 'backup_section_id'];
    public function messages()
    {
        return $this->hasMany(TicketMessage::class);
    }
    public function section()
    {
        return $this->belongsTo(BackupSection::class, 'backup_section_id');
    }
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
