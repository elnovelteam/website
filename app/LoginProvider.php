<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LoginProvider extends Model
{
    public $timestamps = false;
    protected $fillable = ['name'];
    //
}
