<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Nicolaslopezj\Searchable\SearchableTrait;

class Writer extends Model
{
    use SearchableTrait, SoftDeletes;
    protected $searchable = [
        'columns' => [
            'nickname' => 10,
            'bio'      => 8,
            // 'writers.nickname' => 10,
            // 'books.title' => 8,
        ],
        // 'joins' => [
        //     'books' => ['writers.id', 'books.writer_id'],
        // ]
    ];
    protected $fillable = ['first_name', 'last_name', 'nickname', 'bio', 'education_id', 'exprience_id', 'interest_id', 'active'];
    public function books() {
        return $this->hasMany(Book::class);
    }
    public function likes() {
        return $this->morphMany(Like::class, 'likeable');
    }
    public function user() {
        return $this->belongsTo(User::class);
    }
    public function rates() {
        return $this->morphMany(Rate::class, 'rateable');
    }
    public function interest() {
        return $this->belongsTo(Definition::class, 'interest_id', 'id');
    }
    public function exprience() {
        return $this->belongsTo(Definition::class, 'exprience_id', 'id');
    }
    public function education() {
        return $this->belongsTo(Definition::class, 'education_id', 'id');
    }
    public function photo() {
        return $this->morphOne(Photo::class, 'photoable');
    }
}
