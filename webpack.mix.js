const mix = require('laravel-mix');
const src = {
    res: {
        js: "resources/js/",
        sass: "resources/sass/",
        eladmin: {
            js: "resources/js/eladmin/",
            sass: "resources/sass/admin/eladmin/"
        },
        userarea: {
            js: "resources/js/userarea/",
            sass: "resources/sass/admin/userarea/"
        }
    },
    pub: {
        js: "public/js/",
        css: "public/css/",
        eladmin: {
            js: "public/js/eladmin/",
            css: "public/css/eladmin/"
        },
        userarea: {
            js: "public/js/userarea/",
            css: "public/css/userarea/"
        }
    }
}
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

var { res, pub } = src;

// mix.sass(res.userarea.sass + 'userarea.scss', pub.css)
// mix.react(res.js + 'request-form.js', pub.js)
mix.sass(res.userarea.sass + 'userarea.scss', pub.css)
// mix.react(res.js + 'request-form.js', pub.js)
// .sass(res.eladmin.sass + 'admin.scss', pub.css);
// .sass(res.eladmin.sass + 'rtl.scss', pub.css);
// mix.js(res.eladmin.js + 'dropzone.js', pub.js);
// mix.sass(res.sass + 'auth.scss', pub.css)
// // .sass(res.sass + 'errors.scss', pub.css)
// mix.react(res.js + 'app.js', pub.js)
//     .react(res.js + 'ck-editor.js', pub.js)
//     .sass(res.sass + 'app.scss', pub.css)
//     .react(res.js + 'comments.js', pub.js)
//     .react(res.js + 'searchPro.js', pub.js)
//     .react(res.js + 'writer-profile.js', pub.js)
// .sass(res.eladmin.sass + 'admin.scss', pub.css);
    // mix.react(res.js + 'audio-player.js', pub.js)
// mix.sass(res.sass + 'errors.scss', pub.css)
    // .sass(res.eladmin.sass + 'admin.scss', pub.eladmin.css)
	// .sass(res.eladmin.sass + 'rtl.scss', pub.eladmin.css)
	// .sass(res.sass + 'auth.scss', pub.css);
    // .react(res.js + 'editor.js', pub.js);
    // .react(res.js + 'app.js', pub.js);
    // .react([
    //     res.js + '/app.js', pub.js,
    //     res.eladmin.js + '/quill.js',
    //     res.eladmin.js + '/CollapsableGallery.js',
    //     res.eladmin.js + '/Gallery.js',
    //     res.eladmin.js + '/Country.js',
    //     res.eladmin.js + '/dropzone.js',
    //     res.eladmin.js + '/SelectedCountry.js',
    // ], pub.js);
