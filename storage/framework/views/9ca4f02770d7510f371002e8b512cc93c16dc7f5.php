<div class="swiper-container" id="slider">
    <!-- Additional required wrapper -->
    <div class="swiper-wrapper">
        <!-- Slides -->
        <div class="swiper-slide"><img src="<?php echo e(asset('images/slide.jpg')); ?>" alt=""></div>
        <div class="swiper-slide"><img src="<?php echo e(asset('images/slider-1.jpg')); ?>" alt=""></div>
        <div class="swiper-slide"><img src="<?php echo e(asset('images/slider-2.jpg')); ?>" alt=""></div>
        <div class="swiper-slide"><img src="<?php echo e(asset('images/slider-3.jpg')); ?>" alt=""></div>
    </div>
    <!-- If we need pagination -->
    <div class="swiper-pagination"></div>
    <!-- If we need navigation buttons -->
    <div class="swiper-button-prev"><i class="fas fa-4x fa-angle-left"></i></div>
    <div class="swiper-button-next"><i class="fas fa-4x fa-angle-right"></i></div>
</div>
<?php /**PATH E:\Project\Programming\php\elnovel\resources\views/theme/components/slider.blade.php ENDPATH**/ ?>