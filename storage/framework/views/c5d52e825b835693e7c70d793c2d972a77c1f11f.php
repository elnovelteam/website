<nav class="menu">
    <div class="logo">
        <a href="<?php echo e(route('home')); ?>"><img src="<?php echo e(route('home')); ?>/images/logo.png" alt=""></a>
    </div>
    <ul>
        <li><a href="<?php echo e(asset('/')); ?>">خونه</a></li>
        <li><a href="<?php echo e(asset('/novels')); ?>">رمان ها</a></li>
        <li><a href="<?php echo e(asset('/writers')); ?>">داستان نویس ها</a></li>
        <li><a href="<?php echo e(asset('/podcasts')); ?>">پادکست ها</a></li>
        <li><a href="<?php echo e(route('blog.index')); ?>">وبلاگ</a></li>
        <li><a href="<?php echo e(asset('/contact')); ?>">تماس با ما</a></li>
        <li><a href="<?php echo e(route('search')); ?>">جستجو</a></li>
    </ul>
    <div class="last col-sm-6 col-12">
    <div class="btn btn-dark float-left d-sm-none d-block" id="menu-opener">
        <i class="fas fa-2x fa-bars"></i>
    </div>
    <?php if(auth()->guard()->guest()): ?>
    <p class="float-left text-center col-lg-12 col-8 white h-100">
        <span id="search-icon" class="search-icon"><i class="fas fa-search"></i></span>
        <span class="search-icon">
            <a class="mt-0" href="<?php echo e(route('register')); ?>"><i class="fas fa-user"></i> عضویت</a>
            <a class="mt-0" href="<?php echo e(route('login')); ?>"><i class="fas fa-lock"></i> ورود</a>
        </span>
    </p>
    <?php else: ?>
        <p class="float-left text-center col-lg-12 col-8 white h-100">
            <a class="float-left text-center search-icon d-sm-block d-none" href="<?php echo e(route('logout')); ?>" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
            <?php echo app('translator')->get('auth.logout'); ?> <i class="fas fa-sign-out-alt"></i>
            </a>
            <span id="search-icon" class="search-icon"><i class="fas fa-search"></i></span>
            <a href="<?php echo e(route('userarea.')); ?>" class="search-icon"><?php echo e(ucfirst(auth()->user()->name)); ?> <i class="fas fa-user"></i></a>
        </p>
        <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" style="display: none;">
            <?php echo csrf_field(); ?>
        </form>
    <?php endif; ?>
    </div>
</nav>
<?php /**PATH E:\Project\Programming\php\elnovel\resources\views/theme/components/menu.blade.php ENDPATH**/ ?>