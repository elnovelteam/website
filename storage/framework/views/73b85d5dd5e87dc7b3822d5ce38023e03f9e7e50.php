<?php $__env->startSection('form'); ?>
<div class="title col-12 mb-3 p-3 text-center"><?php echo app('translator')->get('auth.login'); ?> / <a href="<?php echo e(route('register')); ?>"><?php echo app('translator')->get('auth.register'); ?></a></div>
<form class="col-12" action="<?php echo e(route('login')); ?>" method="post">
	<?php echo csrf_field(); ?>
    <div class="input-group mb-3 col-12">
        <input type="text" name="email" class="form-control" placeholder="نام کاربری یا ایمیل" aria-label="Username" aria-describedby="username">
		<div class="input-group-append">
            <span class="input-group-text" id="basic-addon1"><i class="fas fa-user"></i></span>
        </div>
	</div>
	<div class="input-group mb-3 col-12">
        <input type="password" name="password" class="form-control" placeholder="رمز عبور" aria-label="Username" aria-describedby="password">
		<div class="input-group-append">
            <span class="input-group-text" id="basic-addon1"><i class="fas fa-lock"></i></span>
        </div>
	</div>
    <div class="input-group mb-3 col-12">
        <div class="col-12 float-right text-center">
            <label for="remember">مرا به خاطر بسپار</label>
            <input type="checkbox" name="remember" id="remember" class="remember">
        </div>
	</div>
	<div class="input-group mb-3 col-12">
        <button type="submit" class="btn btn-primary col-md-4 offset-md-4 col-12">ورود</button>
        <div class="col-12 px-3 pt-3">
        <?php if(Route::has('password.request')): ?>
            <a class="btn float-right" href="<?php echo e(route('password.request')); ?>"><?php echo app('translator')->get('auth.passwordforget'); ?></a>
        <?php endif; ?>
        </div>
	</div>
</form>
<div class="col-md-6 col-12 p-3 float-left oauth">
    <a class="col-12 btn linkedin" href="<?php echo e(route('login.driver', ['driver'  => 'linkedIn'])); ?>">
        <span><i class="fab fa-2x fa-linkedin-in"></i></span>
        <p><?php echo app('translator')->get('auth.signinLinkedIn'); ?></p>
    </a>
</div>
<div class="col-md-6 col-12 p-3 float-left oauth">
    <a class="col-12 btn google" href="<?php echo e(route('login.driver', ['driver'  => 'Google'])); ?>">
        <span class="p-1 br"><img class="px-2" height="40" width="40" src="<?php echo e(asset('images/inc/google-logo.svg')); ?>" alt=""></span>
        <p><?php echo app('translator')->get('auth.signinGoogle'); ?></p>
    </a>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.auth', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH E:\Project\Programming\php\elnovel\resources\views/auth/login.blade.php ENDPATH**/ ?>