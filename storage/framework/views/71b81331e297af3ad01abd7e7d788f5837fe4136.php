<?php $__env->startSection('form'); ?>
<div class="title col-12 mb-3 p-3 text-center"><?php echo app('translator')->get('auth.verifyemail'); ?></div>
<?php if(session('resent')): ?>
    <div class="col-12 mb-3 p-3 float-left alert alert-dark text-right" role="alert"><?php echo app('translator')->get('auth.freshLinkMessage'); ?></div>
<?php else: ?>
<?php endif; ?>
<form class="col-12" action="<?php echo e(route('verification.resend')); ?>" method="post">
	<?php echo csrf_field(); ?>
    <div class="input-group mb-3 col-12">
        <p class="text-center col-12 c-white"><?php echo app('translator')->get('auth.checkYourEmailMessage'); ?></p>
        <button type="submit" class="btn btn-dark p-3 m-auto align-baseline"><?php echo app('translator')->get('auth.ifDidnotRecieveEmail'); ?><br><?php echo app('translator')->get('auth.requestAnother'); ?></button>
	</div>
</form>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.auth', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH E:\Project\Programming\php\elnovel\resources\views/auth/verify.blade.php ENDPATH**/ ?>