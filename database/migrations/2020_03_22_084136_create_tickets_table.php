<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $status = ['INPROGRESS', 'CLOSED', 'OPEN'];
        Schema::create('tickets', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('code');
            $table->string('title');
            $table->longText('question');
            $table->integer('force_rate');
            $table->integer('rate')->default(0);
            $table->string('status')->default('OPEN');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('backup_section_id');
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('backup_section_id')->references('id')->on('backup_sections')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tickets');
    }
}
