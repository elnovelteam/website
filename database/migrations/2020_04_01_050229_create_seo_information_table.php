<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSeoInformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seo_informations', function (Blueprint $table) {
            $table->increments('id');
            $table->morphs('seoable');
            $table->string('description', 160)->nullable();
            $table->integer('robots')->default(1);
            $table->string('changefreq')->default('never');
            $table->decimal('priority', 10, 1)->default(0.5);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('seo_informations');
    }
}
