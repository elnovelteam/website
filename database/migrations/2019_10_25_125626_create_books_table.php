<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('books', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('slug');
            $table->text('summary');
            $table->text('description');
            $table->integer('admin_rate')->default(0);
            $table->boolean('certified')->default(false);
            $table->boolean('active')->default(false);
            $table->integer('writer_id')->unsigned();
            $table->unsignedInteger('serie_id')->nullable();
            $table->integer('last_views_count')->default(0);
            $table->softDeletes();
            $table->timestamps();
            $table->foreign('writer_id')->references('id')->on('writers')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('serie_id')->references('id')->on('series')->onDelete('cascade')->onUpdate('cascade');
        });
        Schema::create('book_tag', function (Blueprint $table) {
            $table->integer('tag_id')->unsigned();
            $table->integer('book_id')->unsigned();
            $table->foreign('tag_id')->references('id')->on('tags')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('book_id')->references('id')->on('books')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('books');
        Schema::dropIfExists('book_tag');
    }
}
