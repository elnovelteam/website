<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->string('sex')->nullable();
            $table->dateTime('birthdate')->nullable();
            $table->string('phone_number')->nullable();
            $table->string('email')->unique();
            $table->string('password');
            $table->string('api_token')->unique()->nullable();
            $table->boolean('active')->default(true);
            $table->unsignedInteger('country_id');
            $table->unsignedInteger('state_id')->nullable();
            $table->unsignedInteger('provider_id');
            $table->boolean('profile_visible')->default(true);
            $table->integer('last_views_count')->default(0);
            $table->rememberToken();
            $table->timestamp('email_verified_at')->nullable();
            $table->timestamps();
            $table->foreign('country_id') ->references('id')->on('countries')      ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('state_id')   ->references('id')->on('states')         ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('provider_id')->references('id')->on('login_providers')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
