<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuizAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quiz_answers', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_quiz_id');
            $table->unsignedInteger('quiz_question_id');
            $table->unsignedInteger('quiz_option_id')->nullable();
            $table->text('text_answer')->nullable();
            $table->timestamps();
            $table->foreign('user_quiz_id')->references('id')->on('quiz_user')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('quiz_question_id')->references('id')->on('quiz_questions')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('quiz_option_id')->references('id')->on('quiz_question_options')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quiz_answers');
    }
}
