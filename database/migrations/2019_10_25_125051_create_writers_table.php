<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWritersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('writers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name');
            $table->string('last_name')->nullable();
            $table->string('nickname')->unique();
			$table->string('bio', 200);
            $table->boolean('active')->default(true);
            $table->boolean('certified')->default(false);
            $table->integer('admin_rate')->default(0);
            $table->integer('education_id')->unsigned();
            $table->integer('interest_id')->unsigned();
            $table->integer('exprience_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->integer('last_views_count')->default(0);
            $table->softDeletes();
            $table->timestamps();
            $table->foreign('education_id')->references('id')->on('definitions')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('interest_id')->references('id')->on('definitions')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('exprience_id')->references('id')->on('definitions')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('writers');
    }
}
