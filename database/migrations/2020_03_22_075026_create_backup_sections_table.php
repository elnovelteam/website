<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBackupSectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('backup_sections', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('description');
        });
        Schema::create('backup_section_user', function (Blueprint $table) {
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('backup_section_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('backup_section_id')->references('id')->on('backup_sections')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('backup_sections');
        Schema::dropIfExists('backup_section_user');
    }
}
