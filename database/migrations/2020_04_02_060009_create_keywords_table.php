<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKeywordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('keywords', function (Blueprint $table) {
            $table->increments('id');
            $table->string('value');
            $table->string('type');
            $table->timestamps();
        });
        Schema::create('keyword_seoinfo', function (Blueprint $table) {
            $table->unsignedInteger('keyword_id');
            $table->unsignedInteger('seoinfo_id');
            $table->foreign('keyword_id')->references('id')->on('keywords')        ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('seoinfo_id')->references('id')->on('seo_informations')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('keywords');
        Schema::dropIfExists('keyword_seoinfo');
    }
}
