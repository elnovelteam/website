<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Writer;
use Faker\Generator as Faker;

$factory->define(Writer::class, function (Faker $faker) {
    return [
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'nickname' => $faker->userName,
        'bio' => $faker->sentence,
        'active' => true,
        'certified' => false,
        'education_id' => 1,
        'interest_id' => 1,
        'exprience_id' => 1,
        'user_id' => rand(1,10)
    ];
});

