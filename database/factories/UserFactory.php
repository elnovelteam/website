<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\User;
use App\Writer;
use Faker\Generator as Faker;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    $sexes = ['male','female'];
    return [
        'name' => $faker->userName,
        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => null,
        'password' => bcrypt('uss828487'),
        'birthdate' => $faker->dateTimeBetween('1990-01-01', '2003-12-31')->format('Y-m-d'),
        'phone_number' => $faker->phoneNumber,
        'sex' => Arr::random($sexes),
        'country_id' => rand(1,245),
        'provider_id' => 1,
        'active' => true,
    ];
});