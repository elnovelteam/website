<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Post;
use Faker\Generator as Faker;

$factory->define(Post::class, function (Faker $faker) {
    $title = $faker->words(4,true);
    return [
        'lang' => 'fa',
        'body' => "<h1>{$title}</h1>",
        'title'   => $title,
        'slug' => str_replace(' ', '-', $title),
        'summary' => $faker->paragraph,
        'menu_id' => null,
        'slider_id' => null,
        'last_views_count' => rand(0,1234) * 1000
    ];
});