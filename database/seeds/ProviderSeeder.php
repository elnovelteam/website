<?php

use App\LoginProvider;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

require 'providers.php';

class ProviderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('ALTER TABLE `login_providers` AUTO_INCREMENT = 1;');
        foreach (providers() as $provider_name) {
            $provider = new LoginProvider;
            $provider->name = $provider_name;
            // var_dump($provider->name);
            $provider->save();
        }
    }
}
