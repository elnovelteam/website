<?php

use Illuminate\Database\Seeder;

class DefinitionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $definitions = [
            [
                'title' => 'محصل',
                'lang'  => 'fa',
                'type'  => 'education',
                'description' => 'در حال تحصیل در مقطع متوسطه / دبیرستان',
            ],
            [
                'title' => 'دیپلم',
                'lang'  => 'fa',
                'type'  => 'education',
                'description' => 'اتمام دوران دبیرستان',
            ],
            [
                'title' => 'فوق دیپلم',
                'lang'  => 'fa',
                'type'  => 'education',
                'description' => 'مدرک کاردانی',
            ],
            [
                'title' => 'لیسانس',
                'lang'  => 'fa',
                'type'  => 'education',
                'description' => 'مدرک کارشناسی',
            ],
            [
                'title' => 'فوق لیسانس',
                'lang'  => 'fa',
                'type'  => 'education',
                'description' => 'مدرک کارشناسی ارشد',
            ],
            [
                'title' => 'دکتری',
                'lang'  => 'fa',
                'type'  => 'education',
                'description' => 'اتمام تحصیلات تخصصی دانشگاهی',
            ],
            [
                'title' => 'فوق دکتری',
                'lang'  => 'fa',
                'type'  => 'education',
                'description' => 'اتمام تحصیلات فوق تختصصی دانشگاهی',
            ],
            [
                'title' => 'نوپا',
                'lang'  => 'fa',
                'type'  => 'exprience',
                'description' => 'بدون هیچ تجربه ای در نوشتن',
            ],
            [
                'title' => 'مبتدی',
                'lang'  => 'fa',
                'type'  => 'exprience',
                'description' => '1 تا 6 ماه',
            ],
            [
                'title' => 'نیمه حرفه ای',
                'lang'  => 'fa',
                'type'  => 'exprience',
                'description' => '6 ماه تا 1 سال',
            ],
            [
                'title' => 'مجرب',
                'lang'  => 'fa',
                'type'  => 'exprience',
                'description' => '1 تا 3 سال',
            ],
            [
                'title' => 'حرفه ای',
                'lang'  => 'fa',
                'type'  => 'exprience',
                'description' => 'بیش از 3 سال',
            ],
            [
                'title' => 'خیلی زیاد',
                'lang'  => 'fa',
                'type'  => 'interest',
                'description' => 'حداقل 7 نوشته در 1 هفته',
            ],
            [
                'title' => 'زیاد',
                'lang'  => 'fa',
                'type'  => 'interest',
                'description' => 'حداقل 4 نوشته در 1 هفته',
            ],
            [
                'title' => 'تقریبا زیاد',
                'lang'  => 'fa',
                'type'  => 'interest',
                'description' => 'حداقل 8 نوشته در 1 ماه',
            ],
            [
                'title' => 'متوسط',
                'lang'  => 'fa',
                'type'  => 'interest',
                'description' => 'حداقل 4 نوشته در 1 ماه',
            ],
            [
                'title' => 'تقریبا کم',
                'lang'  => 'fa',
                'type'  => 'interest',
                'description' => 'حداقل 2 نوشته در 1 ماه',
            ],
            [
                'title' => 'کم',
                'lang'  => 'fa',
                'type'  => 'interest',
                'description' => 'حداقل 1 نوشته در 1 ماه',
            ],
            [
                'title' => 'خیلی کم',
                'lang'  => 'fa',
                'type'  => 'interest',
                'description' => 'حداقل 1 نوشته در 1 ماه',
            ],
            [
                'title' => 'کمبود وقت',
                'lang'  => 'fa',
                'type'  => 'interest',
                'description' => 'هر زمان وقت شد',
            ],
        ];
        App\Definition::insert($definitions);
    }
}
