<?php

use App\Permission;
use Illuminate\Database\Seeder;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = require 'permissions.php';
        foreach ($permissions as $name => $label) {
            $permission = new Permission;
            $permission->name = $name;
            $permission->label = $label;
            $permission->save();
        }
    }
}
