<?php

use App\Role;
use Illuminate\Database\Seeder;



class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = require 'roles.php';
        foreach ($roles as $name => $label) {
            $role = new Role;
            $role->name = $name;
            $role->label = $label;
            $role->save();
        }
    }
}
