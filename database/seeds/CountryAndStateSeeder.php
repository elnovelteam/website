<?php

use App\Country;
use App\State;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

require 'countries.php';

class CountryAndStateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('ALTER TABLE `countries` AUTO_INCREMENT = 1;');
        Country::insert(countrylist());
        $country = Country::whereCode('IR')->first();
        foreach (stateList() as $state) {
            $st = new State;
            $st->name = $state;
            $st->country_id = $country->id;
            $st->save();
            // array_push($states, $st);
        }
        // var_dump(stateList());
    }
}
