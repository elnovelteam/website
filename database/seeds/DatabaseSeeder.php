<?php

use App\BackupSection;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(CountryAndStateSeeder::class);
        // $this->call(ProviderSeeder::class);
        // $this->call(RoleSeeder::class);
        // $this->call(UserSeeder::class);
        // $this->call(DefinitionsSeeder::class);
        // $this->call(PermissionSeeder::class);
        // $this->call(BackupSectionSeeder::class);
        $this->call(PostSeeder::class);
    }
}
