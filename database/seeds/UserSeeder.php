<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('ALTER TABLE `users` AUTO_INCREMENT = 1;');
        $user = new User;
        $user->name = 'yoonus';
        $user->email = 'yoonustehrani28@gmail.com';
        $user->sex = 'male';
        $user->birthdate = '2003-04-17 00:00:00';
        $user->phone_number = '09150013422';
        $user->country_id = 103;
        $user->state_id = 11;
        $user->email_verified_at = now();
        $user->api_token = Str::random(20);
        $user->active = true;
        $user->password = bcrypt("uss828487");
        $user->provider_id = 1;
        $user->save();
        $role = \App\Role::whereName('DVP')->first();
        $user->roles()->sync($role);
        $users = factory(User::class, 20)->create();
    }
}
