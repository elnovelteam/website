<?php

return [
    'DVP' => 'Developer',
    'ADM' => 'Admin',
    'BLG' => 'Blogger',
    'JDG' => 'Judge',
    'WRT' => 'Writer',
    'CRT' => 'Critic',
    'RDR' => 'Reader'
];