<?php

use App\BackupSection;
use App\Section;
use Illuminate\Database\Seeder;

class BackupSectionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sections = [
            'حلقه نشر ادبی' => 'پیگیری انتشار آثار / سوالات نویسندگان',
            'حلقه مالی ، تبلیغات و سرمایه گذاری' => 'امور اسپانسرشیپ ، سرمایه گذاری و تبلیغات',
            'حلقه نقد ادبی' => 'امور منتقدین',
            'امور خوانندگان' => 'پاسخگویی به سوالات متداول و درخواست های خوانندگان',
            'حلقه دادرسی' => 'رسیدگی به شکایات آثار و امور وب سایت',
            'حلقه فناوری اطلاعات' => 'امور وبسایت ، گزارش عیب در وبسایت ، پیشنهادات برنامه نویسان',
            'حلقه مشارکت' => 'بررسی امور همکاری ، پیشنهادات و انتقادات',
            'گروه پادکست' => 'امور رادیویی و پادکست'
        ];
        foreach ($sections as $name => $description) {
            $section = new BackupSection;
            $section->name = $name;
            $section->description = $description;
            $section->save();
        }
    }
}
